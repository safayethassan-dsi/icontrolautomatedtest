package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.config.Configuration;
import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Frame;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SprocketMenu;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.UserManager;

import com.icontrolesi.automation.platform.util.Logger;


public class TifOnFly extends TestHelper{
	@Test
	public void test_c248_TifOnFly(){
		handleTifOnFly(driver);
	}
	
	
	private void handleTifOnFly(WebDriver driver){
	    new SelectRepo(AppConstant.DEM0);
	
		String base_url= driver.getCurrentUrl();
		
		SprocketMenu.openUserManager();
		
		UserManager um= new UserManager();
		um.loadUserSettings(AppConstant.USER);
		
		um.setGeneralAccess("Image On The Fly", false);
		
		driver.get(base_url);
		
		SearchPage.waitForFrameLoad(Frame.MAIN_FRAME);
		SearchPage.addSearchCriteria("filename");
		SearchPage.setOperatorValue(0, "does not equal");
		SearchPage.setCriterionValue(0, "paper");
		
		SearchPage.addSearchCriteria("Document Name");
		SearchPage.setOperatorValue(1, "is empty");
		
		performSearch();
		
		DocView dc= new DocView();
		
		String permission = dc.checkViewMenuPermission("Image");
		System.out.println( Logger.getLineNumber() + "Permission: "+permission);
		
		softAssert.assertTrue((permission.equalsIgnoreCase("0")), "3) On the first doc, verify that the [Image] tab is dimmed, and that you cannot access it:: ");
		
		SprocketMenu.openUserManager();
		um.loadUserSettings(Configuration.getConfig("recenseo.username"));
		um.setGeneralAccess("Image On The Fly", true);
		
		SearchPage.gotoViewDocuments();
		
		DocView.clickOnDocument(2);
		
		DocView.selectView("Imaged");
		
		WebElement notificationMsgElement = getElement(By.id("warning"));
		
		System.out.println( Logger.getLineNumber() + notificationMsgElement.getText()+"***");
		
		softAssert.assertAll();
	 }
}