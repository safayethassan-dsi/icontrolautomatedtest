package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import java.util.Date;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.ManageReviewAssignment;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.ReviewAssignmentAccordion;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;


public class AdminManageAssignments extends TestHelper{
	String assigneeName = "Abdul Awal";
	String assignmentNameToOpen = "COTEST - 007";
	String assignmentWorkflow = "Search";
	String assignmentPriority = "3";
	String assignmentName = assigneeName + new Date().getTime();
	
	@Test
	public void test_c166_AdminManageAssignments(){
		handleAdminManageAssignments(driver);
	}

	private void handleAdminManageAssignments(WebDriver driver){
		new SelectRepo(AppConstant.DEM0);
		
		SearchPage.setWorkflow("Search");
		
		performSearch();
		
		DocView.openManageReviewAssignmentWindow();
		
	    waitFor(20);
	    ManageReviewAssignment.gotoFiltersPanel();
	    ReviewAssignmentAccordion.selectAllFilterByStatus();
	    ReviewAssignmentAccordion.selectAllFilterByUsers();
	    ManageReviewAssignment.createAssignment(assignmentName, assignmentWorkflow, assigneeName, assignmentPriority);
	    
	    ReviewAssignmentAccordion.searchForAssignment(assignmentName);
	    
	    int totalAssignments = getTotalElementCount(By.cssSelector("span[title='" + assignmentName + "']"));
	    
	    softAssert.assertEquals(totalAssignments, 1, "5) Confirm the Assignment was created as expected in Assignment list:: ");
	    
	    //ReviewAssignmentAccordion.selectAssignment(assignmentName);	
	    tryClick(By.id("toggleAllAssignments"), 1);
	    
	    ManageReviewAssignment.editAssignment(assignmentName + "_Edited", "", "");
	    
	    ReviewAssignmentAccordion.searchForAssignment(assignmentName + "_Edited");
	    
	    totalAssignments = getTotalElementCount(By.cssSelector("span[title='" + assignmentName + "_Edited']"));
	    
	    softAssert.assertEquals(totalAssignments, 1, "6) Attempt to edit the new Assignment by changing one field:: ");
	    
	    ManageReviewAssignment.deleteAssignment(assignmentName + "_Edited");
	}
}
