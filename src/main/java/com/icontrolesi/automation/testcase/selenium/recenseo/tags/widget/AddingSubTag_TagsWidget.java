package com.icontrolesi.automation.testcase.selenium.recenseo.tags.widget;

import java.util.Calendar;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.TagManagerInGridView;

public class AddingSubTag_TagsWidget extends TestHelper{
	long timeFrame = Calendar.getInstance().getTimeInMillis();
	String parentTag = "AddingSubTag_TagsWidget_c1832" + timeFrame;
	String subTag = "AddingSubTag_TagsWidget_c1832_sub_" + timeFrame;
	
	@Test
	public void test_c1798_AddingSubTagFromManageTag(){
		new SelectRepo(AppConstant.DEM0);
		SearchPage.setWorkflow("Search");
		
		SearchPage.addSearchCriteria("Author");
		SearchPage.setCriterionValue(0, "jeff");
		
		performSearch();
		
		TagManagerInGridView.createTopLevelTag(parentTag, "");
		TagManagerInGridView.createSubTagFor(parentTag, subTag, "");
		
		boolean isTagCreated = TagManagerInGridView.isTagFound(subTag);
		
		List<WebElement> foundTagList = getElements(TagManagerInGridView.tagItemLocator);
		
		boolean isCreatedTagIsAChild = foundTagList.stream().anyMatch(tag -> getParentOf(tag).getAttribute("class").contains("childNode") && getText(tag).equals(subTag));
		
		TagManagerInGridView.deleteTag(parentTag);		
		
		softAssert.assertTrue(isTagCreated, "***) Tag created as expected:: ");
		softAssert.assertTrue(isCreatedTagIsAChild, "***) Created tag is a subtag::  "); 
		
		softAssert.assertAll();
	}
}
