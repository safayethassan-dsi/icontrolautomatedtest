package com.icontrolesi.automation.testcase.selenium.recenseo.tags.tags_accordion;

import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.ManageTags;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.TagAccordion;

public class ClearingSingleSelectionFromManageTag extends TestHelper{
	
	@Test
	public void test_c1800_ClearingSingleSelectionFromManageTag(){
		new SelectRepo(AppConstant.DEM0);
		SearchPage.setWorkflow("Search");
		
		TagAccordion.open();
		
		ManageTags.open();
		
		tryClick(ManageTags.tagItemCheckboxLocator);
		
		boolean isChecked = getAttribute(ManageTags.tagItemLocator, "class").contains("checked");
		
		tryClick(ManageTags.clearSelectionLocator, 5);
		
		boolean isItemDeselected = !getAttribute(ManageTags.tagItemLocator, "class").contains("checked");
		
		softAssert.assertTrue(isChecked, "***) Item is checked before clearing:: ");			
		softAssert.assertTrue(isItemDeselected, "***) Item is unchecked after clearing:: ");
		
		softAssert.assertAll();
	}
}
