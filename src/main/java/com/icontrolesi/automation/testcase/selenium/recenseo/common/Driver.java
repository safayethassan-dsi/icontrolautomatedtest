package com.icontrolesi.automation.testcase.selenium.recenseo.common;

import org.openqa.selenium.WebDriver;
/**
 * 
 * @author Shaheed Sagar
 * 
 */
public class Driver {
	private static WebDriver driver;
	
	private Driver(){
		
	}
	
	public static WebDriver getDriver(){
		return driver;
	}
	
	public static void setWebDriver(WebDriver driver){
		Driver.driver = driver;
		
	}
}
