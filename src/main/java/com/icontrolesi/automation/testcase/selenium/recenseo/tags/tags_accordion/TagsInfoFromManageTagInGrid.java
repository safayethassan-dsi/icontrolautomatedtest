package com.icontrolesi.automation.testcase.selenium.recenseo.tags.tags_accordion;

import java.util.Calendar;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.TagManagerInGridView;

public class TagsInfoFromManageTagInGrid extends TestHelper{
	long timeFrame = Calendar.getInstance().getTimeInMillis();
	String tagName = "TagsInfoFromManageTagInGrid_c1817_" + timeFrame;
	
	@Test
	public void test_c1817_TagsInfoFromManageTagInGrid(){
		new SelectRepo(AppConstant.DEM0);
		SearchPage.setWorkflow("Search");
		
		SearchPage.addSearchCriteria("Addressee");
		
		performSearch();
		
		TagManagerInGridView.open();
		TagManagerInGridView.selectTagFilter("All");
		TagManagerInGridView.createTopLevelTag(tagName, "");
		
		TagManagerInGridView.openTagInfoDetail(tagName);
		
		boolean isInfoAppeared = getTotalElementCount(By.xpath("//span[text()='Tag Info']")) == 1;
		
		
		
		TagManagerInGridView.deleteTag(tagName);
		
		Assert.assertTrue(isInfoAppeared, "***) Tag info appeared:: ");
	}
}
