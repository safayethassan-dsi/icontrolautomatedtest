package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Frame;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchesAccordion;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;


/*
 * 
 * DB: DEM4
(1) Verify Previous searches are saved under the Recent Searches area.
(2) Select one of the recent searches and verify search parameters are correct.
(3) Run search and confirm results are still correct
* 
 */

public class SearchHistory extends TestHelper{
	@Test
	public void test_c63_SearchHistory(){
		handleSearchHistory(driver);
	}

   
    protected void handleSearchHistory(WebDriver driver){
		new SelectRepo(AppConstant.DEM4);
		
		SearchPage.setWorkflow("Search");
	
		SearchesAccordion.open();
		SearchesAccordion.openSearchHistoryTab();
		
		boolean isSearchesSaved = getTotalElementCount(By.cssSelector("#searches-picklist > ul > li")) > 0;
		
		softAssert.assertTrue(isSearchesSaved, "(1)Verify Previous searches are saved under the Recent Searches area:: ");
		
		switchToDefaultFrame();
		waitForFrameToLoad(Frame.MAIN_FRAME);
		
		SearchPage.addSearchCriteria("Document ID");
		SearchPage.setOperatorValue(0, "is less than");
		SearchPage.setCriterionValue(0, "90000");
	    
		performSearch();
	    
	    int docIdMax = DocView.getDocmentId();
	          
	    softAssert.assertTrue((docIdMax <= 90000), "All document ids in the search results are less than 90000:: ");
      
	    SearchPage.goToDashboard();
	  	      
	    waitForFrameToLoad(Frame.MAIN_FRAME);
	    SearchPage.clearSearchCriteria();
	           
	    SearchesAccordion.open();
	    SearchesAccordion.openSearchHistoryTab();
	    //SearchesAccordion.addSelectedToSearch();
		
	    tryClick(By.cssSelector("#today li a i[class$='checkbox']"));
		tryClick(By.cssSelector("#history button[title='Add selected to search builder']"), 2);
	          
	    switchToDefaultFrame();
	    waitForFrameToLoad(Frame.MAIN_FRAME);
	    //Check value of text area having document id
        String DocumentIdRange = getElement(By.className("CriterionValue")).getAttribute("value");
    
        softAssert.assertEquals(DocumentIdRange, "90000", "Document Id range is OK");
  
        //Check value of criterion operator
        Select comboBox = new Select(getElement(By.className("CriterionOperator")));
        String selectedComboValue = comboBox.getFirstSelectedOption().getText();
      
        softAssert.assertEquals(selectedComboValue, "is less than", "Operator value is OK in the search parameter:: ");
                  
        //softAssert.assertEquals(step2Correct, 1, "(2)Select one of the recent searches and verify search parameters are correct:: ");
	  	    
        performSearch();
	    
        docIdMax = DocView.getDocmentId();
          
        softAssert.assertTrue((docIdMax <= 90000), "(3)Run search and confirm results are still correct:");
      
        softAssert.assertAll();
	}
}
