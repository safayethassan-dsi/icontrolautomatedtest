package com.icontrolesi.automation.testcase.selenium.recenseo.tags.manage_tags_grid;

import java.util.Calendar;

import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.TagManagerInGridView;

public class DeletingTagFromManageTagInGrid extends TestHelper{
	long timeFrame = Calendar.getInstance().getTimeInMillis();
	String tagName = "DeletingTagFromManageTagInGrid_c1812_" + timeFrame;
	
	String expectedConfirmationMsg = "Deleting a tag will delete the tag and remove all of the documents filed under it. \n" + 
			"It will also delete sub-tag(s) and remove documents filed under them, if there is any.\n" + 
			"Are you sure you want proceed deleting the tag(s) you selected?";
	
	
	@Test
	public void test_c1812_DeletingTagFromManageTagInGrid(){
		new SelectRepo(AppConstant.DEM0);
		SearchPage.setWorkflow("Search");
		
		SearchPage.addSearchCriteria("Addressee");
		
		performSearch();
		
		TagManagerInGridView.open();
		TagManagerInGridView.createTopLevelTag(tagName, "");
		
		String deletinConfirmgMsg = TagManagerInGridView.deleteTag(tagName);
		
		boolean isTagRemoved = !TagManagerInGridView.isTagFound(tagName);
		
		softAssert.assertEquals(deletinConfirmgMsg, expectedConfirmationMsg, "***) Deletig confirmation message appeared correctly:: ");
		softAssert.assertTrue(isTagRemoved, "***) Tag deleted and not found in the system:: ");
		
		softAssert.assertAll();
	}
}
