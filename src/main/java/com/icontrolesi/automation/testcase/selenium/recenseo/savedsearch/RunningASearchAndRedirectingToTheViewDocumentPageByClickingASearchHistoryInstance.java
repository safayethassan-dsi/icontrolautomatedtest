package com.icontrolesi.automation.testcase.selenium.recenseo.savedsearch;

import org.openqa.selenium.By;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Frame;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchesAccordion;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;

public class RunningASearchAndRedirectingToTheViewDocumentPageByClickingASearchHistoryInstance extends TestHelper{
	By todayItemLocator = By.cssSelector("#today > ul > li");
	
	String searchCriteria = "Author"; 
	
	String stepMsg8 = "8. Recenseo populates all the search criteria of the selected 'Search History' search on the Search Page ( %s )::";
	
	@Test
	public void test_c43_RunningASearchAndRedirectingToTheViewDocumentPageByClickingASearchHistoryInstance(){
		handleRunningASearchAndRedirectingToTheViewDocumentPageByClickingASearchHistoryInstance();
	}
	
	private void handleRunningASearchAndRedirectingToTheViewDocumentPageByClickingASearchHistoryInstance(){
		new SelectRepo(AppConstant.DEM0);
		
		SearchPage.setWorkflow("Search");
		
		SearchPage.addSearchCriteria(searchCriteria);
		performSearch();
		
		int totalDocumentCountInGridView = DocView.getDocmentCount();
		
		SearchPage.goToDashboard();
		waitForFrameToLoad(Frame.MAIN_FRAME);
		
		SearchesAccordion.open();
		SearchesAccordion.openSearchHistoryTab();
		
		tryClick(todayItemLocator);
		switchToDefaultFrame();
		waitForFrameToLoad(Frame.MAIN_FRAME);
		waitforViewDocumentPage();
		
		int totalDocumentCount = DocView.getDocmentCount();
		
		switchToDefaultFrame();
		
		String viewDocumentLinkClassValue = getAttribute(SearchPage.viewDocumentsLink, "class");
		
		
		softAssert.assertEquals(viewDocumentLinkClassValue, "tab-highlight", "7. Recenseo runs a search by using the search criteria in the selected 'Search History' Search and redirects the user to the View Documents Page (View Documents Tab selected):: ");
		softAssert.assertEquals(totalDocumentCount, totalDocumentCountInGridView, "7. Recenseo runs a search by using the search criteria in the selected 'Search History' Search and redirects the user to the View Documents Page (Document count equals to the Search):: ");
			
		softAssert.assertAll();		
	}
}
