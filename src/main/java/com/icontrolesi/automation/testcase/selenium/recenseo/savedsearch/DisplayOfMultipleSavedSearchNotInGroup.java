package com.icontrolesi.automation.testcase.selenium.recenseo.savedsearch;

import java.util.Date;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.FolderAccordion;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Frame;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchesAccordion;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;

public class DisplayOfMultipleSavedSearchNotInGroup extends TestHelper{
	long date = new Date().getTime();
	String savedSearchName = "DisplayOfMultipleSavedSearchNotInGroup_" + date + "_1";
	String savedSearchName2 = "DisplayOfMultipleSavedSearchNotInGroup_" + date +"_2";
	
	@Test
	public void test_c4_DisplayOfMultipleSavedSearchNotInGroup(){
		handleDisplayOfMultipleSavedSearchNotInGroup();
	}
	
	private void handleDisplayOfMultipleSavedSearchNotInGroup(){
		new SelectRepo(AppConstant.DEM0);
    	
    	SearchPage.setWorkflow("Search");
        
        FolderAccordion.open();
        FolderAccordion.expandFolder(driver, "Email");
        FolderAccordion.selectFolder(driver, "Foster, Ryan e-mail");
        FolderAccordion.addSelectedToSearch();
        
        switchToDefaultFrame();
        waitForFrameToLoad(driver, Frame.MAIN_FRAME);
        
        SearchPage.createSavedSearch(savedSearchName, "", "");
        
        SearchPage.clearSearchCriteria();
        
        FolderAccordion.open();
        FolderAccordion.selectFolder(driver, "EDRM");
        FolderAccordion.addSelectedToSearch();
        
        switchToDefaultFrame();
        waitForFrameToLoad(driver, Frame.MAIN_FRAME);
        
        SearchPage.createSavedSearch(savedSearchName2, "", "");
        
        SearchesAccordion.open();
        SearchesAccordion.selectFilter("All");
        SearchesAccordion.searchForSavedSearch(savedSearchName);
        
        int totalSearchResultFound = SearchesAccordion.getSavedSearchCount(savedSearchName);
        
        softAssert.assertEquals(totalSearchResultFound, 1);
        
        WebElement savedSearchElement = driver.findElement(SearchesAccordion.historyItemsLocation);
        
        String nameOfSavedSearch = getText(savedSearchElement);
        
        softAssert.assertEquals(nameOfSavedSearch, savedSearchName);
        
        String checkBoxDisplayProperty = savedSearchElement.findElement(By.cssSelector("a > i[class$='jstree-checkbox']")).getAttribute("display");
        
        softAssert.assertEquals(checkBoxDisplayProperty, null);
        
        SearchesAccordion.searchForSavedSearch(savedSearchName2);
        
        totalSearchResultFound = getTotalElementCount(SearchesAccordion.historyItemsLocation);
        
        softAssert.assertEquals(totalSearchResultFound, 1);
        
        savedSearchElement = driver.findElement(SearchesAccordion.historyItemsLocation);
        
        nameOfSavedSearch = getText(savedSearchElement);
        
        softAssert.assertEquals(nameOfSavedSearch, savedSearchName2);
        
        checkBoxDisplayProperty = savedSearchElement.findElement(By.cssSelector("a > i[class$='jstree-checkbox']")).getAttribute("display");
        
        softAssert.assertEquals(checkBoxDisplayProperty, null);
        
        softAssert.assertAll();
   	}
}
