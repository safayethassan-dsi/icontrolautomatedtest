package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.FolderAccordion;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.ReviewTemplatePane;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;


public class Dbd29TagsWidget extends TestHelper{
	String tagNumRemoved="";//indicates which tag was removed
	String tagNumAdded="";//indicates which tag was added
	
	@Test
	//@Test(enabled = false)
	public void test_c375_Dbd29TagsWidget(){
		handleDbd29TagsWidget(driver);
	}
	
	private void handleDbd29TagsWidget(WebDriver driver){
			new SelectRepo(AppConstant.DEM0);
			
			DocView.openPreferenceWindow();
			DocView.checkSelectedColumns("Tags");
			DocView.closePreferenceWindow();
			
			SearchPage.setWorkflow("Search");
			
			FolderAccordion.open();
			FolderAccordion.openFolderForView("Loose Files");
			
			switchToDefaultFrame();
			SearchPage.setWorkflowFromGridView("Issue Review");
			
			
			boolean isTagWidgetDisplayed = ReviewTemplatePane.isTagWidgetDisplayed();
			
			softAssert.assertTrue(isTagWidgetDisplayed, "(2)Confirm Tags Widget is present in the coding template:: ");
			
			List<String> tagListForASelectedDocument = new ArrayList<>();
			
			List<WebElement> tagColumnItems = getElements(DocView.documentTagColumnLocator);
			
			for(int i = 0; i < tagColumnItems.size(); i++){
				if(!tagColumnItems.get(i).getText().trim().equals("")){
					String tags = tagColumnItems.get(i).getText().trim();
					
					tagListForASelectedDocument = Stream.of(tags.split("\\,")).collect(Collectors.toList());
					tagColumnItems.get(i).click();
					waitForPageLoad(driver);
					waitFor(10);
					break;
				}
			}
			
			
			ReviewTemplatePane.selectTagWidgetFilter("Applied Tags");
			
			List<String> appliedTags = ReviewTemplatePane.getAppliedTags();
			
			softAssert.assertTrue(appliedTags.containsAll(tagListForASelectedDocument), "3) Verify that Tags Widget's Show Only Applied/Show all toggle (+/-) works properly:: "); 
			
			String tagNameToRemove = tagListForASelectedDocument.get(0);
			
			ReviewTemplatePane.removeTagsFromSelectedDocument(tagNameToRemove);
			
			ReviewTemplatePane.saveDocument();
			
			tagListForASelectedDocument = DocView.getTagsNameSplitted(1);
			
			boolean isTagRemovedFromSelectedDocument = tagListForASelectedDocument.contains(tagNameToRemove) == false;
			
			softAssert.assertTrue(isTagRemovedFromSelectedDocument, "4) In \"Show Only Applied\" state, Remove a tag and hit save. Verify the cleared tag goes away, when you navigate away from and back to the document:: ");
			
			softAssert.assertAll();
	}
}