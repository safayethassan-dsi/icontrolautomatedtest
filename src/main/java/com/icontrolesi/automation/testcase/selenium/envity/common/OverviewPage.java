package com.icontrolesi.automation.testcase.selenium.envity.common;

import org.openqa.selenium.By;

import com.icontrolesi.automation.common.CommonActions;

public class OverviewPage implements CommonActions{
	public static final By recommendationLocator = By.id("summary-actions");
	// public static final By DASHBOARD_LOCATOR = By.id("dashboardDiv");
	public static final By DASHBOARD_LOCATOR = By.id("page-wrapper");
	public static final By DOCUMENT_COUNT_LOCATOR = By.xpath("//h3[contains(text(), 'documents')]");
	public static final By TOTAL_TRAINING_SAMPLE_MSG_LOCATOR = By.cssSelector(".ui-helper-clearfix > div > h4");
	public static final By JOB_STATUS_LOCATOR = By.id("running");

	public static final By MONITOR_JOB_INIT_LOCATOR = By.xpath("//*[contains(text(), 'Initializing Batch monitoring job')]");

	public static OverviewPage overviewPage = new OverviewPage();

	public static String getJobStatus(){
		return overviewPage.getCssValue(JOB_STATUS_LOCATOR, "display").equals("none")? "Stopped" : "Running";
	}

	public static boolean isJobRunning(){
		return getJobStatus().equals("Running");
	}

	public static int getTotalDocumentsAdded(){
		return Integer.valueOf(overviewPage.getText(DOCUMENT_COUNT_LOCATOR).replaceAll("\\D+", ""));
	}
	/**
	 * Wait for Batch Job to start from Overview page
	 * 
	 * @param waitTime wait time is specified in seconds
	 */
	public static void waitForBatchJobStart(int waitTime){
		overviewPage.waitForVisibilityOf(MONITOR_JOB_INIT_LOCATOR, waitTime);
		overviewPage.waitForInVisibilityOf(MONITOR_JOB_INIT_LOCATOR, waitTime);
		System.err.printf("\nMonitor Job Started successfully...\n");
	}
}
