package com.icontrolesi.automation.testcase.selenium.envity.project_management;

import java.util.List;

import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.envity.common.ManageProjectsPage;
import com.icontrolesi.automation.testcase.selenium.envity.common.ProjectPage;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

public class EveryColumnDisplayedProperly extends TestHelper {
    @Test(description = "Workspace, Project Type, Last Modified, Job Status & Application Instance are displayed properly for each of the projects")
    public void test_c1879_EveryColumnDisplayedProperly(){
        ProjectPage.gotoProjectAdministrationPage();
        
        List<WebElement> tableRows =  getElements(ManageProjectsPage.PROJECT_ITEM_LOCATOR);

       

        //  Until NEXT button is disabled, do loop
        int currentPage = 1;
        int totalPage = ProjectPage.getTotalPageCount();
        while(currentPage <= totalPage){
            
            System.err.printf("\nVerifying columns for page %d\n\n", currentPage);
            
            for(WebElement row : tableRows){
                String projectName = getText(row.findElement(By.cssSelector("td:nth-of-type(1)")));
                String workspaceName = getText(row.findElement(By.cssSelector("td:nth-of-type(2)")));
                String projectType = getText(row.findElement(By.cssSelector("td:nth-of-type(3)")));
                String lastModified = getText(row.findElement(By.cssSelector("td:nth-of-type(4)")));
                String jobStatus = getText(row.findElement(By.cssSelector("td:nth-of-type(5) > a")));
                String appInstance = getSelectedItemFroDropdown(row.findElement(By.cssSelector("td:nth-of-type(6) select")));
                String seventhColumnValue = getText(row.findElement(By.cssSelector("td:nth-of-type(7) > a")));
                String eighthColumnValue = getAttribute(row.findElement(By.cssSelector("td:nth-of-type(8) img")), "src");
                
                
                softAssert.assertNotEquals(projectName, "", "***. Project column displayed as expected:: ");
                softAssert.assertNotEquals(workspaceName, "", "***. Workspace column displayed as expecedted:: ");
                softAssert.assertTrue(projectType.matches("SAL|CAL"), "*** Project Type matches:: ");
                softAssert.assertNotEquals(lastModified, "", "***. Last Modified date always displayed in the column:: ");
                softAssert.assertTrue(jobStatus.matches("Start Sync Job|Stop Sync Job"), "***. Job Status column displayed as expected:: ");
                softAssert.assertNotEquals(appInstance, "", "***. Application Instance date always displayed in the column:: ");
                softAssert.assertTrue(seventhColumnValue.matches("ReBuild Evaluation Index|''"), "***. 7th column displayed as expected:: " );
                softAssert.assertTrue(eighthColumnValue.endsWith("delete-icon.png"), "***. Delete Icon colum displayed properly:: ");  
            }
            
            if(currentPage < totalPage){
                ProjectPage.gotoNextPage();
                waitFor(5);
            }else{
                break;
            }
           
            currentPage = ProjectPage.getSelectedPageNumber();

            tableRows = getElements(ManageProjectsPage.PROJECT_ITEM_LOCATOR);
        }
        

        softAssert.assertAll();
    }
}