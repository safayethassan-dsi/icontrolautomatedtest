package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import java.io.File;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Driver;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Frame;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Utils;

import com.icontrolesi.automation.platform.util.Logger;

/**
 *
 * @author Milon
 *
 *         DB: DEM0
 * 
 *         Test each of the following report types to ensure proper
 *         functionality and results.
 * 
 *         1) Review Activity 2) Data Type Summary 3) Custodian Totals 4) Folder
 *         Summary 5) Documents Over Time 6) Picklist Tally Report 7) Email
 *         Domain Report Download/Export the reports to both CSV and Excel, if
 *         available, and verify the contents match the Recenseo results.
 * 
 */

public class ReportsReports extends TestHelper{
	@Test(enabled=false)
	public void test_c161_ReportsReports(){
		handleReportsReports(driver);
	}

	private void handleReportsReports(WebDriver driver){
		new SelectRepo(AppConstant.DEM0);
		SearchPage sp = new SearchPage();
		DocView docView = new DocView();

		String name = "C:/Users/dsipc/Downloads/";

		// for review report...

		SearchPage.waitForFrameLoad(Frame.MAIN_FRAME);

		sp.openReports(0);
		/// select all users.
		Utils.waitForElement("selectAllUsers", "id").click();

		WebElement reportFormButton = Utils.waitForElement("reportForm", "id");
		WebElement fieldSet = reportFormButton.findElement(By.tagName("fieldset"));
		WebElement submitButton = Utils.getFollowingSibling(fieldSet);
		submitButton.findElement(By.tagName("span")).click();
		Utils.waitShort();
		click(Utils.waitForElement("downloadReport", "id"));
		File varTmpDir = new File(name + "CodingReport_06_09_2016-06_09_2016.cst");
		if (varTmpDir.exists()) {
			System.out.println( Logger.getLineNumber() + "A. Review Issue done.....");
			varTmpDir.delete();
		}
		docView.clickCrossButton();

		for (int i = 1; i < 6; i++) {
			i = (i == 5 ? i + 1 : i);
			
			name = "C:/Users/dsipc/Downloads/";
			
			Driver.getDriver().switchTo().defaultContent();
			SearchPage.waitForFrameLoad(Frame.MAIN_FRAME);
			sp.openReports(i);
			Utils.waitMedium();
			WebElement bodyTag = Driver.getDriver().findElement(By.tagName("body"));
			WebElement heading = bodyTag.findElement(By.tagName("h1"));
			System.out.println( Logger.getLineNumber() + heading.getText());
			WebElement div = heading.findElement(By.tagName("div"));
			WebElement button = div.findElement(By.tagName("button")).findElement(By.tagName("span"));
			button.click();

			WebElement ulTag = bodyTag.findElement(By.tagName("ul"));

			WebElement liTag = ulTag.findElement(By.tagName("li"));
			WebElement aLink = liTag.findElement(By.tagName("a"));
			System.out.println( Logger.getLineNumber() + aLink.getText());
			
			File varTmpDir1 = new File(name + "report.csv");
			if(varTmpDir1.exists())
			{
				varTmpDir1.delete();
				System.out.println( Logger.getLineNumber() + i+" complete.........");
			}
			aLink.click();
			docView.clickCrossButton();
			Utils.waitShort();
		}

		// for Picklist Tally Report.......

		Driver.getDriver().switchTo().defaultContent();
		SearchPage.waitForFrameLoad(Frame.MAIN_FRAME);
		sp.openReports(5);

		WebElement spanTag = Driver.getDriver().findElement(By.id("generatePlot")).findElement(By.tagName("span"));
		spanTag.click();
		Utils.waitShort();
		

		WebElement downloadButton = Driver.getDriver().findElement(By.id("downloadReport")).findElement(By.tagName("span"));
		downloadButton.click();
		
		Utils.waitMedium();
		
		File downloadedFile = new File("C:/Users/dsipc/Downloads/" + "reports.csv");
		if(downloadedFile.exists())
		{
			System.out.println( Logger.getLineNumber() + "Fully completed......");
			downloadedFile.delete();
		}

		docView.clickCrossButton();
	}
}
