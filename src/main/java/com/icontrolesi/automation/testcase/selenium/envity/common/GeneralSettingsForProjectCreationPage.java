/**
 * 		This class contains the common FIELDS and ACTIONS for all types of project creation
 */

package com.icontrolesi.automation.testcase.selenium.envity.common;

import org.openqa.selenium.By;

import com.icontrolesi.automation.common.CommonActions;

public class GeneralSettingsForProjectCreationPage implements CommonActions{
	public static final By PROJECT_NAME_LOCATOR = By.name("name");;
	//public static final By ENVIZE_INSTANCE_LOCATOR = By.name("envizeConnection");;
	public static final By ENVIZE_INSTANCE_LOCATOR = By.cssSelector("select[name='envizeInstance.id'], [name='envizeConnection']");
	public static final By RELATIVITY_CONNECTION_LOCATOR = By.cssSelector("select[name='relativityConnection'], [name='envityRelativityInstance.id']");
	public static final By WORKSPACE_LOCATOR = By.name("workspaceId");
	
	public static final By PREDICTION_FIELD_LOCATOR = By.cssSelector("input[name='rankingField'], [name='field.name']");
	public static final By TRUE_POSITIVE_DROPDOWN_LOCATOR = By.cssSelector("select[name='positiveValue'], [name='field.choices[0].artifactIdWithName']");
	public static final By TRUE_NEGATIVE_DROPDOWN_LOCATOR = By.cssSelector("select[name='negativeValue'], [name='field.choices[1].artifactIdWithName']");
	
	public static final By SAVED_SEARCH_FIELD_LOCATOR = By.cssSelector("input[name='dataSourceSavedSearchName'], [name='dataSourceName']");
	public static final By BATCH_SET_FIELD_LOCATOR = By.cssSelector("input[name='batchSetName'], [name='envityBatchSet.batchSetName']");
	
	public static final By PROJECT_PAGE_LOADER = By.xpath("//*[text()='Retrieving fields...']");
	
	public static final By PROJECT_CREATION_WINDOW = By.className("ui-dialog-title");

	public static final By PROJECT_NAME_LABEL_LOCATOR = By.cssSelector(".projectLleftPanelItem label.nonBoldFont");

	public static final By PROJECT_NAME_DROPDOWN_LOCATOR = By.id("switchProject");
	
	public static final GeneralSettingsForProjectCreationPage gspp = new GeneralSettingsForProjectCreationPage();
	
	public static void populateGeneralConfiguration(String projectName, String envizeInstanc, String relativityConnection, String workspace) {
		gspp.enterText(PROJECT_NAME_LOCATOR, projectName);
		gspp.selectFromDrowdownByText(ENVIZE_INSTANCE_LOCATOR, envizeInstanc);
		gspp.selectFromDrowdownByText(RELATIVITY_CONNECTION_LOCATOR, relativityConnection);
		gspp.waitFor(10);
		gspp.selectFromDrowdownByText(WORKSPACE_LOCATOR, workspace);
		// SALGeneralConfigurationPage.waitforLoadingComplete();
		gspp.waitFor(20);
	}
	
	
	public static void populateFieldSettingsForSAL(String predictionField, String truePositiveValue, String trueNegativeValue) {
		gspp.enterText(PREDICTION_FIELD_LOCATOR, predictionField);
		gspp.waitFor(2);
		gspp.waitForVisibilityOf(By.className("ui-menu-item"));
		gspp.tryClick(By.className("ui-menu-item"), 2);
		gspp.selectFromDrowdownByText(TRUE_POSITIVE_DROPDOWN_LOCATOR, truePositiveValue);
		gspp.waitFor(2);
		gspp.selectFromDrowdownByText(TRUE_NEGATIVE_DROPDOWN_LOCATOR, trueNegativeValue);
	}
}
