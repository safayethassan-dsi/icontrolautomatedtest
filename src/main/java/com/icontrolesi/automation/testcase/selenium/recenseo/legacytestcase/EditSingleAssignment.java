package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.ManageReviewAssignment;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.ReviewAssignmentAccordion;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;


public class EditSingleAssignment extends TestHelper{
	public static String assignmentName = "EditSingleAssignment_13536";
	
	@Test
	public void test_c1665_EditSingleAssignment(){
		handleEditSingleAssignment(driver);
	}


	private void handleEditSingleAssignment(WebDriver driver){
	     	new SelectRepo(AppConstant.DEM0);
			
			SearchPage.setWorkflow("Search");
			
			ReviewAssignmentAccordion.open();
			ReviewAssignmentAccordion.selectFilterByStatus("All");
			ReviewAssignmentAccordion.openReviewTab();
            ReviewAssignmentAccordion.openAssignment(assignmentName);
            
            new DocView().clickActionMenuItem("Manage Review Assignments");
            
            switchToFrame(1);
            waitFor(10);
            
            ReviewAssignmentAccordion.searchForAssignment(assignmentName);
            ReviewAssignmentAccordion.selectAssignment(assignmentName);
            
            String currentAssignee = getText(By.cssSelector("div.right > span"));
            
            //String newAssignee = currentAssignee.equalsIgnoreCase("Unassigned") ? "Abdul Awal" : "UNASSIGNED";
            String newAssignee = currentAssignee.equals("Abdul Awal") ? "Ishtiyaq Kamal" : "Abdul Awal";
            
            ManageReviewAssignment.editAssignment("", newAssignee, "");
            
    		//ReviewAssignmentAccordion.searchForAssignment(assignmentName);  
    		
    		String updatedAssignee = getText(By.cssSelector("div.right > span")); 
    		
    		Assert.assertEquals(updatedAssignee.toUpperCase(), currentAssignee.toUpperCase(), "(5)Verify the assignment is updated:: ");
    		
 	        ReviewAssignmentAccordion.selectAssignment(assignmentName);
 	        
 	        currentAssignee = getText(By.cssSelector("div.right > span"));
 	       
 	        //newAssignee = currentAssignee.equals("Unassigned") ? "Abdul Awal" : "UNASSIGNED";
 	        newAssignee = currentAssignee.equals("Abdul Awal") ? "Ishtiyaq Kamal" : "Abdul Awal";
 	        
 	        ManageReviewAssignment.editAssignment(assignmentName + "_Edited", newAssignee, "");
      			
 	        String updatedAssignmentName = getText(By.cssSelector("#assignmentList > li > div > div > span:nth-child(2)"));
      		
 	        Assert.assertEquals(updatedAssignmentName, assignmentName + "_Edited", "7) Verify the assignment is updated - Assignment name:: ");
 	        
 	        updatedAssignee = getText(By.cssSelector("div.right > span"));
 	        
 	        Assert.assertEquals(updatedAssignee.toUpperCase(), currentAssignee.toUpperCase(), "7) Verify the assignment is updated - Assignee name:: ");
 	        
 	        ReviewAssignmentAccordion.selectAssignment(updatedAssignmentName);
	        ManageReviewAssignment.editAssignment(assignmentName, "", ""); // get back to orignal assignment name      
	}
}
