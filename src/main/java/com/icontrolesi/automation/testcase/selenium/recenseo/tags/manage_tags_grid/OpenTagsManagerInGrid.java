package com.icontrolesi.automation.testcase.selenium.recenseo.tags.manage_tags_grid;

import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.TagManagerInGridView;

public class OpenTagsManagerInGrid extends TestHelper{
	@Test
	public void test_c1806_EditTagsFromManageTagInGrid(){
		new SelectRepo(AppConstant.DEM0);
		SearchPage.setWorkflow("Search");
		
		performSearch();
		
		TagManagerInGridView.open();
	
		softAssert.assertAll();
	}
}
