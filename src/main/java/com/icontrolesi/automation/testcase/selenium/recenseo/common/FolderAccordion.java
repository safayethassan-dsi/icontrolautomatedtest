package com.icontrolesi.automation.testcase.selenium.recenseo.common;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.icontrolesi.automation.common.CommonActions;
import com.icontrolesi.automation.platform.config.Configuration;

import com.icontrolesi.automation.platform.util.Logger;

public class FolderAccordion implements CommonActions{
	public static By tabLocation = By.id("folder");
	public static By topAddToSearchBtn = By.cssSelector("button[title='Add selected to search builder']");
	public static By viewSelected = By.cssSelector("button[title='View documents in selected']");
	public static By firstFolderLocator = By.cssSelector("#folder-picklist > ul > li > a > i");
	public static By totalDocsInFolderAccordion = By.id("totalDocs");
	public static By folderInfoDetail = By.id("folder_detail");
	public static By folderInfoCloseButton = By.id("dialogClose");
	public static By totalCountInFolderInfoModal = By.id("totalCount");
	public static By folderLocator = By.cssSelector("ul[role='group'] > li[role='treeitem'] > a");
	//public static By folderItemLocator = By.cssSelector("#folder-picklist  li[role='treeitem'] > a");
	public static By folderItemLocator = By.cssSelector("#folder-picklist  li a");
	public static By folderCheckboxLocator = By.cssSelector("#folder-picklist  li[role='treeitem'] > a > i");
	
	static FolderAccordion folderAccordion = new FolderAccordion(); 

	public static void open(){
		WebElement folderAccordionTab = Driver.getDriver().findElement(tabLocation);
		
		if(folderAccordionTab.getAttribute("aria-expanded").trim().equals("false")){
			folderAccordionTab.click();
			if(Configuration.getConfig("selenium.browser").equals("ie11")){
				folderAccordion.waitForInVisibilityOf(By.id("loadingCounts"));
				folderAccordion.waitFor(30);
				folderAccordion.waitForFrameToLoad(Frame.FOLDER_FRAME);
			}else{
				folderAccordion.waitForFrameToLoad(Frame.FOLDER_FRAME);
				folderAccordion.waitForInVisibilityOf(By.id("loadingCounts"));
				folderAccordion.waitForNumberOfElementsToBeGreater(folderItemLocator, 0);
				folderAccordion.waitFor(15);
			}
			
			
			System.out.println( Logger.getLineNumber() + "\nFolders accordion expanded...\n");
		}else{
			folderAccordion.waitForFrameToLoad(Frame.FOLDER_FRAME);
			folderAccordion.waitForInVisibilityOf(By.id("loadingCounts"));
			folderAccordion.waitForNumberOfElementsToBeGreater(folderItemLocator, 0);
			folderAccordion.waitFor(15);
			System.out.println( Logger.getLineNumber() + "\nFolders accordion already expanded...\n");
		}
		
		
	}
	
	
	public static void selectFolder(String folderName){
		List<WebElement> listOfFolders = folderAccordion.getElements(folderItemLocator);
		System.out.println( Logger.getLineNumber() + "Number of folders: " + listOfFolders.size());
		for(WebElement folder : listOfFolders){
			System.err.println(folder.getText());
			if(folder.getText().trim().equals(folderName)){
				System.out.println( Logger.getLineNumber() + "Matched ho gaya...");
				folder.findElement(By.tagName("i")).click();
				new SearchPage().waitFor(10);
				break;
			}
		}
		
		System.out.println( Logger.getLineNumber() + folderName + " folder selected...");
	}
	
	
	public static void selectFolder(WebDriver driver, String folderName){
		folderAccordion.waitForVisibilityOf(folderItemLocator);
		List<WebElement> listOfFolders = folderAccordion.getElements(folderItemLocator);
		System.out.println( Logger.getLineNumber() + "Number of folders: " + listOfFolders.size());
		for(WebElement folder : listOfFolders){
			System.err.println(folder.getText());
			if(folder.getText().trim().equals(folderName)){
				folder.findElement(By.tagName("i")).click();
				new SearchPage().waitFor(10);
				break;
			}
		}
		
		System.out.println( Logger.getLineNumber() + folderName + " folder selected...");
	}
	
	public static void expandFolder(String folderName){
		List<WebElement> listOfFolders = folderAccordion.getElements(folderItemLocator);
		int i = 1;
		for(WebElement folder : listOfFolders){
			System.out.println( Logger.getLineNumber() + folder.getText()+"*********************************");
			if(folder.getText().trim().equals(folderName)){
				WebElement parentOfSelectedFoldersAnchor = new SearchPage().getParentOf(folder);
				parentOfSelectedFoldersAnchor.findElement(By.tagName("i")).click();
				folderAccordion.waitForAttributeIn(By.cssSelector("#folder-picklist  li:nth-child(" + i + ")"), "class", "open");
				folderAccordion.waitForAttributeIn(By.cssSelector("#folder-picklist  li:nth-child(" + i + ")"), "aria-busy", "false");
				folderAccordion.waitFor(8);
				System.out.println( Logger.getLineNumber() + folderName + " folder expanded...");
				break;
			}
			
			i++;
		}
	}
	
	public static void expandFolder(WebDriver driver, String folderName){
		List<WebElement> listOfFolders = folderAccordion.getElements(folderItemLocator);
		int i = 1;
		for(WebElement folder : listOfFolders){
			if(folder.getText().trim().equals(folderName)){
				WebElement parentOfSelectedFoldersAnchor = new SearchPage().getParentOf(folder);
				parentOfSelectedFoldersAnchor.findElement(By.tagName("i")).click();
				folderAccordion.waitForAttributeIn(By.cssSelector("#folder-picklist  li:nth-child(" + i + ")"), "class", "open");
				folderAccordion.waitFor(10);
				System.out.println( Logger.getLineNumber() + folderName + " folder expanded...");
				break;
			}
			
			i++;
		}
	}
	
	public void checkFolder(WebDriver driver,String folderName){
		By folderLocator = By.cssSelector("ul[role='group'] > li[role='treeitem']");
		List<WebElement> listOfFolders = folderAccordion.getElements(folderLocator);
		
		for(WebElement folder : listOfFolders){
			String folderTitle = folder.getAttribute("title").trim();
			String folderClass = folder.getAttribute("class");
			if(folderTitle.equals(folderName) && folderClass.contains("jstree-leaf")){
				folder.findElement(By.tagName("i")).click();
				new SearchPage().waitForAttributeIn(folderLocator , "aria-expanded", "true");
				System.out.println( Logger.getLineNumber() + folderName + " folder expanded...");
				break;
			}
		}
	}
	
	public static void selectFirstFolder(){
		Driver.getDriver().findElement(firstFolderLocator).click();
		folderAccordion.waitFor(8);
	}
	
	
	public void checkSubFolder(WebDriver driver,String subFolderName){
		List<WebElement> folderNames= folderAccordion.getElements(By.className("folderName"));
		String[] brokenFolders = subFolderName.split(">");
		for(int i=0;i<brokenFolders.length;i++){
			if(i==(brokenFolders.length-1)){
				for(int j=0;j<folderNames.size();j++){
					if(folderNames.get(j).getText().equals(brokenFolders[i])){
						click(SearchPage.getPrecedingSibling(folderNames.get(j)));
						break;
					}				
				}
			}else{
				for(int j=0;j<folderNames.size();j++){
					if(folderNames.get(j).getText().equals(brokenFolders[i])){
						click(SearchPage.getPrecedingSibling(SearchPage.getParent(folderNames.get(j))));
						Utils.waitShort();
						folderNames= folderAccordion.getElements(By.className("folderName"));
						break;
					}				
				}
			}
		}
	}

	
	public void addFolder(){
		click(Utils.waitForElement(Driver.getDriver(), "addToSearch", "id"));
	}
	
	
	public static void addSelectedToSearch(){
		folderAccordion.tryClick(topAddToSearchBtn);
		System.out.println( Logger.getLineNumber() + "Selected folder added to search...");
	} 
	
   public static long getTotalDocCount(){
	  return Long.parseLong(folderAccordion.getText(totalDocsInFolderAccordion).replaceAll("\\D+", ""));
   }
   
   public static long getTotaDocCountInFolderInfo(){
	  	return Long.parseLong(folderAccordion.getText(totalCountInFolderInfoModal).replaceAll("\\D+", ""));
  }
   
   
   public static void openFolderForView(String folderName){
	   selectFolder(folderName);
	   folderAccordion.tryClick(viewSelected);
	   folderAccordion.waitForPageLoad();
	   folderAccordion.switchToDefaultFrame();
	   folderAccordion.waitForFrameToLoad(Frame.MAIN_FRAME);
	  
	   folderAccordion.waitforViewDocumentPage();
	   
	   System.out.println( Logger.getLineNumber() + "\nSelected folder(s) opened for view...");
   }
   
	public static void openFolderInfoDetail(){
		folderAccordion.hoverOverElement(By.cssSelector("#folder-picklist > ul > li"));
		folderAccordion.tryClick(folderInfoDetail, folderInfoCloseButton);
		
		System.out.println( Logger.getLineNumber() + "\nFolder info modal opened ...");
	}
	
	public static void openFolderInfoDetail(String folderName){
		List<WebElement> folderList = folderAccordion.getElements(folderItemLocator);
		
		for(WebElement folder : folderList){
			if(folderAccordion.getText(folder).equals(folderName)){
				folderAccordion.hoverOverElement(folder);
				JavascriptExecutor jse = (JavascriptExecutor)Driver.getDriver();
				WebElement details = folderAccordion.getElement(folderInfoDetail);
				
				jse.executeScript("arguments[0].click();", details);
				break;
			}
		}
		
		folderAccordion.waitFor(5);
		
		System.out.println( Logger.getLineNumber() + "\nFolder info modal opened ...");
	}
	
	public static void closeFolderInfoDetail(){
		folderAccordion.tryClick(folderInfoCloseButton);
		System.out.println( Logger.getLineNumber() + "\nFolder info modal closed...");
	}
}
