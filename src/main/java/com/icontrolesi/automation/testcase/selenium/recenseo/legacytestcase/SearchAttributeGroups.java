package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Frame;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Utils;

import com.icontrolesi.automation.platform.util.Logger;

/**
 *
 * @author Shaheed Sagar
 *
 *  "DB: DEM0 
	(1) Go to search page and run a new search with the criteria: (Attributes) Dates is after or on ""2000-06-22"" 
	(2) Run search. Confirm results are logically correct. 
	(3) Return to search page and run a new search with the criteria: (Attributes) Names contains ""Fred"" 
	(4) Run search. Confirm results are logically correct. 
	(5) Return to search page and combine both searches together using Query Match Operator: AND  
	(6) Run search. Confirm results are logically correct. 
	(7) Return to search page and combine both searches together using Query Match Operator: OR  
	(8) Run search. Confirm results are logically correct."

*/

public class SearchAttributeGroups extends TestHelper{
	@Test
	public void test_c59_SearchAttributeGroups(){
		handleSearchAttributeGroups(driver);
	}

    private void handleSearchAttributeGroups(WebDriver driver){
    	new SelectRepo(AppConstant.DEM0);
        
        DocView.openPreferenceWindow();
        DocView.checkSelectedColumns("Date Added");
        DocView.closePreferenceWindow();

        waitForFrameToLoad(Frame.MAIN_FRAME);
        SearchPage.addSearchCriteria("dates");
        SearchPage.setOperatorValue(0, "is on or after");
        SearchPage.setCriterionValue(0, "2000-06-22");
        
        performSearch();

        List<WebElement> allDates = getElements(DocView.documentDateAddedLocator);
        
        boolean allOk = true;
        
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date checkerDate = null;
		try {
			checkerDate = formatter.parse("2000-06-22 00:00:00");
			System.out.println( Logger.getLineNumber() + "checkerDate: " + checkerDate);
		} catch (ParseException e2) {
			e2.printStackTrace();
		}
        for(WebElement e : allDates){
        	if(!e.getText().trim().equals("")){
        		Date docDate;
				try {
					docDate = formatter.parse(e.getText().trim());
					if(!docDate.after(checkerDate)){
	                    allOk = false;
						break;
					}
				} catch (ParseException e1) {
					System.out.println( Logger.getLineNumber() + e1.getMessage());
				}
			}
        }
        
        softAssert.assertTrue(allOk, "(2) Run search. Confirm results are logically correct:: ");

        SearchPage.goToDashboard();
        waitForFrameToLoad(Frame.MAIN_FRAME);

        SearchPage.clearSearchCriteria();
        
        SearchPage.addSearchCriteria("names");
        SearchPage.setCriterionValue(0, "Fred");

        performSearch();

        DocView.selectView("Text");

        softAssert.assertEquals(getText(By.id("term1")), "Fred", "(4) Run search. Confirm results are logically correct:: ");
        
        SearchPage.goToDashboard();
        waitForFrameToLoad(Frame.MAIN_FRAME);
        
        SearchPage.clearSearchCriteria();
        SearchPage.addSearchCriteria("names");
        SearchPage.setCriterionValue(0, "Fred");
       
        SearchPage.addSearchCriteria("dates");
        SearchPage.setOperatorValue(1, "is on or after");
        SearchPage.setCriterionValue(1, "2000-06-22");

        performSearch();

        int docCount = DocView.getDocmentCount();
        System.out.println( Logger.getLineNumber() + "doc count : "+docCount);
       
        softAssert.assertEquals(docCount, 6555, "(6) Run search. Confirm results are logically correct:: ");

        SearchPage.goToDashboard();
        waitForFrameToLoad(Frame.MAIN_FRAME);
        
        WebElement buttonElement = Utils.waitForElement("button[class='MatchOperator']", "css-selector");
        click(buttonElement);
        
        performSearch();

        docCount = DocView.getDocmentCount();
        System.out.println( Logger.getLineNumber() + "doc count : "+docCount);

        softAssert.assertEquals(docCount,  646934, "(8) Run search. Confirm results are logically correct:: ");
        
        softAssert.assertAll();
    }
}
