package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Frame;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;

/**
 *
 * @author Shaheed Sagar
 *
 *"Run a search, and display Document ID in Grid View. From the View menu:
- Select Default - Right, and verify that Recenseo displays...
--- Correct Document ID at the upper-right of doc view
--- Grid View, Coding Template, and the Doc view
- Select Default - Left, confirm Coding template is on left, and perform same validations from Default - Right.
- Select Grid Only, and verify Recenseo displays:
--- Correct Document ID at the upper-right of grid
--- Only the grid view
- Select Split Grid / Doc, and verify Recenseo displays:
--- Correct Document ID at the upper-right of doc view
--- Only the Grid and Doc views
- Select Hide Grid - Right and verify Recenseo displays:
--- Correct Document ID at the upper-right of doc view
--- Only the Doc and Coding template views
- Select Hide Grid - Left, confirm coding template is on the left, and perform same validations from Default - Right.

Log out of Repository, log back into the same database and verify Recenseo remembered your last layout setting and uses the ""Hide Grid - Left"" view."

 */

public class DbdViewLayoutOptions extends TestHelper{

	@Test
	public void test_c234_DbdViewLayoutOptions(){
		handleDbDViewLayoutOptions(driver);
	}

    private void handleDbDViewLayoutOptions(WebDriver driver){
        new SelectRepo(AppConstant.DEM0);
       
        DocView.openPreferenceWindow();
    	DocView.checkSelectedColumns("Document ID");
    	DocView.clickCrossBtn();
        
        SearchPage.waitForFrameLoad(driver, Frame.MAIN_FRAME);

        performSearch();

        softAssert.assertTrue(isDefaultRightViewSelectionOk(), "(***) Default - Right page layout selection working fine:: ");
        
        softAssert.assertTrue(isDefaultLeftViewSelectionOk(), "(***) Default - Left page layout selection working fine:: ");
        
        softAssert.assertTrue(isGridOnlyViewSelectionOk(), "(***) Grid Only page layout selection working fine:: ");
        
        softAssert.assertTrue(isSplitGridAndDocViewSelectionOk(), "(***) Split Grid / Doc page layout selection working fine:: ");
        
        softAssert.assertTrue(isHideGridRightViewSelectionOk(), "(***) Hide Grid - Right page layout selection working fine:: ");
		
        selectPageLayout("Hide Grid - Left");
		softAssert.assertTrue(isHideGridLeftSelectionOk(), "(***) Hide Grid - Left page layout selection working fine:: ");
        
		SearchPage.logout();
		
		performLoginToRecenseo();
		
		new SelectRepo(AppConstant.DEM0);
		
		waitForFrameToLoad(driver, Frame.MAIN_FRAME);
		performSearch();
			
		boolean isLastLayoutReserved = isHideGridLeftSelectionOk();
		
		selectPageLayout("Default - Right");
		
		softAssert.assertTrue(isLastLayoutReserved, "(###) Last selected layout reserved after log back:: ");
		
		softAssert.assertAll();
	}
    
    public boolean isDisplayed(By element){
    	return getCssValue(element, "display").equals("block");
    }
    
    boolean isDefaultRightViewSelectionOk(){
    	  selectPageLayout("Default - Right");
          
          boolean isSidebarPanelDisplayedProperly = isDisplayed(DocView.sideBarPanelLocator) && 
          										getCssValue(DocView.sideBarPanelLocator, "right").equals("0px") &&
          										getCssValue(DocView.sideBarPanelLocator, "width").equals("0px") == false &&
          										getCssValue(DocView.sideBarPanelLocator, "height").equals("0px") == false;
          
          boolean isDocPanelDisplayedProperly = isDisplayed(DocView.docPanelLocator) && 
  				getCssValue(DocView.docPanelLocator, "left").equals("0px") &&
  				getCssValue(DocView.docPanelLocator, "right").equals("0px") &&
  				getCssValue(DocView.docPanelLocator, "width").equals("0px") == false &&
  				getCssValue(DocView.docPanelLocator, "height").equals("0px") == false;
          
          
          boolean isGridPanelDisplayedProperly = isDisplayed(DocView.gridPanelLocator) && 
  				getCssValue(DocView.gridPanelLocator, "left").equals("0px") &&
  				getCssValue(DocView.gridPanelLocator, "right").equals("0px") &&
  				getCssValue(DocView.gridPanelLocator, "width").equals("0px") == false &&
  				getCssValue(DocView.gridPanelLocator, "height").equals("0px") == false;
          
          boolean isDefaultRightViewSelectionWorking = isSidebarPanelDisplayedProperly && isDocPanelDisplayedProperly && isGridPanelDisplayedProperly;
   
          return isDefaultRightViewSelectionWorking;
    }
    
    boolean isDefaultLeftViewSelectionOk(){
    	selectPageLayout("Default - Left");
        
        boolean isSidebarPanelDisplayedProperly = isDisplayed(DocView.sideBarPanelLocator) && 
        										getCssValue(DocView.sideBarPanelLocator, "left").equals("0px") &&
        										getCssValue(DocView.sideBarPanelLocator, "right").equals("0px") == false &&
        										getCssValue(DocView.sideBarPanelLocator, "width").equals("0px") == false &&
        										getCssValue(DocView.sideBarPanelLocator, "height").equals("0px") == false;
        
        boolean isDocPanelDisplayedProperly = isDisplayed(DocView.docPanelLocator) && 
				getCssValue(DocView.docPanelLocator, "left").equals("0px") &&
				getCssValue(DocView.docPanelLocator, "right").equals("0px") &&
				getCssValue(DocView.docPanelLocator, "width").equals("0px") == false &&
				getCssValue(DocView.docPanelLocator, "height").equals("0px") == false;
        
        
        boolean isGridPanelDisplayedProperly = isDisplayed(DocView.gridPanelLocator) && 
				getCssValue(DocView.gridPanelLocator, "left").equals("0px") &&
				getCssValue(DocView.gridPanelLocator, "right").equals("0px") &&
				getCssValue(DocView.gridPanelLocator, "width").equals("0px") == false &&
				getCssValue(DocView.gridPanelLocator, "height").equals("0px") == false;
        
        boolean isDefaultLeftViewSelectionWorking = isSidebarPanelDisplayedProperly && isDocPanelDisplayedProperly && isGridPanelDisplayedProperly;
        
        return isDefaultLeftViewSelectionWorking;
    }
    
    boolean isGridOnlyViewSelectionOk(){
    	 selectPageLayout("Grid Only");
         
         boolean isSidebarPanelHidden = (isDisplayed(DocView.sideBarPanelLocator) == false);
         boolean isDocPanelHidden = (isDisplayed(DocView.docPanelLocator) == false);
         
         boolean isGridPanelDisplayedProperly = isDisplayed(DocView.gridPanelLocator) && 
 				getCssValue(DocView.gridPanelLocator, "left").equals("0px") &&
 				getCssValue(DocView.gridPanelLocator, "right").equals("0px") &&
 				getCssValue(DocView.gridPanelLocator, "width").equals("0px") == false &&
 				getCssValue(DocView.gridPanelLocator, "height").equals("0px") == false;
        
         boolean isGridOnlyViewSelectionWorking = isSidebarPanelHidden && isDocPanelHidden && isGridPanelDisplayedProperly;
         
         return isGridOnlyViewSelectionWorking;
    }
    
    boolean isSplitGridAndDocViewSelectionOk(){
    	 selectPageLayout("Split Grid / Doc");
         
         boolean isSidebarPanelHidden = (isDisplayed(DocView.sideBarPanelLocator) == false);
        
         boolean isDocPanelDisplayedProperly = isDisplayed(DocView.docPanelLocator) && 
 				getCssValue(DocView.docPanelLocator, "left").equals("0px") &&
 				getCssValue(DocView.docPanelLocator, "right").equals("0px") &&
 				getCssValue(DocView.docPanelLocator, "top").equals("0px") == false &&
 				getCssValue(DocView.docPanelLocator, "bottom").equals("0px") &&
 				getCssValue(DocView.docPanelLocator, "width").equals("0px") == false &&
 				getCssValue(DocView.docPanelLocator, "height").equals("0px") == false;
         
         boolean isGridPanelDisplayedProperly = isDisplayed(DocView.gridPanelLocator) && 
 				getCssValue(DocView.gridPanelLocator, "left").equals("0px") &&
 				getCssValue(DocView.gridPanelLocator, "right").equals("0px") &&
 				//getCssValue(DocView.docPanelLocator, "top").equals("0px") &&
 				getCssValue(DocView.docPanelLocator, "bottom").equals("0px") &&
 				getCssValue(DocView.gridPanelLocator, "width").equals("0px") == false &&
 				getCssValue(DocView.gridPanelLocator, "height").equals("0px") == false;
        
         boolean isSplitGridAndDocViewSelectionWorking = isSidebarPanelHidden && isDocPanelDisplayedProperly && isGridPanelDisplayedProperly;
         
         return isSplitGridAndDocViewSelectionWorking;
    }
    
    boolean isHideGridRightViewSelectionOk(){
    	selectPageLayout("Hide Grid - Right");
        
        boolean isSidebarPanelDisplayedProperly = isDisplayed(DocView.sideBarPanelLocator) && 
										getCssValue(DocView.sideBarPanelLocator, "left").equals("0px") == false &&
										getCssValue(DocView.sideBarPanelLocator, "right").equals("0px") &&
										getCssValue(DocView.sideBarPanelLocator, "width").equals("0px") == false &&
										getCssValue(DocView.sideBarPanelLocator, "height").equals("0px") == false;

        boolean isDocPanelDisplayedProperly = isDisplayed(DocView.docPanelLocator) && 
									getCssValue(DocView.docPanelLocator, "left").equals("0px") &&
									getCssValue(DocView.docPanelLocator, "right").equals("0px") &&
									getCssValue(DocView.docPanelLocator, "top").equals("0px") == false &&
									getCssValue(DocView.docPanelLocator, "bottom").equals("0px") &&
									getCssValue(DocView.docPanelLocator, "width").equals("0px") == false &&
									getCssValue(DocView.docPanelLocator, "height").equals("0px") == false;
		
		
        boolean isGridPanelDisplayedProperly = isDisplayed(DocView.gridPanelLocator) && 
										getCssValue(DocView.gridPanelLocator, "height").equals("1px");
		
		boolean isHideGridViewSelectionWorking = isSidebarPanelDisplayedProperly && isDocPanelDisplayedProperly && isGridPanelDisplayedProperly;
		
		return isHideGridViewSelectionWorking;
    }
    
    boolean isHideGridLeftSelectionOk(){
        boolean isSidebarPanelDisplayedProperly = isDisplayed(DocView.sideBarPanelLocator) && 
										getCssValue(DocView.sideBarPanelLocator, "left").equals("0px") &&
										getCssValue(DocView.sideBarPanelLocator, "right").equals("0px") == false &&
										getCssValue(DocView.sideBarPanelLocator, "width").equals("0px") == false &&
										getCssValue(DocView.sideBarPanelLocator, "height").equals("0px") == false;

		boolean isDocPanelDisplayedProperly = isDisplayed(DocView.docPanelLocator) && 
									getCssValue(DocView.docPanelLocator, "left").equals("0px") &&
									getCssValue(DocView.docPanelLocator, "right").equals("0px") &&
									getCssValue(DocView.docPanelLocator, "top").equals("0px") == false &&
									getCssValue(DocView.docPanelLocator, "bottom").equals("0px") &&
									getCssValue(DocView.docPanelLocator, "width").equals("0px") == false &&
									getCssValue(DocView.docPanelLocator, "height").equals("0px") == false;
		
		
		boolean isGridPanelDisplayedProperly = isDisplayed(DocView.gridPanelLocator) && 
										getCssValue(DocView.gridPanelLocator, "height").equals("1px");
		
		boolean isHideGridLeftSelectionWokring = isSidebarPanelDisplayedProperly && isDocPanelDisplayedProperly && isGridPanelDisplayedProperly;
	
		return isHideGridLeftSelectionWokring;
    }
    
    void selectPageLayout(String layout){
    	DocView.openPreferenceWindow();
        DocView.selectPageLayout(layout);
        DocView.clickCrossBtn();
        
        waitForFrameToLoad(driver, Frame.MAIN_FRAME);
    }
}
