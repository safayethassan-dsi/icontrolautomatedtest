package com.icontrolesi.automation.testcase.selenium.envity.create_user;

import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.envity.common.ProjectPage;
import com.icontrolesi.automation.testcase.selenium.envity.common.UsersListPage;
import org.testng.annotations.Test;

import java.util.Date;

public class CreatingDuplicateUserIsChecked extends TestHelper {
        String possibleValidUserName = "AutoUser_" + new Date().getTime();
        String email = String.format("%s%s", possibleValidUserName, "@automation.com");

        @Test(description = "Creating duplicate user is checked.")
        public void  test_c1837_CreatingDuplicateUserIsChecked(){
            ProjectPage.gotoUserAdministrationPage();

            UsersListPage.openCreateUserWindow();

            UsersListPage.CreateUser.perform(email, possibleValidUserName, "@3455466@EEEE13f", "@3455466@EEEE13f", "Abdul Awal", "Sazib", "Project Reviewer", "iControl");

            UsersListPage.openCreateUserWindow();

            UsersListPage.CreateUser.fillUserCreationForm(email, possibleValidUserName, "@3455466@EEEE13f", "@3455466@EEEE13f", "Abdul Awal", "Sazib", "Project Reviewer", "iControl");

            UsersListPage.CreateUser.clickCreateBtn();


            softAssert.assertEquals(getText(UsersListPage.CreateUser.EMAIL_ERR), "This email is already taken", "***) Error is message is shown properly for EXISTING EMAIL:: ");
            softAssert.assertEquals(getText(UsersListPage.CreateUser.USERNAME_ERR), "This username is already taken", "***) Error is message is shown properly for EXISTING USERNAME:: ");

            UsersListPage.CreateUser.closeWindow();
            UsersListPage.deleteUser(possibleValidUserName);

            softAssert.assertAll();
        }
}
