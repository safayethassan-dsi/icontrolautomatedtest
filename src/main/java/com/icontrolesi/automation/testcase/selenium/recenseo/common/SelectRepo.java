package com.icontrolesi.automation.testcase.selenium.recenseo.common;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.SkipException;

import com.icontrolesi.automation.common.CommonActions;

import com.icontrolesi.automation.platform.util.Logger;

public class SelectRepo implements CommonActions{
	public static By repoLocator = By.cssSelector("#content > ul > li > a > span:nth-child(2)");
	static SelectRepo selectRepo = new SelectRepo();
	
	public SelectRepo() {}
	
	public SelectRepo(String repoName) {
		if(repoName == null || repoName.equals("")){
			throw new SkipException("Repo name can't be blank or null. Please set the repo name appropriately in the recenseo.properties file.");
		}
		waitForPageLoad();
		SearchPage.waitForFrameLoad(Frame.MAIN_FRAME);
		
		List<WebElement> repoList = getElements(repoLocator);
        
        System.err.println("Total Repos: " + repoList.size());
        
        for(WebElement repoLink : repoList){
        	if(repoLink.getAttribute("title").equals(repoName)){
        		repoLink.click();
        		System.out.println( Logger.getLineNumber() + "Repository '" + repoName + "' selected...");
        		break;
        	}
        }
        
        waitForFrameToLoad(Frame.MAIN_FRAME);
        
		waitForVisibilityOf(SearchPage.runSearchButton);

		switchToDefaultFrame();
	}
	
	public SelectRepo(WebDriver driver, String repoName) {
		SearchPage.waitForFrameLoad(Frame.MAIN_FRAME);
		//waitForFrameToLoad(Frame.MAIN_FRAME);
		  
		//waitForVisibilityOf(By.cssSelector("#content > ul > li > a > span"));
        List<WebElement> repoList = selectRepo.getElements(repoLocator);
        
        System.err.println("Total Repos: " + repoList.size());
        
        for(WebElement repoLink : repoList){
        	if(repoLink.getAttribute("title").equals(repoName)){
        		repoLink.click();
        		System.out.println( Logger.getLineNumber() + "Repository '" + repoName + "' selected...");
        		break;
        	}
        }
        
        SearchPage.waitForFrameLoad(Frame.MAIN_FRAME);
        //Frame.setCurrentFrame(Frame.MAIN_FRAME);
		
		waitForVisibilityOf(SearchPage.runSearchButton);

		switchToDefaultFrame();
	}
	
	
	public static void selectRepoFromRepoPage(WebDriver driver, String repoName){
		/*if(!Frame.getCurrentFrame().equals(Frame.MAIN_FRAME)){
			driver.switchTo().defaultContent();
			waitForFrameToLoad(driver, Frame.MAIN_FRAME);
		}*/
		//Frame.setCurrentFrame(Frame.MAIN_FRAME);
		selectRepo.waitForFrameToLoad(Frame.MAIN_FRAME);
		  
        List<WebElement> repoList = selectRepo.getElements(repoLocator);
        
        System.err.println("Total Repos: " + repoList.size());
        
        for(WebElement repoLink : repoList){
        	if(repoLink.getAttribute("title").equals(repoName)){
        		repoLink.click();
        		System.out.println( Logger.getLineNumber() + "Repository '" + repoName + "' selected...");
        		break;
        	}
        }
        
        selectRepo.waitForFrameToLoad(Frame.MAIN_FRAME);
        //Frame.setCurrentFrame(Frame.MAIN_FRAME);
		
		selectRepo.waitForVisibilityOf(SearchPage.runSearchButton);

		selectRepo.switchToDefaultFrame();
	} 
	
	public static void selectRepoFromSecondaryMenu(String repoName){
		selectRepo.switchToDefaultFrame();
		selectRepo.hoverOverElement(SearchPage.repoMenu);
		SearchPage.clickRepo(repoName);
		selectRepo.waitForFrameToLoad(Frame.MAIN_FRAME);
	}
	
	public static void selectRepoFromSecondaryMenu(WebDriver driver, String repoName){
		SearchPage.clickRepoMenu();
		SearchPage.clickRepo(repoName);
	}
}
