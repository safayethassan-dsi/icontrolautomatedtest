package com.icontrolesi.automation.testcase.selenium.envity.common;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.icontrolesi.automation.common.CommonActions;

public class UsersListPage implements CommonActions{
	public static final By PAGE_HEADER_LOCATOR = By.className("configPageTitle");
	public static final By CREATE_USER_BTN_LOCATOR = By.cssSelector("input[value='Create User']");
	public static final By SEARCH_FIELD_LOCATOR = By.cssSelector("input[placeholder='Search']");
	
	/**
	 * 		User table column header locators
	 */
	
	public static final By NAME_COLUMN_HEADER = By.xpath("//th[text()='Name']");
	public static final By USERNAME_COLUMN_HEADER = By.xpath("//th[text()='Username']");
	public static final By EMAIL_COLUMN_HEADER = By.xpath("//th[text()='Email Address']");
	public static final By ACTIVE_COLUMN_HEADER = By.xpath("//th[text()='Active']");
	public static final By ACTIONS_COLUMN_HEADER = By.xpath("//th[text()='Action icons']");
	
	/**
	 * 		User column locators
	 */
	
	public static final By NAME_COLUMN = By.cssSelector("#datatableUserList tbody tr td a");
	public static final By USERNAME_COLUMN = By.cssSelector("#datatableUserList tbody tr td:nth-child(2)");
	public static final By EMAIL_COLUMN = By.cssSelector("#datatableUserList tbody tr td:nth-child(3)");
	public static final By ACTIVE_COLUMN = By.cssSelector("#datatableUserList tbody tr td:nth-child(4)");
	
	
	/**
	 * 		Action column item locators
	 */
	public static final By EDIT_USER_ICON = By.cssSelector("a[title='Edit user']");
	public static final By MANAGE_PROJECT_ICON = By.cssSelector("a[title='Manage Project']");
	public static final By ENABLE_DISABLE_USER_ICON = By.cssSelector("a[title='Enable user'], a[title='Disable user']");
	public static final By RESET_PASSWORD_ICON = By.cssSelector("a[title='Reset Password']");
	public static final By DELETE_USER_ICON = By.cssSelector("a[title='Delete user']");
	
	
	/**
	 * 		Confirmation related locators
	 */
	
	public static final By CONFIRMATION_BODY = By.cssSelector(".modal-body div");
	public static final By CONFIRM_BTN = By.xpath("//button[contains(text(), 'Confirm')]");
	public static final By SUCCESS_MSG = By.cssSelector(".alert.alert-success.successfulOrErrorMessages");
	
	
	public static final UsersListPage userListPage = new UsersListPage();

	
	public static class CreateUser{
		public static final By HEADER = By.className("panel-heading");
		public static final By EMAIL_FIELD = By.id("email");
		public static final By USERNAME_FIELD = By.id("username");
		public static final By PASSWORD_FIELD = By.id("password");
		public static final By CONFIRM_PASSWORD_FIELD = By.id("confirmPassword");
		public static final By FIRSTNAME_FIELD = By.id("firstName");
		public static final By LASTNAME_FIELD = By.id("lastName");
		public static final By COMPANY_DROPDOWN = By.id("company.id");
		public static final By ROLE_FIELD = By.cssSelector("input[placeholder='Please Select Here To Choose Role']");
		public static  final  By ROLE_ITEM = By.cssSelector(".select2-results__option[role='treeitem']");
		
		public static final By CREATE_BTN = By.cssSelector("input[value='Create']");
        //public static final By CANCEL_BTN = By.cssSelector("input[value='Cancel']");
        public static final By CANCEL_BTN = By.id("close");
		
		
		/**
		 * 		Error message related LOCATORS
		 */	
		
		public static final By EMAIL_ERR = By.id("email-error");
		public static final By USERNAME_ERR = By.id("username-error"); 
		public static final By PASSWORD_ERR = By.id("password-error"); 
		public static final By CONFIRM_PASSWORD_ERR = By.id("confirmPassword-error"); 
		public static final By FIRSTNAME_ERR = By.id("firstName-error"); 
		public static final By LASTNAME_ERR = By.id("lastName-error");
		public static final By ROLE_ERR = By.id("companyRoleList-error"); 	
		
		
		public static void fillUserCreationForm(String email, String username, String password, String confirmPassword, String fname, String lName, String role, String company){
			userListPage.editText(EMAIL_FIELD, email);
			userListPage.editText(USERNAME_FIELD, username);
			userListPage.editText(PASSWORD_FIELD, password);
			userListPage.editText(CONFIRM_PASSWORD_FIELD, confirmPassword);
			userListPage.editText(FIRSTNAME_FIELD, fname);
			userListPage.editText(LASTNAME_FIELD, lName);
			userListPage.selectFromDrowdownByText(COMPANY_DROPDOWN, company);

			userListPage.waitFor(1);
			
			selectRole(role);
		}
		
		public static void clickCreateBtn(){
			userListPage.tryClick(CREATE_BTN);
		} 
		
		public static void perform(String email, String username, String password, String confirmPassword, String fname, String lName, String role, String company){
			fillUserCreationForm(email, username, password, confirmPassword, fname, lName, role, company);
			clickCreateBtn();
			userListPage.waitForInVisibilityOf(CREATE_BTN);
			
			System.err.printf("\nUser with name %s created successfully...\n", username);
		}

		public static void selectRole(String role){
			if (role.equals("") == false) {
			
				userListPage.tryClick(ROLE_FIELD);
				/*userListPage.getElements(ROLE_ITEM).stream()
						.filter(item -> item.getText().trim().equals(role))
						.findFirst()
						.get()
						.click();*/

				List<WebElement> roleList = userListPage.getElements(ROLE_ITEM);

				boolean isFound = false;
				for(WebElement roleItem : roleList){
					System.out.print(roleItem.getText()+"***");
					if(roleItem.getText().trim().equals(role)){
						roleItem.click();
						System.out.printf("Role %s selected...", role);
						isFound = true;
						break;
					}
				}

				if(!isFound){
					System.err.printf("No such Role %s found!!", role);
				}

			}

		}
		
		public static void closeWindow(){
			userListPage.tryClick(CANCEL_BTN);
			userListPage.waitForInVisibilityOf(CANCEL_BTN);
			
			System.err.println("Create User window closed...");
		}
	}
	
	public static class EditUser{
		public static final By HEADER = By.className("panel-heading");
		public static final By EMAIL_FIELD = By.id("email");
		public static final By USERNAME_FIELD = By.id("username");
		public static final By FIRSTNAME_FIELD = By.id("firstName");
		public static final By LASTNAME_FIELD = By.id("lastName");
		public static final By COMPANY_DROPDOWN = By.id("company.id");
		public static final By ROLE_FIELD = By.cssSelector("input[placeholder='Please Select Here To Choose Role']");
		public  static final  By ROLE_ITEM = By.cssSelector("#select2-companyRoleList-results li");
		
		public static final By SAVE_BTN = By.cssSelector("input[value='Save']");
		public static final By CLOSE_BTN = By.id("close");

		/**
		 * 		Error message related LOCATORS
		 */
		public static final By EMAIL_ERR = By.id("email-error");
		public static final By USERNAME_ERR = By.id("username-error");
		public static final By LASTNAME_ERR = By.id("lastName-error");
		public static final By ROLE_ERR = By.id("companyRoleList-error");
		public static final By FIRSTNAME_ERR = By.id("firstName-error");
		public static final By COMPANY_ERR = By.id("company.id-error");

		public static void open(){
			userListPage.tryClick(EDIT_USER_ICON, SAVE_BTN);
			
			System.err.println("\nEdit User window opened...\n");
		}


		public static void openFor(String username){
			userListPage.editText(SEARCH_FIELD_LOCATOR, username);

			List<WebElement> userList = userListPage.getElements(USERNAME_COLUMN);

			for(WebElement user : userList){
				System.out.println("User: " + user.getText() + "%%%%%%%%%%%%%%%%%%%%%%%%%");
				if(user.getText().trim().equals(username)){
					userListPage.getParentOf(user).findElement(By.cssSelector("a.editUser")).click();
					userListPage.waitForVisibilityOf(SAVE_BTN);
					System.err.println("Edit User window opened...");
					break;
				}
			}
		}

		public static void perform(String email, String username, String fname, String lname, String company, String role){
			openFor(username);

			fillUserEditForm(email, username, fname, lname, company, role);

			clickSaveBtn();
		}

		public static void fillUserEditForm(String email, String username, String fname, String lname, String company, String role){
				userListPage.editText(EMAIL_FIELD, email);
				userListPage.editText(USERNAME_FIELD, username);
				userListPage.editText(FIRSTNAME_FIELD, fname);
				userListPage.editText(LASTNAME_FIELD, lname);


			if(company.equals("")) {
				userListPage.selectFromDrowdownByIndex(COMPANY_DROPDOWN, 0);
			}else {
				userListPage.selectFromDrowdownByText(COMPANY_DROPDOWN, company);
			}

			if(role.equals("")){
				removeAllRoles();
			}else{
				selectRole(role);
			}

		}

		public static void selectRole(String role){
			boolean isPresent = false;
			userListPage.tryClick(ROLE_FIELD);
			userListPage.getElements(ROLE_ITEM).stream()
					.filter(item -> item.getText().trim().equals(role))
					.findFirst()
					.get()
					.click();
		}

		public static void removeAllRoles(){
			userListPage.getElement(By.className("select2-selection__clear")).click();
			// userListPage.tryClick(By.cssSelector("ul.select2_selection__rendered span"));
			// userListPage.tryClick(By.className("select2-search__field"));
			userListPage.tryClick(ROLE_FIELD);
		}

		public  static void clickSaveBtn(){
			userListPage.tryClick(SAVE_BTN);
			userListPage.waitForInVisibilityOf(SAVE_BTN);
		}

		public  static void close(){
			userListPage.tryClick(CLOSE_BTN);
			userListPage.waitForInVisibilityOf(CLOSE_BTN);
			System.err.println("User Edit window closed...");
		}

	}

	public static void searchForUser(final String userToSearch){
		userListPage.editText(SEARCH_FIELD_LOCATOR, userToSearch);
	}	
	
	public static void openCreateUserWindow() {
		userListPage.tryClick(CREATE_USER_BTN_LOCATOR, CreateUser.CREATE_BTN);
		
		System.err.println("Create User window opened...");
	}
	
	public static void gotoManageUserProjectPage(){
		userListPage.tryClick(MANAGE_PROJECT_ICON, ManageUserProjectPage.UPDATE_BTN);
		
		System.err.println("\nManage User Project loaded...\n");
	}

	public static void gotoManageUserProjectPageFor(final String userToFind){
		searchForUser(userToFind);
		userListPage.tryClick(MANAGE_PROJECT_ICON, ManageUserProjectPage.UPDATE_BTN);
		
		System.err.printf("%s %s...", "\nManage User Project loaded for\n", userToFind);
	}

	public static boolean isUserExist(String userName){
		userListPage.editText(SEARCH_FIELD_LOCATOR, userName);

		List<WebElement> userList = userListPage.getElements(USERNAME_COLUMN);

		for(WebElement user : userList){
			System.err.println("User: " + user.getText());
			if(user.getText().trim().equals(userName)){
				return  true;
			}
		}

		return false;
	}
	
	public static boolean isUserEnabled(String userName){
		userListPage.editText(SEARCH_FIELD_LOCATOR, userName);
		
		List<WebElement> userList = userListPage.getElements(USERNAME_COLUMN);
		
		for(WebElement user : userList){
			System.err.println("User: " + user.getText());
			if(user.getText().trim().equals(userName)){
				return  true;
			}
		}
		
		return false;
	}
	
	
	public static String getStatusIconType(String userName){
		userListPage.editText(SEARCH_FIELD_LOCATOR, userName);
		
		List<WebElement> userList = userListPage.getElements(USERNAME_COLUMN);
		
		for(WebElement user : userList){
			System.err.println("User: " + user.getText());
			if(user.getText().trim().equals(userName)){
				return userListPage.getParentOf(user).findElement(By.cssSelector("td a:nth-child(3) img")).getAttribute("src");
			}
		}
		
		return null;
	}
	
	
	public static void activateUser(String userName){
		boolean isUserDisabled = isUserEnabled(userName) == false;
		
		if(isUserDisabled){
			userListPage.tryClick(ENABLE_DISABLE_USER_ICON, CONFIRM_BTN);
			userListPage.tryClick(CONFIRM_BTN, SUCCESS_MSG);
			
			System.err.printf("\n%s is activated...\n", userName.toUpperCase());
		}else{
			System.err.printf("\n%s is already active...\n", userName.toUpperCase());
		}	
	}
	
	public static void deactivateUser(String userName){
		boolean isUserEnabled = isUserEnabled(userName);
		
		if(isUserEnabled){
			userListPage.tryClick(ENABLE_DISABLE_USER_ICON, CONFIRM_BTN);
			userListPage.tryClick(CONFIRM_BTN, SUCCESS_MSG);
			
			System.err.printf("\n%s is deactivated...\n", userName.toUpperCase());
		}else{
			System.err.printf("\n%s is already deactive...\n", userName.toUpperCase());
		}	
	}


	public static void deleteUser(String userName){
		boolean isUserPresent = isUserExist(userName);

		if(isUserPresent){
			userListPage.tryClick(DELETE_USER_ICON, CONFIRM_BTN);
			userListPage.tryClick(CONFIRM_BTN, SUCCESS_MSG);

			System.err.printf("\n%s is deleted...\n", userName.toUpperCase());
		}else{
			System.err.printf("\n%s is already deleted or does not exist...\n", userName.toUpperCase());
		}
	}
	
	
	public static void toggleUserStatus(String userName, boolean currentStatus){
		if(currentStatus){
			deactivateUser(userName);
		}else{
			activateUser(userName);
		}
	} 
}
