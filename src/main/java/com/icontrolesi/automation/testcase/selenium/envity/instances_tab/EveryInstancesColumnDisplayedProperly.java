package com.icontrolesi.automation.testcase.selenium.envity.instances_tab;

import java.util.List;

import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.envity.common.GeneralConfigurationPage;
import com.icontrolesi.automation.testcase.selenium.envity.common.ManageProjectsPage;
import com.icontrolesi.automation.testcase.selenium.envity.common.ProjectPage;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

public class EveryInstancesColumnDisplayedProperly extends TestHelper {
    @Test(description = "Workspace, Project Type, Last Modified, Job Status & Application Instance are displayed properly for each of the projects")
    public void test_c1842_EveryColumnDisplayedProperly(){
        ProjectPage.gotoGeneralConfigurationPage();
        GeneralConfigurationPage.Instances.gotoInstancePage();
        
        List<WebElement> tableRows =  getElements(GeneralConfigurationPage.Instances.TABLE_ROW);

       

        //  Until NEXT button is disabled, do loop
        int currentPage = 1;
        int totalPage = ProjectPage.getTotalPageCount();
        while(currentPage <= totalPage){
            System.err.printf("\nVerifying columns for page %d\n\n", currentPage);
            for(WebElement row : tableRows){
                String projectName = getText(row.findElement(By.cssSelector("td:nth-of-type(2)")));
                boolean isInstanceOk = projectName.matches("(.*)\\(\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}:\\d+\\)");
               
                softAssert.assertTrue(isInstanceOk, "***. Project column displayed as expected:: ");
            }
            
            if(currentPage < totalPage){
                ProjectPage.gotoNextPage();
                waitFor(5);
            }else{
                break;
            }
           
            currentPage = ProjectPage.getSelectedPageNumber();

            tableRows = getElements(ManageProjectsPage.PROJECT_ITEM_LOCATOR);
        }
        

        softAssert.assertAll();
    }
}