package com.icontrolesi.automation.testcase.selenium.envity.pagination;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.envity.common.ProjectPage;

public class SortingWithColumnHeadersWorkAccordingly extends TestHelper{
	
	@Test
	public void test_c350_SortingWithColumnHeadersWork(){
		checkSortingByProject();
		checkSortingByWorkspace();
		checkSortingByProjectType();
		checkSortingByCreated();
		checkSortingByLastModified();
		
		softAssert.assertAll();
	}
	
	public void checkSortingByProject(){
		tryClick(ProjectPage.headerLocator, 2);
		System.out.println("\nChecking sorting funtionality for PROJECT column....\n");
		String sortingTypeBefore = getAttribute(ProjectPage.headerLocator, "class");
		List<String> projectListBeforeSorting = getListOfItemsFrom(ProjectPage.projectNameLocator, p -> p.getText());
		tryClick(ProjectPage.headerLocator, 2);
		String sortingTypeAfter = getAttribute(ProjectPage.headerLocator, "class");
		List<String> projectListAfterSorting = getListOfItemsFrom(ProjectPage.projectNameLocator, p -> p.getText());
		
		System.out.println(projectListAfterSorting+"*****");
		Collections.sort(projectListBeforeSorting);
		
		System.out.println(projectListBeforeSorting);
		System.out.println(projectListAfterSorting);

		tryClick(ProjectPage.headerLocator, 2);

		List<String> projectListAfterReSorting = getListOfItemsFrom(ProjectPage.projectNameLocator, p -> p.getText());
		
		softAssert.assertNotEquals(sortingTypeBefore, sortingTypeAfter);
		softAssert.assertEquals(projectListBeforeSorting, projectListAfterReSorting, "Sorting done by Project Name:: ");
	}
	
	public void checkSortingByWorkspace(){
		tryClick(ProjectPage.workspaceHeaderLocator, 2);
		System.out.println("Checking sorting funtionality for WORKSPACE column....\n");
		String sortingTypeBefore = getAttribute(ProjectPage.workspaceHeaderLocator, "class");
		List<String> workspaceListBeforeSorting = getListOfItemsFrom(ProjectPage.projectWorkspacLocator, p -> p.getText());
		tryClick(ProjectPage.workspaceHeaderLocator, 2);
		String sortingTypeAfter = getAttribute(ProjectPage.workspaceHeaderLocator, "class");
		List<String> workspaceListAfterSorting = getListOfItemsFrom(ProjectPage.projectWorkspacLocator, p -> p.getText());
		
		Collections.sort(workspaceListBeforeSorting, Collections.reverseOrder());

		tryClick(ProjectPage.workspaceHeaderLocator, 2);

		List<String> workspaceListAfterReSorting = getListOfItemsFrom(ProjectPage.projectWorkspacLocator, p -> p.getText());
		
		softAssert.assertNotEquals(sortingTypeBefore, sortingTypeAfter);
		softAssert.assertEquals(workspaceListBeforeSorting, workspaceListAfterReSorting, "Workspace sorted correctly:: ");
	}
	
	public void checkSortingByProjectType(){
		tryClick(ProjectPage.typeHeaderLocator, 2);
		System.out.println("Checking sorting funtionality for PROJECT TYPE column....\n");
		String sortingTypeBefore = getAttribute(ProjectPage.typeHeaderLocator, "class");
		List<String> projectTypeBeforeSorting = getListOfItemsFrom(ProjectPage.projectTypeLocator, p -> p.getText());
		tryClick(ProjectPage.typeHeaderLocator, 2);
		String sortingTypeAfter = getAttribute(ProjectPage.typeHeaderLocator, "class");
		List<String> projectTypeAfterSorting = getListOfItemsFrom(ProjectPage.projectTypeLocator, p -> p.getText());
		
		Collections.sort(projectTypeBeforeSorting);

		tryClick(ProjectPage.typeHeaderLocator, 2);

		List<String> projectTypeAfterReSorting = getListOfItemsFrom(ProjectPage.projectTypeLocator, p -> p.getText());
		
		softAssert.assertNotEquals(sortingTypeBefore, sortingTypeAfter);
		softAssert.assertEquals(projectTypeBeforeSorting, projectTypeAfterReSorting, "Sorting done accordingly for Project Type:: ");
	}
	
	public void checkSortingByCreated(){
		tryClick(ProjectPage.createdHeaderLocator, 2);
		System.out.println("Checking sorting funtionality for PROJECT CREATED column....\n");
		String sortingTypeBefore = getAttribute(ProjectPage.createdHeaderLocator, "class");
		List<Date> projectCreatedBeforeSorting = getListOfItemsFrom(ProjectPage.projectCreatedLocator, p -> convertStringToDate(p.getText()));
		tryClick(ProjectPage.createdHeaderLocator, 2);
		String sortingTypeAfter = getAttribute(ProjectPage.createdHeaderLocator, "class");
		List<Date> projectCreatedAfterSorting = getListOfItemsFrom(ProjectPage.projectCreatedLocator, p -> convertStringToDate(p.getText()));
		
		Collections.sort(projectCreatedBeforeSorting);

		tryClick(ProjectPage.createdHeaderLocator, 2);

		List<Date> projectCreatedAfterReSorting = getListOfItemsFrom(ProjectPage.projectCreatedLocator, p -> convertStringToDate(p.getText()));
		
		softAssert.assertNotEquals(sortingTypeBefore, sortingTypeAfter);
		softAssert.assertEquals(projectCreatedBeforeSorting, projectCreatedAfterReSorting, "Proejct Creation date sorted accordingly:: ");
	}
	
	
	public void checkSortingByLastModified(){
		tryClick(ProjectPage.lastModifiedHeaderLocator, 2);
		System.out.println("Checking sorting funtionality for LAST MODIFIED column....\n");
		String sortingTypeBefore = getAttribute(ProjectPage.lastModifiedHeaderLocator, "class");
		List<Date> projectLastModifiedBeforeSorting = getListOfItemsFrom(ProjectPage.projectLastModifiedLocator,p -> convertStringToDate(p.getText()));
		tryClick(ProjectPage.lastModifiedHeaderLocator, 2);
		String sortingTypeAfter = getAttribute(ProjectPage.lastModifiedHeaderLocator, "class");
		List<Date> projectLastModifiedAfterSorting = getListOfItemsFrom(ProjectPage.projectLastModifiedLocator, p -> convertStringToDate(p.getText()));
		
		Collections.sort(projectLastModifiedBeforeSorting);

		tryClick(ProjectPage.lastModifiedHeaderLocator, 2);

		List<Date> projectLastModifiedAfterReSorting = getListOfItemsFrom(ProjectPage.projectLastModifiedLocator, p -> convertStringToDate(p.getText()));
		
		softAssert.assertNotEquals(sortingTypeBefore, sortingTypeAfter);
		softAssert.assertEquals(projectLastModifiedBeforeSorting, projectLastModifiedAfterReSorting, "Last Modified date sorted correctly:: ");
	}
	
	public static Date convertStringToDate(String date){
		SimpleDateFormat sdf = new SimpleDateFormat("MMM dd, yyyy hh:mm a");
		Date d = null;
		try {
			d = sdf.parse(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		return d;
	}
}