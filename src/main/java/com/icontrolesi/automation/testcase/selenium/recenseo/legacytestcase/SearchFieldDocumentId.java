package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;



/*
 * 
 * DB: DEM0
(1) Go to search page and run a new search with the criteria: Document ID is in this list = 1,2,3,4,5,8,11,14,17,20,23,26,29,32,35,38,41,44,47,50,53,56,59,62,65,68,71,74,77,80,83,86,89,92,95,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,115,117,119,121,123,125,127,129,131,133,135,137,139,141,143
(2) Confirm an error message occurs: "Error, invalid format. Remember, all entries need to be separated by newlines, not commas. Please correct and try again."
(3) Revise Search Criteria and run a new search with the criteria:
1
2
3
4
5
8
11
14
17
20
23
26
29
32
35
38
41
44
47
50
53
56
59
62
65
68
71
74
77
80
83
86
89
92
95
98
99
100
101
102
103
104
105
106
107
108
109
110
111
112
113
115
117
119
121
123
125
127
129
131
133
135
137
139
141
143
(4) Run search. Confirm results are correct
(5) Return to search page and run a new search with the criteria: Document ID is in this list = copy 100,000 document ids from file "E:\general\TEAM LEADERS\100K_ID_List.txt"
(6) Run search. Confirm results are correct
 * 
 * 
 * 
 * 
 */


public class SearchFieldDocumentId extends TestHelper{
	String document_ids = "1,2,4,5,8,11,14,17,20,23,26,29,32,35,38,41,44,47,50,53,56,59,62,65,68,71,74,77,80,83,86,89,92,95,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,115,117,119,121,123,125,127,129,131,133,135,137,139,141,143";
	//String document_ids = "1,2,3,4,5,8,11,14,17,20,23,26,29,32,35,38,41,44,47,50,53,56,59,62,65,68,71,74,77,80,83,86,89,92,95,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,115,117,119,121,123,125,127,129,131,133,135,137,139,141,143";
	
	//String[] document_ids_with_newline= {"1","\n","2","\n","3","\n","4","\n","5","\n","8","\n","11","\n","14","\n","17","\n","20","\n","23","\n","26","\n","29","\n","32","\n","35","\n","38","\n","41","\n","44","\n","47","\n","50","\n","53","\n","56","\n","59","\n","62","\n","65","\n","68","\n","71","\n","74","\n","77","\n","80","\n","83","\n","86","\n","89","\n","92","\n","95","\n","98","\n","99","\n","100","\n","101","\n","102","\n","103","\n","104","\n","105","\n","106","\n","107","\n","108","\n","109","\n","110","\n","111","\n","112","\n","113","\n","115","\n","117","\n","119","\n","121","\n","123","\n","125","\n","127","\n","129","\n","131","\n","133","\n","135","\n","137","\n","139","\n","141","\n","143"};
	String[] document_ids_with_newline= {"1","\n","2","\n","4","\n","5","\n","8","\n","11","\n","14","\n","17","\n","20","\n","23","\n","26","\n","29","\n","32","\n","35","\n","38","\n","41","\n","44","\n","47","\n","50","\n","53","\n","56","\n","59","\n","62","\n","65","\n","68","\n","71","\n","74","\n","77","\n","80","\n","83","\n","86","\n","89","\n","92","\n","95","\n","98","\n","99","\n","100","\n","101","\n","102","\n","103","\n","104","\n","105","\n","106","\n","107","\n","108","\n","109","\n","110","\n","111","\n","112","\n","113","\n","115","\n","117","\n","119","\n","121","\n","123","\n","125","\n","127","\n","129","\n","131","\n","133","\n","135","\n","137","\n","139","\n","141","\n","143"};

	@Test
	public void test_c93_SearchFieldDocumentId(){
	  //loginToRecenseo();
	  handleSearchFieldDocumentId(driver);
	}


    protected void handleSearchFieldDocumentId(WebDriver driver){
		new SelectRepo(AppConstant.DEM0);
		
		DocView.openPreferenceWindow();
		DocView.checkSelectedColumns("Document ID");
		DocView.closePreferenceWindow();
		
		SearchPage.setWorkflow("Search");
		
		SearchPage.addSearchCriteria("Document ID");
		SearchPage.setOperatorValue(0, "is in list");
		SearchPage.setCriterionValueTextArea(0, document_ids);
		  
		tryClick(SearchPage.runSearchButton);
		
		String alertText = getAlertMsg(driver);
	    
		softAssert.assertEquals(alertText, "Error, invalid format. Remember, all entries need to be separated by newlines, "
				+ "not commas. Please correct and try again.", "(2)Confirm an error message occurs: Error, invalid format. Remember, all entries need:: ");
	  
		SearchPage.setCriterionValueTextArea(0, document_ids_with_newline);
	    
	    SearchPage.setSortCriterion();
	    SearchPage.setSortCriterionValue(1, "Document ID");
	   
	    performSearch();
	    
	    DocView.setNumberOfDocumentPerPage(500);	    
	    
	    List<String> foundIDList = getListOfItemsFrom(DocView.documentIDColumnLocator, id -> getText(id));
	    
	    List<String> idList = Arrays.asList(document_ids.split(","));
	      
	    softAssert.assertEquals(foundIDList, idList, "4)  Results are correct:: ");
	    
	    softAssert.assertAll();
	}
}
