package com.icontrolesi.automation.testcase.selenium.recenseo.savedsearch;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Frame;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchesAccordion;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;

import com.icontrolesi.automation.platform.util.Logger;

public class HoverOverSearchHistoryExpandName extends TestHelper{
	By todayItemLocator = By.cssSelector("#today > ul > li");
	
	String searchCriteria = "Author"; 
	
	@Test
	public void test_c40_HoverOverSearchHistoryExpandName(){
		handleHoverOverSearchHistoryExpandName();
	}
	
	private void handleHoverOverSearchHistoryExpandName(){
		new SelectRepo(AppConstant.DEM0);
		
		SearchPage.setWorkflow("Search");
		
		SearchPage.addSearchCriteria(searchCriteria);
		performSearch();
		
		SearchPage.goToDashboard();
		waitForFrameToLoad(driver, Frame.MAIN_FRAME);
		
		SearchesAccordion.open();
		SearchesAccordion.openSearchHistoryTab();
		
		boolean isShrinked = getAttribute(By.cssSelector("#expand > span"), "class").endsWith("icon-arrow-down2");
		
		hoverOverElement(todayItemLocator);
		//tryClick(By.cssSelector("#expand > span"), 5);
		
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		WebElement expand = driver.findElement(By.cssSelector("#expand > span"));
		
		jse.executeScript("arguments[0].click();", expand);
		waitFor(driver, 5);
		
		
		System.out.println( Logger.getLineNumber() + getAttribute(By.cssSelector("#expand > span"), "class")+"*****");
		boolean isExpanded = getAttribute(By.cssSelector("#expand > span"), "class").endsWith("icon-arrow-up2");
		
		softAssert.assertTrue(isShrinked && isExpanded, "8. Recenseo displays the expanded version (shows all the search criteria attribute being used) of the 'Search History' search name:: ");
		
		//tryClick(By.cssSelector("#expand > span"), 5);
		expand = driver.findElement(By.cssSelector("#expand > span"));
		
		jse.executeScript("arguments[0].click();", expand);
		waitFor(driver, 5);
		
		isShrinked = getAttribute(By.cssSelector("#expand > span"), "class").endsWith("icon-arrow-down2");;
		
		softAssert.assertTrue(isShrinked, "9. User can click on the icon again to see the truncated version:: ");
		
		softAssert.assertAll();		
	}
}
