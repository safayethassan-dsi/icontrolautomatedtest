package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import java.util.List;
import java.util.stream.Collectors;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.FolderAccordion;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;


public class Dbd76DocNavigation extends TestHelper{
	private int itemsPerpage = 25;
	@Test
	public void test_c1646_Dbd76DocNavigation(){
		handleDbd76DocNavigation(driver);
	}

	private void handleDbd76DocNavigation(WebDriver driver){
		new SelectRepo(AppConstant.DEM0);
		
		SearchPage.setWorkflow("Search");
		
		FolderAccordion.open();
		FolderAccordion.expandFolder("Email");
		FolderAccordion.openFolderForView("Foster, Ryan e-mail");
		switchToDefaultFrame();
		SearchPage.setWorkflowFromGridView("QC Objective Coding");
		
		DocView.setNumberOfDocumentPerPage(itemsPerpage);

		List<WebElement> notInStateItems = getElements(DocView.gridRowLocator).stream()
		.filter(item -> getAttribute(item, "class").contains("NotInState")).collect(Collectors.toList());

		long totalNotInState = notInStateItems.stream().count();

		if(itemsPerpage - totalNotInState < 5){
			for(int i = 1; i <= 5; i++){
				getElements(DocView.gridRowLocator).stream()
					.filter(item -> getAttribute(item, "class").contains("NotInState"))
					.findFirst()
					.get()
					.findElement(By.cssSelector("td[aria-describedby='grid_cb']"))
					.click();
			}
			
			new DocView().clickActionMenuItem("Workflow state manager");
			DocView.addDocsToWorkflow("QC Objective Coding");
			SearchPage.gotoViewDocuments();
		}
		
		/*List<WebElement> gridRows = getElements(DocView.gridRowLocator);
		
		boolean isFirstDocmentInState = gridRows.get(0).getAttribute("class").endsWith("NotInState") == false;
		boolean isLastDocmentInState = gridRows.get(gridRows.size() - 1).getAttribute("class").endsWith("NotInState") == false;
		
		if(isFirstDocmentInState)
			getElements(By.cssSelector("td[aria-describedby='grid_cb']")).get(0).click();
		
		if(isLastDocmentInState)
			getElements(By.cssSelector("td[aria-describedby='grid_cb']")).get(gridRows.size() - 1).findElement(By.tagName("input")).click();
			
		
		if(isFirstDocmentInState || isLastDocmentInState){
			new DocView().clickActionMenuItem("Workflow state manager");
			switchToFrame(1);
			//new DocView().addDocsToWorkflow("Issue Review");
			DocView.removeDocsFromeWorkflow("QC Objective Coding");
			
			SearchPage.gotoViewDocuments();
		}*/
		
		DocView.selectDocumentBySpecificNumber(itemsPerpage / 2); //Set the selected document in the middle
		
		DocView.gotoFirstDocument();
		
		int firstDocumentNumber = DocView.getCurrentDocumentNumber() % 25;
		
		boolean isFirstDocumentIsInState = !getElements(DocView.gridRowLocator).get(firstDocumentNumber -1).getAttribute("class").endsWith("NotInState");
	
		softAssert.assertTrue(isFirstDocumentIsInState, "5b). The First Doc button navigates to the Next Doc still IN the selected workflow state:: ");
		
		DocView.gotoLastDocument();
		
		int lastDocumentNumber = DocView.getCurrentDocumentNumber() % 25;
		
		boolean islastDocumentIsInState = !getElements(DocView.gridRowLocator).get(lastDocumentNumber -1).getAttribute("class").endsWith("NotInState");
		
		softAssert.assertTrue(islastDocumentIsInState, "5b). The Last Doc button navigates to the Next Doc still IN the selected workflow state:: ");
		
		DocView.gotoPreviousDocument();
		
		int previousDocumentNumber = DocView.getCurrentDocumentNumber() % 25;
		
		boolean ispreviousDocumentIsInState = !getElements(DocView.gridRowLocator).get(previousDocumentNumber -1).getAttribute("class").endsWith("NotInState");
		
		softAssert.assertTrue(ispreviousDocumentIsInState, "5c). The Previous Doc button navigates to the Next Doc still IN the selected workflow state:: ");
		
		DocView.gotoNextDocument();
		
		int nextDocumentNumber = DocView.getCurrentDocumentNumber() % 25;
		
		boolean isNextDocumentIsInState = !getElements(DocView.gridRowLocator).get(nextDocumentNumber -1).getAttribute("class").endsWith("NotInState");
		
		
		softAssert.assertTrue(isNextDocumentIsInState, "5d). The Next Doc button navigates to the Next Doc still IN the selected workflow state:: ");
		
		int firstNotInStateDocPosition = getFirstNotInStateDocPosition();

		DocView.selectDocumentBySpecificNumber(firstNotInStateDocPosition);
		
		boolean isDocumentNavigatedProperly = getElements(DocView.gridRowLocator).get(firstNotInStateDocPosition -1).getAttribute("class").contains("NotInState");
		
		softAssert.assertTrue(isDocumentNavigatedProperly, "5e). The entry box navigates to the proper document Not IN the selected workflow state:: ");
		
		int firstInStateDocPosition = getFirstInStateDocPosition();
		
		DocView.selectDocumentBySpecificNumber(firstInStateDocPosition);
		
		boolean isDocumentNavigatedProperlyForInState = !getElements(DocView.gridRowLocator).get(firstInStateDocPosition - 1).getAttribute("class").contains("NotInState");
		
		softAssert.assertTrue(isDocumentNavigatedProperlyForInState, "5e). The entry box navigates to the proper document IN the selected workflow state:: ");
		
		softAssert.assertAll();
	}

	/**
	 * 
	 * @param isNotInState whether document to be InState or NotInState
	 * @return Return the document Position which is either of the two states
	 */
	private int getFirstDocPositionBy(boolean isNotInState){
		List<WebElement> docList = getElements(DocView.gridRowLocator);
		
		int i = 0;
		for(; i < docList.size(); i++){
			if(docList.get(i).getAttribute("class").contains("NotInState") == isNotInState){
				break;
			}
		}

		return i + 1;
	}

	/**
	 * 
	 * @return Return the document Position which is InState
	 */
	private int getFirstInStateDocPosition(){
		return getFirstDocPositionBy(false);
	}

	/**
	 * 
	 * @return Return the document Position which is NotInState
	 */
	private int getFirstNotInStateDocPosition(){
		return getFirstDocPositionBy(true);
	}
}
