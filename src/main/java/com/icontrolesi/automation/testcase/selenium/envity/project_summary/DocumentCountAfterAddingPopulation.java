package com.icontrolesi.automation.testcase.selenium.envity.project_summary;

import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.envity.common.LeftPanelForProjectPage;
import com.icontrolesi.automation.testcase.selenium.envity.common.OverviewPage;
import com.icontrolesi.automation.testcase.selenium.envity.common.ProjectInfoLoader;
import com.icontrolesi.automation.testcase.selenium.envity.common.SALGeneralConfigurationPage;
import com.icontrolesi.automation.testcase.selenium.envity.common.SALInfo;

public class DocumentCountAfterAddingPopulation extends TestHelper {

	SALInfo salInfo = null;

	@Test(description = "Add population for a new project")
	public void test_390_DocumentCountAfterAddingPopulation() {
		checkPopulationAddingWorksForSAL();

		LeftPanelForProjectPage.gotoOverviewPage();

		int totalDocumentsAdded = OverviewPage.getTotalDocumentsAdded();

		softAssert.assertEquals(totalDocumentsAdded, 200);
		
		softAssert.assertAll();
	} 

	void checkPopulationAddingWorksForSAL(){
		salInfo = new ProjectInfoLoader("sal").loadSALInfo();

		SALGeneralConfigurationPage.createSALProject(salInfo);;
		
		LeftPanelForProjectPage.gotoTaskQueuePage();
		
		boolean isProjectCreationOk = SALGeneralConfigurationPage.isPorjectCreated();
		
		softAssert.assertTrue(isProjectCreationOk, "Simple CAL project creation FAILED:: ");

		SALGeneralConfigurationPage.addPopulation(salInfo.getSavedSearchNameForPopulatioin());

		boolean isPopulationAdded = SALGeneralConfigurationPage.isPopulationAdded();

		softAssert.assertTrue(isPopulationAdded, "SAL Population Adding FAILED:: ");
	}
}
