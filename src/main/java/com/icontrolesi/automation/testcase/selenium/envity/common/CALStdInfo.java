package com.icontrolesi.automation.testcase.selenium.envity.common;

public class CALStdInfo implements ProjectInfo{
    //  Optional Fields
    private String projectName = "";
    private String envizeInstance = "";
    private String relativityInstance = "-Please Select-";
    private String workspace = "-Please Select-";
    private String documentSet = "-Please Select-";

    private String predictionField = "";
    private String truePositive = "-Please Select-";
    private String trueNegative = "-Please Select-";
   
    private String savedSearchName = "";
    private String batchSetName = "";

    private String judgementalSample = "-Please Select-";
    private String syntheticDocument = "";


    public String getProjectName() {
        return this.projectName;
    }

    public String getEnvizeInstance() {
        return this.envizeInstance;
    }

    public String getRelativityInstance() {
        return this.relativityInstance;
    }

    public String getWorkspace() {
        return this.workspace;
    }

    public String getDocumentSet(){
        return this.documentSet;
    }

    
    public String getPredictionField() {
        return this.predictionField;
    }
    
    public String getTruePositive() {
        return this.truePositive;
    }
    
    public String getTrueNegative() {
        return this.trueNegative;
    }
    
    public String getSavedSearchName(){
        return this.savedSearchName;
    }

    public String getBatchSetName() {
        return this.batchSetName;
    }

    public String getJudgementalSample() {
        return this.judgementalSample;
    }

    public String getSyntheticDocument() {
        return this.syntheticDocument;
    }

    CALStdInfo(CALStdInfoBuilder sib){
        this.projectName =  sib.projectName;
        this.envizeInstance = sib.envizeInstance;
        this.relativityInstance = sib.relativityInstance;
        this.workspace = sib.workspace;
        this.documentSet = sib.documentSet;

        this.predictionField =  sib.predictionField;
        this.truePositive = sib.truePositive;
        this.trueNegative = sib.trueNegative;
       
        this.savedSearchName = sib.savedSearchName;
        this.batchSetName = sib.batchSetName;
        this.judgementalSample = sib.judgementalSample;
        this.syntheticDocument = sib.syntheticDocument;
    }	

    public static class CALStdInfoBuilder{
        private String projectName = "";
        private String envizeInstance = "";
        private String relativityInstance = "";
        private String workspace = "";
        private String documentSet = "-Please Select-";

        private String savedSearchName = "";
        private String predictionField = "";
        private String truePositive = "";
        private String trueNegative = "";
        private String batchSetName = "";
        private String judgementalSample = "";
        private String syntheticDocument = "";
        


        public CALStdInfoBuilder setProjectName(String projectName) {
            this.projectName = projectName;
            return this;
        }

       
        /**
         * @param documentSet the documentSet to set
         */
        public CALStdInfoBuilder setDocumentSet(String documentSet) {
            this.documentSet = documentSet;
            return this;
        }

        public CALStdInfoBuilder setEnvizeInstance(String envizeInstance) {
            this.envizeInstance = envizeInstance;
            return this;
        }

        public CALStdInfoBuilder setRelativityInstance(String relativityInstance) {
            this.relativityInstance = relativityInstance;
            return this;
        }


        public CALStdInfoBuilder setWorkspace(String workspace) {
            this.workspace = workspace;
            return this;
        }


        public CALStdInfoBuilder setPredictionField(String predictionField) {
            this.predictionField = predictionField;
            return this;
        }


        public CALStdInfoBuilder setTruePositive(String truePositive) {
            this.truePositive = truePositive;
            return this;
        }


        public CALStdInfoBuilder setTrueNegative(String trueNegative) {
            this.trueNegative = trueNegative;
            return this;
        }
        
        public CALStdInfoBuilder setSavedSearchName(String savedSearchName) {
            this.savedSearchName = savedSearchName;
            return this;
        }

        public CALStdInfoBuilder setBatchSetName(String batchSetName) {
            this.batchSetName = batchSetName;
            return this;
        }


        public CALStdInfoBuilder setJudgementalSample(String judgementalSample) {
            this.judgementalSample = judgementalSample;
            return this;
        }


        public CALStdInfoBuilder setSyntheticDocument(String syntheticDocument) {
            this.syntheticDocument = syntheticDocument;
            return this;
        }

        public CALStdInfo build(){
            return new CALStdInfo(this);
        }
    }
}