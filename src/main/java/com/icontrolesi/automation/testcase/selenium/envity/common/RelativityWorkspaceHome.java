package com.icontrolesi.automation.testcase.selenium.envity.common;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.SkipException;

import com.icontrolesi.automation.common.CommonActions;
import com.icontrolesi.automation.common.Frame;
import com.icontrolesi.automation.platform.config.Configuration;
import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Driver;

public class RelativityWorkspaceHome implements CommonActions{
	private static RelativityWorkspaceHome rwh = new RelativityWorkspaceHome();
	
	public static final By EMAIL_LOCATOR = By.id("_email");
	public static final By PASSWD_LOCATOR = By.id("_password__password_TextBox");
	public static final By PROJECT_NAME_LOCATOR = By.xpath("//*[text()='iControl SvP']"); 
	
	/**
	 * 		Header browser items in sidebar
	 */
	public static final By FOLDER_LOCATOR = By.cssSelector("span[title='Saved Searches']");
	public static final By FIELD_TREE_LOCATOR = By.cssSelector("span[title='Field Tree']");
	public static final By SAVED_SEARCHES_LOCATOR = By.cssSelector("span[title='Saved Searches']");
	public static final By CLUSTER_LOCATOR = By.cssSelector("span[title='Clusters']");
	
	
	public static final By SAVED_SEARCH_FILTER_LOCATOR = By.cssSelector("input[placeholder='Filter']");
	public static final By SAVED_SEARCH_ITEM_LOCATOR = By.cssSelector(".browse-content ul.jstree-container-ul.jstree-children.jstree-wholerow-ul.jstree-no-checkboxes li a");
	
	public static final By CB_FILL_ITEMLIST_LOCATOR = By.id("cb_fil_itemListFUI");
	public static final By EDIT_BTN_LOCATOR = By.xpath("//button[text()='Edit']");
	public static final By DOCUMENT_ROW_LOCATOR = By.cssSelector("#fil_itemListFUI tbody tr");
	
	public static final By DOCUMENT_LOADING_ICON_LOCATOR = By.cssSelector("span.spinner");
	
	
	public static final By LAYOUT_LIST = By.name("LayoutDropdownList");
	public static final By LAYOUT_CONTAINER = By.id("dynamicViewRenderer_templateContainer");
	
	public static final By NUMBER_OF_DOC_PER_PAGE = By.cssSelector("select[ng-model$='selectedPageSizeOption']");
	
	
	public static void loginToRelativity(){
		String userName = Configuration.getConfig(AppConstant.PROJECT_SELECTED + ".username_relativity");
		String password = Configuration.getConfig(AppConstant.PROJECT_SELECTED + ".password_relativity");
		
		String URL = Configuration.getConfig(AppConstant.PROJECT_SELECTED + ".relativityURL");
		
		System.out.println("Relativity URL: "+ URL);
		
		Driver.getDriver().get(URL);
		loginToRelativityUsing(userName, password);
	}
	
	public static void loginToRelativityUsing(String userName, String password){
		System.out.printf("Performing login to Relativity with username %s...\n", userName.toUpperCase());
		
		rwh.enterText(EMAIL_LOCATOR, Configuration.getConfig(AppConstant.PROJECT_SELECTED + ".username_relativity") + "\n");
		rwh.enterText(PASSWD_LOCATOR, Configuration.getConfig(AppConstant.PROJECT_SELECTED + ".password_relativity")+"\n");
		
		Frame.switchToExternalFrame();
		rwh.waitForVisibilityOf(PROJECT_NAME_LOCATOR);
		
		System.out.printf("Logged in to Relativity with username %s...\n", userName.toUpperCase());
	}
	
	public static void selectWorkspace(String workspaceName){
		final By WORKSPACE_LOCATOR = By.xpath("//*[text()='"+workspaceName+"']");
		rwh.tryClick(WORKSPACE_LOCATOR);
		
		
		
//		rwh.waitForNumberOfElementsToBeGreater(By.cssSelector("#fil_itemListFUI tbody tr"), 1);
		
		System.err.println("iControl SvP selected.....\n");
	}
	
	public static void filterSavedSearches(String savedSearchName){
		Frame.switchToDefaultFrame();
		Frame.switchToExternalFrame();
		
		int totalSavedSearchesBeforeFiltering = rwh.getTotalElementCount(SAVED_SEARCH_ITEM_LOCATOR);
		
		rwh.tryClick(SAVED_SEARCHES_LOCATOR, SAVED_SEARCH_FILTER_LOCATOR);
		rwh.editText(SAVED_SEARCH_FILTER_LOCATOR, savedSearchName);
		rwh.waitFor(20);
		//rwh.waitForNumberOfElementsToBeLessThan(SAVED_SEARCH_ITEM_LOCATOR, totalSavedSearchesBeforeFiltering);
		
		System.out.printf("Filtering done for saved search %s.\n", savedSearchName);
	}
	
	public static void openSavedSearch(String savedSearchName){
		filterSavedSearches(savedSearchName);

		List<WebElement> savedSearchLoaded = rwh.getElements(SAVED_SEARCH_ITEM_LOCATOR);
		boolean isSavedSearchMatched = false;
		for(WebElement ss : savedSearchLoaded){
			String savedSerach = rwh.getText(ss);
			System.err.println(savedSerach + "***********");
			if(savedSerach.equals(savedSearchName) || savedSerach.contains("CSmple")){
				System.err.println("Match to paya gesi...");
				ss.click();
				rwh.waitForNumberOfElementsToBeGreater(DOCUMENT_LOADING_ICON_LOCATOR, 1);
				rwh.waitForNumberOfElementsToBePresent(DOCUMENT_LOADING_ICON_LOCATOR, 1);
				
				isSavedSearchMatched = true;
				break;
			}
		}
		
		if(isSavedSearchMatched == false){
			throw new SkipException(String.format("No saved search with the name %s found.", savedSearchName));
		}
		
		System.out.printf("Saved search %s opened.\n", savedSearchName);
	}
	
	
	public static void setNumberofDocumentsPerPage(int numberOfDocumentsPerPage){
		rwh.selectFromDrowdownByText(NUMBER_OF_DOC_PER_PAGE, String.valueOf(numberOfDocumentsPerPage));
		
		rwh.waitForNumberOfElementsToBeGreater(DOCUMENT_LOADING_ICON_LOCATOR, 1);
		rwh.waitForNumberOfElementsToBePresent(DOCUMENT_LOADING_ICON_LOCATOR, 1);
		
		System.out.printf("Number of documents per page: %d selected...\n", numberOfDocumentsPerPage);
	}
		
	public static void selectFieldLayout(String layutName){
		String selectedLayout = rwh.getSelectedItemFroDropdown(LAYOUT_LIST);
		if(layutName.equals(selectedLayout) == false){
			rwh.selectFromDrowdownByText(LAYOUT_LIST, layutName);
			/*rwh.waitForInVisibilityOf(LAYOUT_CONTAINER);
			rwh.waitForVisibilityOf(LAYOUT_CONTAINER);*/
			rwh.waitFor(20);
			System.out.printf("Field layout %s selected.\n", layutName);
		}else{
			System.out.printf("Field layout %s already selected.\n", layutName);
		}
	}	
	
	public static void selectReviewField(String fieldName, String fieldValue){
		By fieldValueLocation = By.xpath(String.format("//td[text()='%s:']/parent::tr//label[text()='%s']", fieldName, fieldValue));
		
		WebElement fieldCheckbox = rwh.getParentOf(rwh.getElement(fieldValueLocation)).findElement(By.tagName("input"));
		rwh.tryClick(fieldValueLocation);
		//fieldCheckbox.click();
		rwh.tryClick(By.xpath(String.format("//td[text()='%s:']/parent::tr//span/input[1]", fieldName)));
		
		System.out.printf("%s selected with value %s.\n", fieldName, fieldValue);
	}
	
	public static void saveSelectedField(){
		rwh.tryClick(By.xpath("//a[text()='Save']"), 20);
		//rwh.waitForInVisibilityOf(LAYOUT_CONTAINER);
	}
	
	public static void waitForLoadingComplete(){
		rwh.waitForNumberOfElementsToBeGreater(DOCUMENT_LOADING_ICON_LOCATOR, 1);
		rwh.waitForNumberOfElementsToBePresent(DOCUMENT_LOADING_ICON_LOCATOR, 1);
		
		System.out.println("Page loaded completely...");
	}
}
