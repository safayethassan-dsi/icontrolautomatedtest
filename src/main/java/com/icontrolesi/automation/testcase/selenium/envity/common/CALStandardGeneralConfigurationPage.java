package com.icontrolesi.automation.testcase.selenium.envity.common;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.SkipException;

public class CALStandardGeneralConfigurationPage extends GeneralSettingsForProjectCreationPage{
	public static final By DOCUMENT_SET_LOCATOR = By.id("select2-savedSearchArtifactId-container");
	
	public static final By DOCUMENT_IDENTIFIER_LOCATOR = By.name("savedSearchArtifactId");
	
	public static final By BATCH_PREFIX_LOCATOR = By.name("batchPrefix");
	public static final By BATCH_SIZE_LOCATOR = By.name("batchSize");
	
	public static final By CHECKPOINT_ICON_LOCATOR = By.cssSelector(".icon.addStoppingPoints");
	public static final By SYNTHETIC_DOCUMENT_LOCATOR = By.className("addSyntheticDocuments");
	public static final By SYNTHETIC_DOCUMENT_FIELD_LOCATOR = By.name("syntheticDocumentList[0].text");
	
	public static final By CREATE_PROJECT_BTN_LOCATOR = By.cssSelector("input[value='Create']");
	public static final By CANCEL_PROJECT_BTN_LOCATOR = By.xpath("//button[text()='Cancel']");
	
	public static final By JUDGEMENTAL_SAMPLE_LOCATOR = By.id("judgeSavedSearchArtifactId");
	
	
	public static final CALStandardGeneralConfigurationPage csgp = new CALStandardGeneralConfigurationPage();
	
	
	public static class Checkpoint{
		public static final By CHECKPOINT_TYPE = By.name("type");
		/*
		 * Fields for Percentage
		 */
		public static final By NUMBER_OF_BATCHES = By.name("numberOfbatches");
		public static final By PERCENTAGE = By.name("percentage");
		
		/*
		 * 	Actions
		 */
		
		public static final By STOP_BATCHING_DROPDOWN_LOCATOR = By.name("stopBatchingForReview");
		public static final By CREATE_QC_ELUSION_BATCH_DROPDOWN_LOCATOR = By.name("createQcElusionBatch");
		public static final By QC_ELUSION_BATCH_SIZE_LOCATOR = By.name("qcBatchSize");
		public static final By LATEST_FOUND_DOCS_LOCATOR = By.name("docForReviewBatch");
		
		public static final By CREATE_BTN_LOCATOR = By.cssSelector("input[value$='Create'][class^='upsertCheckpoint ']");
		
		public static final By CLOSE_BTN_LOCATOR = By.id("close");
		
		
		public static final By CHECKPOINT_SUCCESS_MSG_LOCATOR = By.id("checkpointMsg");
		
		
		public static void fillActionFields(String stopBatching, String createElusionBatch, int elusionBatchSize, int foundBatchSize) {
			csgp.selectFromDrowdownByText(STOP_BATCHING_DROPDOWN_LOCATOR, stopBatching);
			csgp.selectFromDrowdownByText(CREATE_QC_ELUSION_BATCH_DROPDOWN_LOCATOR, createElusionBatch);
			csgp.enterText(QC_ELUSION_BATCH_SIZE_LOCATOR, String.valueOf(elusionBatchSize));
			csgp.enterText(LATEST_FOUND_DOCS_LOCATOR, String.valueOf(foundBatchSize));
		}
		
		public static void fillActionFields(String stopBatching, String createElusionBatch, String elusionBatchSize, String foundBatchSize) {
			csgp.selectFromDrowdownByText(STOP_BATCHING_DROPDOWN_LOCATOR, stopBatching);
			csgp.selectFromDrowdownByText(CREATE_QC_ELUSION_BATCH_DROPDOWN_LOCATOR, createElusionBatch);
			csgp.enterText(QC_ELUSION_BATCH_SIZE_LOCATOR, String.valueOf(elusionBatchSize));
			csgp.enterText(LATEST_FOUND_DOCS_LOCATOR, String.valueOf(foundBatchSize));
		}
		
		public static void clickCreateBtn() {
			csgp.tryClick(CREATE_BTN_LOCATOR, CHECKPOINT_SUCCESS_MSG_LOCATOR);
		}
		
		public static void createCheckpointForPercentage(int numberOfBatches, int percentage) {
			csgp.selectFromDrowdownByText(CHECKPOINT_TYPE, "Percentage");
			
			csgp.enterText(NUMBER_OF_BATCHES, String.valueOf(numberOfBatches));
			csgp.enterText(PERCENTAGE, String.valueOf(percentage));
			
			fillActionFields(CALSTDConstants.STOP_BATCHING, CALSTDConstants.CREATE_QC_ELUSION_BATCH, CALSTDConstants.QC_ELUSION_BATCH_SIZE, CALSTDConstants.NUMBER_OF_LATEST_FOUND_DOCS_FOR_REVIEW_BATCH);
			
			clickCreateBtn();
			
			System.err.printf("Checkpoint with 'Number of Batches: %d' and 'Percentage: %d...'", numberOfBatches, percentage);
		}
		
		public static void createCheckpointForPercentage(String numberOfBatches, String percentage) {
			csgp.selectFromDrowdownByText(CHECKPOINT_TYPE, "Percentage");
			
			csgp.enterText(NUMBER_OF_BATCHES, String.valueOf(numberOfBatches));
			csgp.enterText(PERCENTAGE, String.valueOf(percentage));
			
			fillActionFields(CALSTDConstants.STOP_BATCHING, CALSTDConstants.CREATE_QC_ELUSION_BATCH, CALSTDConstants.QC_ELUSION_BATCH_SIZE, CALSTDConstants.NUMBER_OF_LATEST_FOUND_DOCS_FOR_REVIEW_BATCH);
			
			clickCreateBtn();
			
			System.err.printf("Checkpoint with 'Number of Batches: %s' and 'Percentage: %s...'", numberOfBatches, percentage);
		}
		
		public static void close() {
			csgp.tryClick(CLOSE_BTN_LOCATOR);
		}
	}
	
	
	public static void openCheckpoint() {
		csgp.tryClick(CHECKPOINT_ICON_LOCATOR, Checkpoint.CHECKPOINT_TYPE);
		
		System.err.println("\nCheckpoint window opened...\n");
	}
	
	public static void selectSavedSearchFor(By element, String documentSetName) {
		csgp.tryClick(element);
		csgp.editText(By.className("select2-search__field"), documentSetName);
		
		List<WebElement> savedSearchList = csgp.getElements(By.cssSelector(".select2-results__options li"));
		
		for(WebElement savedSearch : savedSearchList) {
			if(savedSearch.getText().trim().equals(documentSetName)) {
				savedSearch.click();
				System.err.printf("Saved search with name '%s' selected...", documentSetName);
				break;
			}
		}
	}
	
	public static void populateGeneralConfiguration(String projectName, String envizeInstanc, String relativityConnection, String workspace, String documentSet) {
		populateGeneralConfiguration(projectName, envizeInstanc, relativityConnection, workspace);
		
		selectSavedSearchFor(DOCUMENT_SET_LOCATOR, documentSet);
	}
	
	public static long getProjectCreationBenchmark() {
		ProjectPage.gotoProjectCreationPage("Create Standard CAL Project");
		populateGeneralConfiguration(CALSTDConstants.PROJECT_NAME, CALSTDConstants.ENVIZE_INSTANCE, CALSTDConstants.RELATIVITY_INSTANCE, CALSTDConstants.WORKSPACE);
		selectSavedSearchFor(DOCUMENT_SET_LOCATOR, CALSTDConstants.DOCUMENT_SET);
		populateFieldSettingsForSAL(CALSTDConstants.PREDICTION_FIELD, CALSTDConstants.TRUE_POSITIVE_VALUE, CALSTDConstants.TRUE_NEGATIVE_VALUE);
		
		csgp.enterText(SAVED_SEARCH_FIELD_LOCATOR, CALSTDConstants.SAVED_SEARCH_NAME_SUFFIX);
		csgp.enterText(BATCH_SET_FIELD_LOCATOR, CALSTDConstants.BATCHSET_NAME_SUFFIX);
		
		openCheckpoint();
		Checkpoint.createCheckpointForPercentage(CALSTDConstants.NUMBER_OF_BATCHES, CALSTDConstants.PERCENTAGE);
		Checkpoint.close();		
		
		/*selectSavedSearchFor(JUDGEMENTAL_SAMPLE_LOCATOR, "200 of 1k");
		csgp.waitForVisibilityOf(By.className("judgementalSampleDetailsDiv"));*/
		
		csgp.tryClick(SYNTHETIC_DOCUMENT_LOCATOR);
		csgp.editText(SYNTHETIC_DOCUMENT_FIELD_LOCATOR, CALSTDConstants.SYNTHETIC_DOCUMENT);
		
		long startTime = System.currentTimeMillis();
		
		csgp.tryClick(CREATE_PROJECT_BTN_LOCATOR, PROJECT_CREATION_WINDOW);
		csgp.waitForInVisibilityOf(CREATE_PROJECT_BTN_LOCATOR, 300);
		csgp.waitForVisibilityOf(OverviewPage.DASHBOARD_LOCATOR);		
		
		long endTime = System.currentTimeMillis();
		
		System.out.println("\nCAL Standard project created with name '" + CALSTDConstants.PROJECT_NAME + "'...");
		
		return endTime - startTime;
	}

	public static void cancelProjectCreation() {
		csgp.tryClick(CANCEL_PROJECT_BTN_LOCATOR);
	}

	/**
	 * 
	 * 	@deprecated Will be removed very soon
	 * 
	 */
	
	public static void createProject(String projectName) {
		ProjectPage.gotoProjectCreationPage("Create Standard CAL Project");
		populateGeneralConfiguration(projectName, "Envize default connection", "Relativity Instance", "iControl SvP");
		selectSavedSearchFor(DOCUMENT_SET_LOCATOR, "1k Docs");
		populateFieldSettingsForSAL("dsi_review_field_05", "Very Good", "Very Bad");
		
		csgp.enterText(SAVED_SEARCH_FIELD_LOCATOR, "SS");
		csgp.enterText(BATCH_SET_FIELD_LOCATOR, "BS");
		
		openCheckpoint();
		Checkpoint.createCheckpointForPercentage(5, 90);
		Checkpoint.close();		
		
		selectSavedSearchFor(JUDGEMENTAL_SAMPLE_LOCATOR, "200 of 1k");
		csgp.waitForVisibilityOf(By.className("judgementalSampleDetailsDiv"));
		csgp.waitFor(10);
		
		csgp.tryClick(CREATE_PROJECT_BTN_LOCATOR, PROJECT_CREATION_WINDOW);
		csgp.waitForInVisibilityOf(CREATE_PROJECT_BTN_LOCATOR, 300);
		csgp.waitForVisibilityOf(OverviewPage.DASHBOARD_LOCATOR);		
		
		System.out.println("\nCAL Standard project created with name '" + projectName + "'...");
	}
	
	public static void populateProjectData(ProjectInfo calStdInfo){
		populateGeneralConfiguration(calStdInfo.getProjectName(), calStdInfo.getEnvizeInstance(), 
										calStdInfo.getRelativityInstance(), calStdInfo.getWorkspace());

		selectSavedSearchFor(DOCUMENT_SET_LOCATOR, calStdInfo.getDocumentSet());
		populateFieldSettingsForSAL(calStdInfo.getPredictionField(), calStdInfo.getTruePositive(), calStdInfo.getTrueNegative());

		csgp.enterText(SAVED_SEARCH_FIELD_LOCATOR, calStdInfo.getSavedSearchName());
		csgp.enterText(BATCH_SET_FIELD_LOCATOR, calStdInfo.getBatchSetName());

		openCheckpoint();
		Checkpoint.createCheckpointForPercentage(5, 90);
		Checkpoint.close();

		String judgementalSmpl = calStdInfo.getJudgementalSampleName();
		String syntheticDocs = calStdInfo.getSyntheticDocument();
		if( judgementalSmpl != null && judgementalSmpl.equals("") == false){
			selectSavedSearchFor(JUDGEMENTAL_SAMPLE_LOCATOR, calStdInfo.getJudgementalSampleName());
			csgp.waitForVisibilityOf(By.className("judgementalSampleDetailsDiv"));
			csgp.waitFor(10);
		}else if( syntheticDocs != null && syntheticDocs.equals("") == false){
			csgp.tryClick(SYNTHETIC_DOCUMENT_LOCATOR);
			csgp.enterText(SYNTHETIC_DOCUMENT_FIELD_LOCATOR, syntheticDocs);
		}else{
			throw new SkipException("Missing SEED DOCUMENTS - either 'Judgemnetal Sample' or 'Synthetic Documents' should be provided to create a new project.");
		}

	}

	public static void clickCreateProjectBtn(){
		csgp.tryClick(CREATE_PROJECT_BTN_LOCATOR, PROJECT_CREATION_WINDOW);
		csgp.waitForInVisibilityOf(CREATE_PROJECT_BTN_LOCATOR, 300);
		csgp.waitForVisibilityOf(OverviewPage.DASHBOARD_LOCATOR);
	}

	public static void createProject(ProjectInfo calStdInfo){
		ProjectPage.gotoProjectCreationPage("Create Standard CAL Project");
		
		populateProjectData(calStdInfo);
		
		clickCreateProjectBtn();

		System.out.println("\nCAL Standard project created with name '" + calStdInfo.getProjectName() + "'...");
	}
}