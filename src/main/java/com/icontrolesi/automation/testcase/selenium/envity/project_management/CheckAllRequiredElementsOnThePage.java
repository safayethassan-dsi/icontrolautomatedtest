package com.icontrolesi.automation.testcase.selenium.envity.project_management;

import java.util.List;

import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.envity.common.ManageProjectsPage;
import com.icontrolesi.automation.testcase.selenium.envity.common.ProjectPage;

import org.testng.annotations.Test;

public class CheckAllRequiredElementsOnThePage extends TestHelper{
    @Test(description = "Verify all the requierd columns are displayed appropriately")
    public void test_c1877_CheckAllRequiredElementsOnThePage(){
        ProjectPage.gotoProjectAdministrationPage();
        
        String pageTitle = getText(ManageProjectsPage.PAGE_TITLE_LOCATOR);

        softAssert.assertEquals(pageTitle, "Manage Projects", "1. Page Title displayed correctly:: ");

        List<String> columnHeaders = getListOfItemsFrom(ManageProjectsPage.PROJECT_CLUMN_HEADER, item -> item.getText());

        softAssert.assertEquals(columnHeaders.get(0), "Project", "3. Project Column exists:: ");
        softAssert.assertEquals(columnHeaders.get(1), "Workspace", "3. Workspace column exists:: ");
        softAssert.assertEquals(columnHeaders.get(2), "Project Type", "3. Project Type column exists:: ");
        softAssert.assertEquals(columnHeaders.get(3), "Last Modified", "3. Last Modified column exists:: ");
        softAssert.assertEquals(columnHeaders.get(4), "Job Status", "3. Job Status column exists:: ");
        softAssert.assertEquals(columnHeaders.get(5), "Application Instance", "3. Application Status column existes:: ");
        softAssert.assertEquals(columnHeaders.get(6), "", "3. 7th column is unnamed:: ");
        softAssert.assertEquals(columnHeaders.get(7), "", "3. 8th column is unnamed:: ");

        softAssert.assertAll();
    }
}