package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Frame;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.ReviewTemplatePane;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Utils;

import com.icontrolesi.automation.platform.util.Logger;
/**
*
* @author Shaheed Sagar
*
*   DB: DEM0 
	(1) Go to search page, run a search, Choose Default View Layout Option  
	(2) Select the Undock button in the  upper-right corner of the Document Viewer Window 
	(3) Make sure all the tabs in document view frame work both in undocked/docked mode. 
	(4) Modify and then post data confirming that changes are kept.   
	(5) "Code" 10-20 documents, verifying: 
	(a) That the correct document loads quickly in the undocked window 
	(b) That Recenseo saves coding changes and displays them correctly in the grid view. 
	(6) Navigate between documents and pages in an imaged document in both undocked/docked mode.  
	(7) Test this with different document types - native word/pdf/excel/ppt with Recenseo viewer to confirm the RIGHT document loads in viewer and coding frame each time.
*/
public class Dbd65DualMonitor extends TestHelper{
	private long exepctedLoadTime = 40000;
	
	@Test
	public void test_c247_Dbd65DualMonitor(){
		handleDbd65DualMonitor(driver);
	}

	private void handleDbd65DualMonitor(WebDriver driver){
		SearchPage sp = new SearchPage();

		new SelectRepo(AppConstant.DEM0);
		
		DocView.openPreferenceWindow();
		DocView.checkSelectedColumns("Document ID");
		DocView.closePreferenceWindow();

		switchToDefaultFrame();
		
		SearchPage.setWorkflow("Search");
		
		performSearch();
        
        DocView docView = new DocView();
        
        docView.clickUndockButton(driver, sp);
        Utils.waitMedium();
         
        String[] windows = docView.getChildrenWindows(driver, sp);
        
        driver.switchTo().window(windows[1]);
        
    	selectViewByLink(driver, "Image");
    	boolean isImageViewOk = waitForElement(DocView.imageViewLocator).getAttribute("class").contains("ui-state-active");
    	softAssert.assertTrue(isImageViewOk);
    	switchToDefaultFrame();
    	selectViewByLink(driver, "Native");
    	boolean isNativeViewOk = waitForElement(DocView.nativeViewLocator).getAttribute("class").contains("ui-state-active");
    	softAssert.assertTrue(isNativeViewOk);
    	switchToDefaultFrame();
    	selectViewByLink(driver, "Text");
    	boolean isTextViewOk = waitForElement(DocView.textViewLocator).getAttribute("class").contains("ui-state-active");
    	softAssert.assertTrue(isTextViewOk);
    	switchToDefaultFrame();
    	selectViewByLink(driver, "History");
    	boolean isreviewHistoryOk = waitForElement(DocView.historyViewLocator).getAttribute("class").contains("ui-state-active");
    	softAssert.assertTrue(isreviewHistoryOk);
    	
    	docView.clickDockButtonOnUndockedWindow(driver, sp);
    	Utils.waitMedium();
    	driver.switchTo().window(windows[0]);
    	driver.switchTo().defaultContent();
    	waitForFrameToLoad(driver, Frame.MAIN_FRAME);
    
    	selectViewByLink(driver, "Imaged");
    	isImageViewOk = waitForElement(DocView.imageViewLocator).getAttribute("class").contains("ui-state-active");
    	softAssert.assertTrue(isImageViewOk);
    	selectViewByLink(driver, "Native");
    	isNativeViewOk = waitForElement(DocView.nativeViewLocator).getAttribute("class").contains("ui-state-active");
    	softAssert.assertTrue(isNativeViewOk);
    	selectViewByLink(driver, "Text");
    	isTextViewOk = waitForElement(DocView.textViewLocator).getAttribute("class").contains("ui-state-active");
    	softAssert.assertTrue(isTextViewOk);
    	selectViewByLink(driver, "History");
    	isreviewHistoryOk = waitForElement(DocView.historyViewLocator).getAttribute("class").contains("ui-state-active");
    	softAssert.assertTrue(isreviewHistoryOk);
    	
        switchToDefaultFrame();
        waitForFrameToLoad(driver, Frame.MAIN_FRAME);
        
        String oldMsg = getText(DocView.commentInCodingTemplate);
        editText(DocView.commentInCodingTemplate, "edited");
        docView.clickPostButton();
        DocView.clickOnDocument(driver, 1);
        
        driver.switchTo().defaultContent();
        waitForFrameToLoad(driver, Frame.MAIN_FRAME);
        
        softAssert.assertEquals(getText(DocView.commentInCodingTemplate), "edited");
        
        editText(DocView.commentInCodingTemplate, oldMsg);
        docView.clickPostButton();
        
        for(int i=1; i <= 10;i++){
        	long startTime = System.currentTimeMillis();
        	DocView.clickOnDocument(driver, i);
        	waitFor(driver, 15);
        	long endTime = System.currentTimeMillis();
        	long executionTime = endTime - startTime;
        	
        	WebElement tdElement = getElements(By.cssSelector("td[aria-describedby='grid_inv_id']")).get(i - 1);
            String documentId = tdElement.getText();
            String docIdInViewer = docView.getDocumentId();
            System.out.println( Logger.getLineNumber() + "documentId: "+documentId.trim());
            System.out.println( Logger.getLineNumber() + "docView.getDocumentId(): "+docIdInViewer.trim());
            System.out.println( Logger.getLineNumber() + "Loading time: "+executionTime);
            
            softAssert.assertEquals(documentId, docIdInViewer, "5(a) Correct document loads...:: ");
            softAssert.assertTrue(executionTime <= exepctedLoadTime, "5(a) And The document loads quickly...:: ");
            
            driver.switchTo().defaultContent();
            waitForFrameToLoad(driver, Frame.MAIN_FRAME);
            waitForVisibilityOf(driver, By.id("comm"));
            oldMsg = getText(DocView.commentInCodingTemplate);
            editText(DocView.commentInCodingTemplate, "edited");
            ReviewTemplatePane.saveDocument();
            Utils.waitShort();
            
            driver.switchTo().defaultContent();
            waitForFrameToLoad(driver, Frame.MAIN_FRAME);
            
            softAssert.assertEquals(getText(DocView.commentInCodingTemplate), "edited", "5(b) That Recenseo saves coding changes and displays them correctly in the grid view:: ");
            editText(DocView.commentInCodingTemplate, oldMsg);
            ReviewTemplatePane.saveDocument();
        }
        
        docView.selectView(driver, sp, "Image");
        docView.clickUndockButton(driver, sp);
        windows = docView.getChildrenWindows(driver, sp);
        driver.switchTo().window(windows[1]);
        docView.selectView(driver, sp, "Image");
        Utils.waitMedium();
        docView.clickDockButtonOnUndockedWindow(driver, sp);
        
        softAssert.assertAll();
 	}
	
	
	   public void selectViewByLink(WebDriver driver, String viewPanelLinkToClick){
		 	List<WebElement> bottomMenuItems = getElements(By.cssSelector("#menu > li > a > span"));
		 	
		 	for(WebElement item : bottomMenuItems){
		 		if(item.getText().equals(viewPanelLinkToClick)){
		 			item.click();
		 			System.out.println( Logger.getLineNumber() + "\n"+viewPanelLinkToClick+" clicked...");
		 			(new DocView()).waitForPageLoad(driver);
		 			waitFor(driver, 10);
		 		}
		 	}
		 	
		 	
		 	System.out.println( Logger.getLineNumber() + "\n"+viewPanelLinkToClick+" view selected...");
		}
}