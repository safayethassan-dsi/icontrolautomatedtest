package com.icontrolesi.automation.testcase.selenium.envity.common;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.testcase.selenium.envity.common.CALSmplInfo.CALSmplInfoBuilder;
import com.icontrolesi.automation.testcase.selenium.envity.common.CALStdInfo.CALStdInfoBuilder;
import com.icontrolesi.automation.testcase.selenium.envity.common.SALInfo.SALInfoBuilder;

public class ProjectInfoLoader {
    Properties props = new Properties();  
    
    
    /**
     * 
     * @param projectType   a string describing the type of project i.e.:- sal for SAL Project, cal_smpl for CAL Simplified Project and cal_std for CAL Standard Project
     * 
     *  {@value sal, cal_smpl, cal_std}
     */
    public ProjectInfoLoader(String projectType){
        InputStream input = null;
        try {
            input = new FileInputStream(
                    String.format("%s%s/%s%s", AppConstant.RESOURCE_FOLDER, "projectData", projectType, ".properties"));
            props.load(input);
        } catch (IOException ioe) {
            System.err.println("Error:: cannot load specified project info file");
        }
    }

    /**
     * 
     * @param projectType
     * @return ProjectInfo
     * 
     * @deprecated
     */
    public static ProjectInfo load(final String projectType){
        Properties props = new Properties();
        InputStream input = null;

        ProjectInfo projectInfo = null;

        try {
            input = new FileInputStream(String.format("%s%s/%s%s", AppConstant.RESOURCE_FOLDER, "projectData", projectType, ".properties"));
            props.load(input);
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        
        SimpleDateFormat sdf = new SimpleDateFormat("ddMMYYY_hhmm");
        String projectName = String.format("%s_%s", props.getProperty("projectName"), sdf.format(new Date()));

        if(projectType.equals("sal")){
            projectInfo = new SALInfo.SALInfoBuilder()
                                     .setProjectName(projectName)
                                     .setEnvizeInstance(props.getProperty("envizeInstance"))
                                     .setRelativityInstance(props.getProperty("relativityInstance"))
                                     .setWorkspace(props.getProperty("workspace"))
                                     
                                     .setPredictionField(props.getProperty("predictionField"))
                                     .setTruePositive(props.getProperty("truePositive"))
                                     .setTrueNegative(props.getProperty("trueNegative"))
                                     .setDocumentIdentifier(props.getProperty("documentIdentifier"))

                                     .setSavedSearchName(props.getProperty("savedSearchName"))
                                     .setBatchSetName(props.getProperty("batchSetName"))
                                     .setBatchPrefix(props.getProperty("batchPrefix"))
                                     .setBatchSize(props.getProperty("batchSize"))

                                     .setJudgmentalSampleName(props.getProperty("judgmentalSampleName"))
                                     
                                     .build();

        }else if(projectType.equals("cal_smpl")){
            projectInfo = new CALSmplInfo.CALSmplInfoBuilder()
                                    .setProjectName(projectName)
                                    .setEnvizeInstance(props.getProperty("envizeInstance"))
                                    .setRelativityInstance(props.getProperty("relativityInstance"))
                                    .setWorkspace(props.getProperty("workspace"))

                                    .setSavedSearchName(props.getProperty("savedSearchName"))
                                    .setPredictionField(props.getProperty("predictionField"))
                                    .setTruePositive(props.getProperty("truePositive"))
                                    .setTrueNegative(props.getProperty("trueNegative"))
                                    .setStoppingPoint(props.getProperty("stoppingPoint"))
                                    .setRandomSample(props.getProperty("randomSample"))
                                    .setSyntheticDocText(props.getProperty("syntheticDocText"))
                                    
                                    .build();

        }else if(projectType.equals("cal_std")){
            projectInfo = new CALStdInfo.CALStdInfoBuilder()
                                    .setProjectName(projectName)
                                    .setEnvizeInstance(props.getProperty("envizeInstance"))
                                    .setRelativityInstance(props.getProperty("relativityInstance"))
                                    .setWorkspace(props.getProperty("workspace"))
                                    .setDocumentSet(props.getProperty("documentSet"))

                                    .setPredictionField(props.getProperty("predictionField"))
                                    .setTruePositive(props.getProperty("truePositive"))
                                    .setTrueNegative(props.getProperty("trueNegative"))

                                    .setSavedSearchName(props.getProperty("savedSearchName"))
                                    .setBatchSetName(props.getProperty("batchSetName"))

                                    .setJudgementalSample(props.getProperty("judgementalSample"))
                                    .setSyntheticDocument(props.getProperty("syntheticDocument"))

                                    .build();
        }

        return projectInfo;
    }

    /**
     *      Read information from configuration file and build an object of type SALInfoBuilder. Afterwards anybody can change its info whenever needed.     
     * 
     *      @return  SALInfoBuilder
     */

    public SALInfoBuilder getSALInfoBuilder(){
        SimpleDateFormat sdf = new SimpleDateFormat("ddMMYYY_hhmm");
        String projectName = String.format("%s_%s", this.props.getProperty("projectName"), sdf.format(new Date()));

        SALInfoBuilder salInfoBuilder = new SALInfo.SALInfoBuilder()
                                    .setProjectName(projectName)
                                    .setEnvizeInstance(this.props.getProperty("envizeInstance"))
                                    .setRelativityInstance(this.props.getProperty("relativityInstance"))
                                    .setWorkspace(this.props.getProperty("workspace"))

                                    .setPredictionField(this.props.getProperty("predictionField"))
                                    .setTruePositive(this.props.getProperty("truePositive")).setTrueNegative(this.props.getProperty("trueNegative"))
                                    .setDocumentIdentifier(this.props.getProperty("documentIdentifier"))

                                    .setSavedSearchName(this.props.getProperty("savedSearchName"))
                                    .setBatchSetName(this.props.getProperty("batchSetName")).setBatchPrefix(this.props.getProperty("batchPrefix"))
                                    .setBatchSize(this.props.getProperty("batchSize"))

                                    .setJudgmentalSampleName(this.props.getProperty("judgmentalSampleName"))
                                    .setSavedSearchNameForPopulatioin(this.props.getProperty("savedSearchNameForPopulatioin"))
                                    .setJudgmentalSampleName(this.props.getProperty("judgmentalSampleName"));
                                    
        return salInfoBuilder;
    }

    public SALInfo loadSALInfo(){
        return getSALInfoBuilder().build();
    }

    /**
     *      Read information from configuration file and build an object of type CALSmplInfoBuilder. Afterwards anybody can change its info whenever needed.     
     *      @return
     */
    public CALSmplInfoBuilder getCALSmplInfoBuilder(){
        SimpleDateFormat sdf = new SimpleDateFormat("ddMMYYY_hhmm");
        String projectName = String.format("%s_%s", this.props.getProperty("projectName"), sdf.format(new Date()));

        CALSmplInfoBuilder calSmplInfoBuilder = new CALSmplInfo.CALSmplInfoBuilder()
                                        .setProjectName(projectName)
                                        .setEnvizeInstance(this.props.getProperty("envizeInstance"))
                                        .setRelativityInstance(this.props.getProperty("relativityInstance"))
                                        .setWorkspace(this.props.getProperty("workspace"))

                                        .setSavedSearchName(this.props.getProperty("savedSearchName"))
                                        .setPredictionField(this.props.getProperty("predictionField"))
                                        .setTruePositive(this.props.getProperty("truePositive")).setTrueNegative(this.props.getProperty("trueNegative"))
                                        .setStoppingPoint(this.props.getProperty("stoppingPoint")).setRandomSample(this.props.getProperty("randomSample"))
                                        .setSyntheticDocText(this.props.getProperty("syntheticDocText"));

        return calSmplInfoBuilder;
    }

    public CALSmplInfo loadCALSmplInfo(){
        return getCALSmplInfoBuilder().build();
    }

    public CALStdInfoBuilder getCALStdInfoBuilder(){
        SimpleDateFormat sdf = new SimpleDateFormat("ddMMYYY_hhmm");
        String projectName = String.format("%s_%s", this.props.getProperty("projectName"), sdf.format(new Date()));

        CALStdInfoBuilder calStdInfoBuilder = new CALStdInfo.CALStdInfoBuilder()
                                    .setProjectName(projectName)
                                    .setEnvizeInstance(this.props.getProperty("envizeInstance"))
                                    .setRelativityInstance(this.props.getProperty("relativityInstance"))
                                    .setWorkspace(this.props.getProperty("workspace"))
                                    .setDocumentSet(this.props.getProperty("documentSet"))

                                    .setPredictionField(this.props.getProperty("predictionField"))
                                    .setTruePositive(this.props.getProperty("truePositive")).setTrueNegative(this.props.getProperty("trueNegative"))

                                    .setSavedSearchName(this.props.getProperty("savedSearchName"))
                                    .setBatchSetName(this.props.getProperty("batchSetName"))

                                    .setJudgementalSample(this.props.getProperty("judgementalSample"))
                                    .setSyntheticDocument(this.props.getProperty("syntheticDocument"));

        return calStdInfoBuilder;                                   
    }

    public CALStdInfo loadCALStdInfo(){
        return getCALStdInfoBuilder().build();
    }
}