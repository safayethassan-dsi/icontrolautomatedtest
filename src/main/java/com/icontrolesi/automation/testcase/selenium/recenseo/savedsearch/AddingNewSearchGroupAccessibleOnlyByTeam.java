package com.icontrolesi.automation.testcase.selenium.recenseo.savedsearch;

import java.util.Date;

import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchesAccordion;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;

public class AddingNewSearchGroupAccessibleOnlyByTeam extends TestHelper{
	String savedSearchGroupName = "AddingNewSearchGroupAccessibleOnlyByTeam_" + new Date().getTime();
	
	String finalStepMsg = "7. New Search Group is created according to the user provided parameter in the �Add New group� dialog box ( %s ):: ";
	
	@Test
	public void test_c22_AddingNewSearchGroupAccessibleOnlyByTeam(){
		handleAddingNewSearchGroupAccessibleOnlyByTeam();
	}
	
	private void handleAddingNewSearchGroupAccessibleOnlyByTeam(){
		new SelectRepo(AppConstant.DEM0);
    	
    	SearchPage.setWorkflow("Search");
    	
    	SearchesAccordion.open();
    	SearchesAccordion.createTopLevelGroup(savedSearchGroupName, "Team");
    	
    	SearchesAccordion.selectFilter("All");
    	
    	SearchesAccordion.searchForSavedSearch(savedSearchGroupName);
    	
    	int totalGroupFound = SearchesAccordion.getSavedSearchCount(savedSearchGroupName);
    	
    	softAssert.assertEquals(totalGroupFound, 1, String.format(finalStepMsg, "Total number count matched"));
    	
    	//hoverOverElement(SavedSearchAccordion.historyItemsLocation);
    	
    	SearchesAccordion.openEditSearchGroupWindow(savedSearchGroupName);
    	
    	String savedSearchPermissionFound = getSelectedItemFroDropdown(SearchesAccordion.savedGroupPermissionLocator);
    	
    	String saveSearchGroupNameFound = getText(SearchesAccordion.savedGroupNameLocator);
    	
    	softAssert.assertEquals(saveSearchGroupNameFound, savedSearchGroupName, String.format(finalStepMsg, "Saved Search name matched"));
    	
    	softAssert.assertEquals(savedSearchPermissionFound, "Team", String.format(finalStepMsg, "Saved Search permission [Mine] matched"));
    	
    	softAssert.assertAll();
	}
}
