package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Frame;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.ManageTags;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.ReviewTemplatePane;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;

import com.icontrolesi.automation.platform.util.Logger;


public class HistViewMetaView extends TestHelper{
@Test
public void test_c231_HistViewMetaView(){
	//loginToRecenseo();
	handleHistViewMetaView(driver);
}


private void handleHistViewMetaView(WebDriver driver){
	new SelectRepo(AppConstant.DEM0);
	
	waitForFrameToLoad(Frame.MAIN_FRAME);
	
	performSearch();
	
	List<WebElement> girdTopPagerLeftMenuItems = getElements(By.cssSelector("#grid_toppager_left > table > tbody > tr > td"));
	System.err.println("Size: " + girdTopPagerLeftMenuItems.size());
	for(WebElement menuItem : girdTopPagerLeftMenuItems){
		System.err.println(menuItem.getAttribute("title")+"****");
		if(menuItem.getAttribute("title").trim().equals("Tags")){
			menuItem.findElement(By.cssSelector("div > span")).click();
			waitFor(20);
			System.out.println( Logger.getLineNumber() + "Tags menuItem clicked...");
			break;
		}
	}
	
	switchToDefaultFrame();
	waitForFrameToLoad("tag-manageIframe");
	
	waitForNumberOfElementsToBeGreater(By.cssSelector("#tagManage-picklist > ul > li"), 0);
	
	//ManageTags.addDocumentsToTag("00000_PF SAMPLES");
	ManageTags.addDocumentsToTag("AtiqBhai");
	
	//Click the Save button
	switchToDefaultFrame();
	getElement(By.id("done")).click();
		
		waitFor(3);
		
		//dc.selectView(sp, "History");
		DocView.selectView("History");
		
		waitFor(20);
	
	SearchPage.waitForElement("tr[class='Category-Added']", "css-selector");
	List<WebElement> categoryAdded= getElements(By.cssSelector("tr[class='Category-Added']"));
	WebElement lastAdded= categoryAdded.get(categoryAdded.size()-1).findElement(By.cssSelector("td"));
	
	System.out.println( Logger.getLineNumber() + "Last Added text is: "+lastAdded.getText());
	
	//boolean isTagHistoryCaptured = lastAdded.getText().trim().equals("AtiqBhai");
	boolean isTagHistoryCaptured = lastAdded.getText().trim().equals("20140901-Internal");
	

	Assert.assertTrue(isTagHistoryCaptured, "(5) Confirm Recent Tag History is Captured:: ");
	
	//Click the Metadata Tab
	ReviewTemplatePane rt= new ReviewTemplatePane();
	
	rt.switchTabReviewPane("Metadata");
	
	//Check if the information under the above tab looks correct
	String[] metaDataValues= rt.getMetaDataInfo();
	
	boolean isMetadataO = metaDataValues[0].equalsIgnoreCase("Fields") && metaDataValues[1].equalsIgnoreCase("Values");
	
	Assert.assertTrue(isMetadataO, "(7) Make sure Information looks correct and no errors come up when viewing:: ");
	}
}