package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Frame;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.ManageDocumentSecurity;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SprocketMenu;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.UserManager;

/**
 *
 * @author Shaheed Sagar
 *
 *  "*Will Require Two Users (or two User IDs, an ""Admin"" ID and a ""Test User"" ID) 
	(1) Log into DEM4 and Select Administrative Settings under Settings Menu and Choose Manage Users 
	(2) Click on the Teams tab 
	(3) Confirm the ""Test User"" ID  is still in  ""Document Security"" Team  
	(4) Return to Search Page, and search for Document 298672 
	(5) Click on the Thread tab  
	(6) Click to ""Complete Doc Family"" in the Upper Left Corner and confirm documents are added 
	(7) Select ""Manage Document Security"" in the Actions Menu and block document 298678 and 299135 from Document Security Team  
	(8) Confirm that when ""Test User"" ID searches for 298672 and Clicks on the Thread tab, Document 298678 does not appear  
	(9) Confirm that when ""Test User"" ID Clicks to ""Complete Doc Family"" that Document 299135 and 298679 do not appear in the list"

*/

public class DSEmailThread extends TestHelper{
    @Test
    public void test_c413_DSEmailThread(){
    	//loginToRecenseo();
    	handleDSEmailThread(driver);
    }

    private void handleDSEmailThread(WebDriver driver){
    	new SelectRepo(AppConstant.DEM4);
    	
    	SearchPage.setWorkflow("Search");
    	
        SprocketMenu.openUserManager();
        UserManager.switchTab("Teams");
        UserManager.selectTeam("Document Security");
        
        boolean isUserExist = UserManager.isUserExists(AppConstant.USER2);
        
        softAssert.assertTrue(isUserExist, "(3) Confirm the \"Test User\" ID  is still in  \"Document Security\" Team:: ");
        
        SearchPage.goToDashboard();
        
        waitForFrameToLoad(Frame.MAIN_FRAME);
        
        SearchPage.addSearchCriteria("Document ID");
        editText(By.cssSelector("input.CriterionValue"), " 298672");
        
        performSearch();
        
        new DocView().clickActionMenuItem("Manage Document Security");
        switchToFrame(1);
        ManageDocumentSecurity.unBlockTeam("Document Security");	//unblock all documents first
        ManageDocumentSecurity.closeWindow();  
        
        DocView.selectViewByLink("Thread");
        
        switchToDefaultFrame();
        waitForFrameToLoad(Frame.MAIN_FRAME);
        
        int documentsAfterClickingThread = DocView.getDocmentCount();
        
        DocView.clickOnCompleteDocFamily();
        
        int documentsAfterClickingDocFamily = DocView.getDocmentCount();
        
        softAssert.assertTrue(documentsAfterClickingDocFamily > documentsAfterClickingThread, "(6) Click to \"Complete Doc Family\" in the Upper Left Corner and confirm documents are added:: ");
        
        DocView.selectDocumentsWitIDs("298678", "299135");
        
        new DocView().clickActionMenuItem("Manage Document Security");
        
        List<WebElement> frameList = getElements(By.tagName("iframe"));
        
        switchToFrame(frameList.get(frameList.size() - 1));
        
        ManageDocumentSecurity.blockTeam("Document Security");
        
        ManageDocumentSecurity.closeWindow();    
        
        SearchPage.logout();
		
        performLoginWithCredential(AppConstant.USER2, AppConstant.PASS2);	
        
        new SelectRepo(AppConstant.DEM4);
       
        SearchPage.setWorkflow("Search");
        
        SearchPage.addSearchCriteria("Document ID");
       
        //editText(By.cssSelector("input.CriterionValue"), " 298672");
        SearchPage.setCriterionValue(0, "298672");
        
        performSearch();
        
        DocView.selectViewByLink("Thread");
        switchToDefaultFrame();
        waitForFrameToLoad(Frame.MAIN_FRAME);
        
        boolean isDocumentIDDisappeared = (DocView.isDocumentIDFound("298678") == false);
        
        softAssert.assertTrue(isDocumentIDDisappeared, "(8) Confirm that when \"Test User\" ID searches for 298672 and Clicks on the Thread tab, Document 298678 does not appear:: ");
        
        DocView.clickOnCompleteDocFamily(driver);
        
        boolean isDocumentIDDisappearedFromDocFamily_1 = (DocView.isDocumentIDFound("299135") == false);
        boolean isDocumentIDDisappearedFromDocFamily_2 = (DocView.isDocumentIDFound("298679") == false);
        
        softAssert.assertTrue(isDocumentIDDisappearedFromDocFamily_1 && isDocumentIDDisappearedFromDocFamily_2, "(9) Confirm that when \"Test User\" ID Clicks to \"Complete Doc Family\" that Document 299135 and 298679 do not appear in the list:: ");
        
        SearchPage.logout();
        performLoginToRecenseo();
        
        new SelectRepo(AppConstant.DEM4);

    	SearchPage.setWorkflow("Search");
        
        SearchPage.addSearchCriteria("Document ID");
        //editText(By.cssSelector("input.CriterionValue"), " 298672");
        SearchPage.setCriterionValue(0, "298672");
        
        performSearch();
        
        waitFor(10);
        DocView.selectViewByLink("Thread");
        switchToDefaultFrame();
        waitForFrameToLoad(Frame.MAIN_FRAME);
        DocView.clickOnCompleteDocFamily(driver);
        
        DocView.selectDocumentsWitIDs("298678", "299135");
        
        new DocView().clickActionMenuItem("Manage Document Security");
        
        driver.switchTo().frame(getElement(By.cssSelector("iframe[src^='javascript']")));
        
        ManageDocumentSecurity.unBlockTeam("Document Security");
        
        ManageDocumentSecurity.closeWindow();    
       
        softAssert.assertAll();
    }
}