package com.icontrolesi.automation.testcase.selenium.recenseo.savedsearch;

import java.util.Date;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Frame;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchesAccordion;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;

public class OpeningMultipleSavedSearchToTheSearchBuilderNotInAnySearchGroup extends TestHelper{
	String savedSearchName = "OpeningMultipleSavedSearchToTheSearchBuilderNotInAnySearchGroup" + new Date().getTime();
	String savedSearchName01 = savedSearchName + 1;
	String savedSearchName02 = savedSearchName + 2;
	
	String stepMsg7 = "7. Recenseo displays all the documents corresponding to the selected 'Saved' search' on the View Documents Page(%s):: ";
	
	@Test
	public void test_c18_OpeningMultipleSavedSearchToTheSearchBuilderNotInAnySearchGroup(){
		handleOpeningASingleSavedSearchToTheSearchBuilderNotInAnySearchGroup();
	}
	
	private void handleOpeningASingleSavedSearchToTheSearchBuilderNotInAnySearchGroup(){
		new SelectRepo(AppConstant.DEM0);
		
		DocView.openPreferenceWindow();
	    DocView.checkSelectedColumns("Author");
	    DocView.clickCrossBtn();
    	
    	SearchPage.setWorkflow("Search");
    	
    	SearchPage.addSearchCriteria("Custodian");
    	editText(By.cssSelector("input.CriterionValue"), "jeff");
    	
    	SearchPage.createSavedSearch(savedSearchName01, "", "");
    	
    	SearchPage.clearSearchCriteria();
    	
    	SearchPage.addSearchCriteria("Title");
    	editText(By.cssSelector("input.CriterionValue"), "jeff");
    	
    	SearchPage.createSavedSearch(savedSearchName02, "", "");
    	
    	SearchesAccordion.open();
    	SearchesAccordion.selectFilter("All");
    	SearchesAccordion.searchForSavedSearch(savedSearchName);
    	
    	List<WebElement> savedSearchList = getElements(SearchesAccordion.savedSearchItemCheckboxLocator);
    	savedSearchList.get(0).click();
    	savedSearchList.get(2).click();
    	
    	SearchesAccordion.clickOpenBtnForViewSavedSearch();
    
    	int totalDocs = DocView.getDocmentCount();
    	
    	List<WebElement> custodianList = getElements(DocView.documentCustodianColumnLocator);
    	
    	boolean isCustodianColumnOk = true;
    	
    	for(WebElement custodian : custodianList){
    		if(getText(custodian).equals("jeff") == false){
    			isCustodianColumnOk = false;
    			break;
    		}
    	}
    	
    	List<WebElement> titleList = getElements(DocView.documentTitleColumnLocator);
    	
    	boolean isTitleColumnOk = true;
    	
    	for(WebElement title : titleList){
    		String titleValue = getText(title);
    		if(titleValue.equals("") == false && titleValue.equals("jeff") == false){
    			isTitleColumnOk = false;
    			break;
    		}
    	}
    	
    	
    	softAssert.assertEquals(totalDocs, 39397, String.format(stepMsg7, "Total Document number verification"));
    	
    	softAssert.assertTrue(isCustodianColumnOk, String.format(stepMsg7,"Custodian Column check"));
    	softAssert.assertTrue(isTitleColumnOk, String.format(stepMsg7,"Title coulumn check"));
    	
    	SearchPage.goToDashboard();
    	waitForFrameToLoad(Frame.MAIN_FRAME);
    	
    	SearchesAccordion.open();
    	SearchesAccordion.deleteSavedSearch(savedSearchName);
    	
    	softAssert.assertAll();
	}
}
