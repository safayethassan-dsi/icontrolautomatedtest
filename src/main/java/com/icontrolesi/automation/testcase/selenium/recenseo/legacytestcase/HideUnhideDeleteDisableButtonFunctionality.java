package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Frame;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchesAccordion;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;

public class HideUnhideDeleteDisableButtonFunctionality extends TestHelper{
	@Test
	public void test_c1653_HideUnhideDeleteDisableButtonFunctionality(){
		new SelectRepo(AppConstant.DEM0);
		
		SearchPage.setWorkflow("Search");
		
		SearchesAccordion.open();
		SearchesAccordion.openSavedSearchForView("iControl_032 - Complicated Saved Search");
		
		int totalCount = DocView.getDocmentCount();
		
		softAssert.assertEquals(totalCount, 169, "2. Total count matched:: ");
		
		SearchPage.goToDashboard();
		
		hideGroup(2);
		
		performSearch();
		
		int totalDocCountAfterHidingGroup = DocView.getDocmentCount();
		
		softAssert.assertEquals(totalDocCountAfterHidingGroup, totalCount, "5. Total count matched:: ");
		
		SearchPage.goToDashboard();
		
		disableGroup(7);
		
		performSearch();
		
		int totalDocCountAfterDisablingGroup = DocView.getDocmentCount();
		
		softAssert.assertTrue(totalDocCountAfterDisablingGroup > totalDocCountAfterHidingGroup, "8. the total number of results has increased dramatically:: ");
		
		SearchPage.goToDashboard();
		
		disableGroup(7);
		
		performSearch();
		
		int totalDocCountAfterRemovingGroup = DocView.getDocmentCount();
		
		softAssert.assertEquals(totalDocCountAfterDisablingGroup, totalDocCountAfterRemovingGroup, "8. the total number of results has increased dramatically:: ");
		
		softAssert.assertAll();
	}
	
	
	void hideGroup(int groupPosition){
		waitForFrameToLoad(Frame.MAIN_FRAME);
		List<WebElement> groupHideBtnList = getElements(By.cssSelector("button[title='Hide']"));
		
		WebElement grandParentOfHideBtn = getParentOf(getParentOf(groupHideBtnList.get(groupPosition-1)));
		
		hoverOverElement(grandParentOfHideBtn);
		
		groupHideBtnList.get(groupPosition-1).click();
	}
	
	
	void disableGroup(int groupPosition){
		waitForFrameToLoad(Frame.MAIN_FRAME);
		List<WebElement> groupHideBtnList = getElements(By.cssSelector("button[title='Disable']"));
		
		WebElement grandParentOfHideBtn = getParentOf(getParentOf(groupHideBtnList.get(groupPosition-1)));
		
		hoverOverElement(grandParentOfHideBtn);
		
		groupHideBtnList.get(groupPosition-1).click();
	}
	
	void removeGroup(int groupPosition){
		waitForFrameToLoad(Frame.MAIN_FRAME);
		List<WebElement> groupHideBtnList = getElements(By.cssSelector("button[title='Remove']"));
		
		WebElement grandParentOfHideBtn = getParentOf(getParentOf(groupHideBtnList.get(groupPosition-1)));
		
		hoverOverElement(grandParentOfHideBtn);
		
		groupHideBtnList.get(groupPosition-1).click();
	}
}
