package com.icontrolesi.automation.testcase.selenium.recenseo.common;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import com.icontrolesi.automation.platform.util.AppConstant;

import com.icontrolesi.automation.platform.util.Logger;

public class FileMethods{

	
	public static final String downloadFolderPath = "C:\\Users\\Abdul Awal\\Downloads";

	
	public static boolean isFilePresentInZip(ZipFile zip, String fileName){
	     Enumeration<?> entries = zip.entries();
	     while(entries.hasMoreElements()){
	      if(((ZipEntry) entries.nextElement()).getName().contains(fileName)){
	       return true;
	      }
	     }
	     return false;
	    }
	
	public static File [] getListOfFiles(String fileMatcher){
   	 	File folder = new File(AppConstant.DOWNLOAD_DIRECTORY).getParentFile();
        File [] files = folder.listFiles(new FilenameFilter() {
			
			@Override
			public boolean accept(File dir, String name) {
				
				return name.matches(fileMatcher);
			}
		});
        
        return files;
	}
	 
	public static File [] getListOfFiles(String folderPath, String fileMatcher){
	    	 File folder = new File(folderPath);
	         File [] files = folder.listFiles(new FilenameFilter() {
	 			
	 			@Override
	 			public boolean accept(File dir, String name) {
	 				Pattern pattern = Pattern.compile(fileMatcher);
	 				
	 				return pattern.matcher(name).find();
	 			}
	 		});
	    	 
	    	/*Pattern pattern = Pattern.compile(fileMatcher);
	    	 
	    	 File [] files = null;
	    	 
	    	 File [] fileList = folder.listFiles();
	    	 
	    	 for(int i = 0; i < fileList.length; i++){
	    		 System.err.println(fileList[i].getName()+"****" + pattern.matcher(fileList[0].getName()).find());
	    		 if(pattern.matcher(fileList[i].getName()).find()){
	    			 files[i] = fileList[i];
	    		 }
	    	 }
	         */
	         return files;
	}
	
	public static void deleteFiles(File [] files){
		//System.out.println( Logger.getLineNumber() + "Deleting previous zip files...");
		 if(files == null){
			 System.out.println( Logger.getLineNumber() + "No matching files to delete.");
		 }else{
			 for(File file : files){
		        	if(!file.delete()){
		        		System.out.println( Logger.getLineNumber() + "Can't be removed " + file.getAbsolutePath());
		        	}else{
		        		System.out.println( Logger.getLineNumber() + "Removed " + file.getName() + " from directory...");
		        	}
		        }
		 }
	}
	
	public File getFile(String fileType,String probableName){
		File[] files = new File(System.getProperty("user.home")+"/Downloads/").listFiles();
		//If this pathname does not denote a directory, then listFiles() returns null. 
		
		int currentYear= Calendar.getInstance().get(Calendar.YEAR);
		String Year= Integer.toString(currentYear);
		String regularExpression;
		regularExpression= probableName+Year + "\\d+.*."+fileType;

		for (File file : files) {
			String fileName = file.getName();
		    if (file.isFile() && fileName.matches(regularExpression)) {
		        return file;
		    }
		   
		}		
		return null;
	}
	
	
	public boolean checkFileFormat(File file, String[] stringsToCheck){
    	boolean allOk = true;
    	for(String s: stringsToCheck){
    		if(!file.getName().contains(s)){
    			allOk = false;
    			return allOk;
    		}
    	}
    	return allOk;
    }
	
	
	
	/**
     * 
     * @param file
     * @param lineNumber First line is lineNumber 1
     * @return
     */
    public String getSingleLineOfFile(File file,int lineNumber) {
    BufferedReader bufferedReader = null;
    	try {
			bufferedReader = new BufferedReader(new java.io.FileReader(file));
			String currentLine = "";
			int ln = 1;
			while ((currentLine = bufferedReader.readLine())!=null) {
				if(ln==lineNumber){
					return currentLine;
				}
				ln++;
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if(null != bufferedReader)
				try {
					bufferedReader.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}
    	return null;
    }
	
	
	public static void deleteFile(String fileLocation){
		File file = new File(fileLocation);
		
		if(!file.exists()){
			System.out.println( Logger.getLineNumber() + "No such file exitst!!");
		}else{
			if(file.isDirectory()){
				System.out.println( Logger.getLineNumber() + "Directory can't be deleted...");
			}else{
				file.delete();
				System.out.println( Logger.getLineNumber() + file.getName() + " deleted successfully.");
			}
		}
	}
	
	public static void deleteFileOrFolder(String folderPath){
		File file = new File(folderPath);
		
		if(file.isDirectory()){
    		if(file.list().length==0){
    		   file.delete();
    		   System.out.println( Logger.getLineNumber() + "Directory is deleted : " + file.getAbsolutePath());
    		}else{
        	   File [] files = file.listFiles();
        	   for (File f : files) {
        	      //File fileDelete = new File(f, temp);
        	      deleteFileOrFolder(f.getAbsolutePath());
        	   }
        	   if(file.list().length==0){
           	     file.delete();
        	     System.out.println( Logger.getLineNumber() + "Directory is deleted : " + file.getAbsolutePath());
        	   }
    		}

    	}else{
    		file.delete();
    		System.out.println( Logger.getLineNumber() + "File is deleted : " + file.getAbsolutePath());
    	}
    }
	
	public static boolean isFileExist(String fileLocation){
		File file = new File(fileLocation);
		
		return file.exists();
	}
	
	 public static void deleteFileWithPattern(String directory, String pattern){
	    	File [] fileList = new File(directory).listFiles();
	    	Pattern p = Pattern.compile(pattern);
	    	
	    	for(File f : fileList){
	    		if(p.matcher(f.getName()).find()){
	    			f.delete();
	    			System.err.println(f.getName() + " deleted...");
	    		}
	    	}
		}
	
	public static void main(String[] args) {
		
		deleteFiles(getListOfFiles("C:\\Users\\abdul awal\\Downloads\\", "aawal20171130\\d+\\.lfp"));
	}
}
