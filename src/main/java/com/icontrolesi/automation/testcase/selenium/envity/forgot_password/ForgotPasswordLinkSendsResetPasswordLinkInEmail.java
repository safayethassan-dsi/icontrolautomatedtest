package com.icontrolesi.automation.testcase.selenium.envity.forgot_password;

import org.openqa.selenium.By;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.envity.common.ProjectPage;

public class ForgotPasswordLinkSendsResetPasswordLinkInEmail extends TestHelper{
	@Test
	public void test_c344_ForgotPasswordLinkSendsResetPasswordLinkInEmail(){
		ProjectPage.performLogout();
		
		gotoForgotPasswordPage();
		
		enterText(ResetPassword.USERNAME_FIELD, AppConstant.USER);
		
		tryClick(ResetPassword.RESET_BTN, By.className("login-wrapper-div"));
		
		String receiver = getText(By.cssSelector("body > div > div > div:nth-child(3)")).replace("\n", "").replace("\r", "");
		
		System.out.println(receiver);
		
		softAssert.assertEquals(receiver, String.format("Mail send to: abdul.awal@dsinnovators.com"));
			
		softAssert.assertAll();
	}
}
