package com.icontrolesi.automation.testcase.selenium.envity.edit_user;

import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.envity.common.ProjectPage;
import com.icontrolesi.automation.testcase.selenium.envity.common.UsersListPage;
import org.testng.annotations.Test;

import java.util.Date;

public class CheckForRequiredField  extends TestHelper {
    private String possibleValidUserName = "AutoUser_" + new Date().getTime() + getRandomNumberInRange(10000, 999999);
    private String email = String.format("%s%s", possibleValidUserName, "@automation.com");

    @Test(description = " If any Required Field is blank, User can not be save.")
    public void test_c278_CheckForRequiredField(){
        ProjectPage.gotoUserAdministrationPage();
        UsersListPage.openCreateUserWindow();

        UsersListPage.CreateUser.perform(email, possibleValidUserName, "@3455466@EEEE13f", "@3455466@EEEE13f", "Abdul Awal", "Sazib", "Project Reviewer", "iControl");

        enterText(UsersListPage.SEARCH_FIELD_LOCATOR, possibleValidUserName);

        tryClick(UsersListPage.EDIT_USER_ICON, 2);

        UsersListPage.EditUser.fillUserEditForm("", "", "", "", "", "");

        tryClick(UsersListPage.EditUser.SAVE_BTN);

        softAssert.assertEquals(getText(UsersListPage.EditUser.EMAIL_ERR), "Please enter email address", "1) Email address not provided:: ");
        softAssert.assertEquals(getText(UsersListPage.EditUser.USERNAME_ERR), "Please enter your username", "2) Username not provided:: ");
        softAssert.assertEquals(getText(UsersListPage.EditUser.FIRSTNAME_ERR), "Please enter first name", "3) First Name not provided:: ");
        softAssert.assertEquals(getText(UsersListPage.EditUser.LASTNAME_ERR), "Please enter last name", "4) Last Name not provided:: ");
        softAssert.assertEquals(getText(UsersListPage.EditUser.COMPANY_ERR), "Please Select a Company", "5) Company not selected:: ");

        softAssert.assertAll();
    }
}
