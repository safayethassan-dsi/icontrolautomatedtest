package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.FolderAccordion;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Frame;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;

import com.icontrolesi.automation.platform.util.Logger;

public class NativeViewer extends TestHelper{
public WebElement div;

	@Test
	public void test_c253_NativeViewer(){
		handleNativeViewer(driver);
	}


	private void handleNativeViewer(WebDriver driver){
			new SelectRepo(AppConstant.DEM0);
	
			SearchPage.setWorkflow("Search");
			
			switchToDefaultFrame();
			
			DocView.openPreferenceWindow();
			DocView.switchPreferenceTab("File Type");
			
			selectViewPreferencesTo("Native");
			
			DocView.closePreferenceWindow();
			
			waitForFrameToLoad(Frame.MAIN_FRAME);
			
			FolderAccordion.open();
	      	FolderAccordion.expandFolder("Email");
	      	FolderAccordion.openFolderForView("Foster, Ryan e-mail");
			 
	     	List<WebElement> docsInSearch= getElements(By.cssSelector("td[aria-describedby='grid_mimeType']"));
	
			for(int i=0;i<docsInSearch.size();i++){	 
				 docsInSearch= getElements(By.cssSelector("td[aria-describedby='grid_mimeType']"));
				
				 docsInSearch.get(i).click();
				 waitFor(15);
				 
				 WebElement span= docsInSearch.get(i).findElement(By.cssSelector("span"));
				 System.out.println( Logger.getLineNumber() + span.getAttribute("class"));
				 String contentType= span.getAttribute("class");
				 
				//Wait for frameload
				 SearchPage.waitForElement("iframe[id='VIEWER']", "css-selector");
				   
				waitFor(10);
				 
				if(contentType.equalsIgnoreCase("HtmlImage")){
					//Get attachment count
					 List<WebElement> spanChildren= docsInSearch.get(i).findElements(By.cssSelector("span"));
					 if(spanChildren.size()==0){
					
						SearchPage.waitForFrameLoad(Frame.VIEWER_FRAME);
						String content= getElement(By.cssSelector("tr")).getText();
						System.out.println( Logger.getLineNumber() + content);
					
						Assert.assertTrue(content.contains("From"), "Verify all 36 docs launch properly in Native Viewer(Email):: ");
				 }else if(contentType.equalsIgnoreCase("XlsImage")){
					SearchPage.waitForFrameLoad(Frame.VIEWER_FRAME);
					
					Assert.assertNotEquals(getElements(By.id("sheet.1")).size(), 0, "Verify all 36 docs launch properly in Native Viewer(Excel):: ");
				}else if(contentType.equalsIgnoreCase("PdfImage")){
					SearchPage.waitForFrameLoad(Frame.VIEWER_FRAME);
						
					Assert.assertNotEquals(getElements(By.cssSelector("div[class='pdfViewer']")).size(), 0, "Verify all 36 docs launch properly in Native Viewer(PDF):: ");
				}else if(contentType.equalsIgnoreCase("PptImage")){
					SearchPage.waitForFrameLoad(Frame.VIEWER_FRAME);
					
					Assert.assertNotEquals(getElements(By.cssSelector("div[id='sheet.1.1']")).size(), 0, "Verify all 36 docs launch properly in Native Viewer(PPT):: ");
				}else if(contentType.equalsIgnoreCase("DocImage")){
				   SearchPage.waitForFrameLoad(Frame.VIEWER_FRAME);
	               
	               if(i!=29){
	            	   Assert.assertNotEquals(getElements(By.cssSelector("div[id='page.1']")), 0, "Verify all 36 docs launch properly in Native Viewer(word):: ");
	               }
		        }	
	           }else if(contentType.equalsIgnoreCase("DefaultImage")){
			
					SearchPage.waitForFrameLoad(Frame.VIEWER_FRAME);
	              
	              if(i!=34){
	            	  Assert.assertNotEquals(getElements(By.cssSelector("div[id='vector.1.1']")), 0, "Verify all 36 docs launch properly in Native Viewer(DefaultImage):: ");
				   }
				}
				switchToDefaultFrame();
				waitForFrameToLoad(Frame.MAIN_FRAME);
			}
	}
	
	
	public void selectViewPreferencesTo(String preference){
		List<WebElement> viewPreferenceList = getElements(By.cssSelector("td[aria-describedby='jqg1_viewPrefName']"));
		
		for(WebElement viewPreference : viewPreferenceList){
			if(viewPreference.getText().trim().equals(preference) == false){
				viewPreference.click();
				selectFromDrowdownByText(By.name("viewPrefName"), preference);
				
				//waitForVisibilityOf(By.id("load_jqg1"));
				waitFor(5);
			}
		}
	}	
}
