package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;

import com.icontrolesi.automation.platform.util.Logger;


public class GridWorkflowStateFunctionality extends TestHelper{
	@Test
	public void test_c125_GridWorkflowStateFunctionality(){
		handleGridWorkflowStateFunctionality(driver);
	}
	
	
	private void handleGridWorkflowStateFunctionality(WebDriver driver){
			new SelectRepo(AppConstant.DEM0);
			
			SearchPage.setWorkflow("Search");
			
			performSearch();
			
			switchToDefaultFrame();
			
			SearchPage.setWorkflowFromGridView("QC Objective Coding");
				
		    int stepPass=1;
			
		    for(int i=4;i<10;i++){	
			List<WebElement> checkboxes= getElements(By.cssSelector("input[class='cbox']"));
			WebElement trParent= checkboxes.get(i).findElement(By.xpath("parent::td/parent::tr"));
			waitFor(2);
			
			if(trParent.getAttribute("class").contains("NotInState")){
				System.out.println( Logger.getLineNumber() + trParent.getCssValue("background-color"));
				if(trParent.getCssValue("background-color").equalsIgnoreCase("rgba(221, 221, 221, 1)")){
					stepPass=1;
				}else{
					stepPass=0;  
					break;
				}
			}
		   }	
			
		   Assert.assertTrue((stepPass == 1), "2) Change your workflow and make sure Recenseo is properly [greying out] documents not in the workflow state that you change to.:: ");
	}
}
