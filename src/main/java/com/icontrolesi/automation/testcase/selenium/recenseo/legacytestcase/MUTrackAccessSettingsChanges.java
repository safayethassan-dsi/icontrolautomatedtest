package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Driver;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SprocketMenu;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.UserManager;

import com.icontrolesi.automation.platform.util.Logger;
/**
 * 
 * @author Shaheed Sagar
 * (1) Log into DEM0 and Select Administrative Settings under Settings Menu and Choose Manage Users
	(2) Select two or more users where all users are on the same team and have some (but NOT all) Access settings in common. "Load Settings" for those users.
	(3)Verify that Recenseo tracks Access changes as described here:
	(a) Verify that in the "General Access" table, for an access colored GREEN, clicking on the checkbox: (i.) Unchecks the checkbox (ii.) Clears the color GREEN, (iii.) gives a RED background to the checkbox. A 2nd click on the checkbox reverses (i.) through (iii.).
	(b) Verify that in the "General Access" table, for an access colored YELLOW, clicking on the checkbox: (i.) Unchecks the checkbox (ii.) Clears the color YELLOW, (iii.) gives a RED background to the checkbox. A 2nd click on the checkbox (i.) checks the checkbox (ii.) color the access GREEN (iii.) leaves the checkbox background as RED. A 3rd click on the checkbox (i.) leaves the checkbox checked (ii.) changes the access color from GREEN back to YELLOW (iii.) clears the RED from the background of the checkbox.
	(c) Verify that in the "Workflow State Access" table, clicking on any radio button under "No Access", "Use", "Assign": (i.) checks the radio button, and only that radio button is checked in that row (ii.) colors that radio button cell GREEN (iii.) colors the corresponding cell under "Keep Current" RED (iv.) all other cells on that row have color WHITE.
	(d) Verify that in the "Workflow State Access" table, whenever a cell under "KEEP CURRENT" is colored RED, clicking on the radio button of that RED "KEEP CURRENT" cell reverses everything that happened previously.
	(4) Change the Team selection. Red indicates you have made a change. Hit "Reset Team" next to the Team selection and verify Recenseo return the original team selection value (or blank), and removes the red.
	(5) After having made (and left) access modifications, as described in stepsabove, click on "Reset All". Verify that the access settings have ALL been reversed to how they were when you first loaded the users access settings.
 *
 */

public class MUTrackAccessSettingsChanges extends TestHelper{
	@Test(enabled = false)
	public void test_c1674_MUTrackAccessSettingsChanges(){
		handleMUTrackAccessSettingsChanges(driver);
	}

	private void handleMUTrackAccessSettingsChanges(WebDriver driver){
		Driver.setWebDriver(driver);
		// Maximize window
		Driver.getDriver().manage().window().maximize();
		System.out.println( Logger.getLineNumber() + "Inside function");
	
		SearchPage sp = new SearchPage();

		new SelectRepo(Driver.getDriver(), "Recenseo Demo DB");

		UserManager userManager = new UserManager();
		
		SprocketMenu sprocketMenu = new SprocketMenu();
		//sprocketMenu.selectSubMenu(Driver.getDriver(), sp, "Tools", "User Manager");
		
		userManager.checkUser("azaman");
		userManager.checkUser("jlumby");
		
		userManager.clickLoadSettings();
		
		userManager.setGeneralAccessWithoutSave(Driver.getDriver(), sp, "Admin Access");
		
		List<WebElement> tdElements = Driver.getDriver().findElements(By.cssSelector("td[aria-describedby='generalAccesses_access']"));
		List<WebElement> chkBoxElements = Driver.getDriver().findElements(By.cssSelector("td[aria-describedby='generalAccesses_granted']"));
		
		if(!chkBoxElements.get(0).isSelected() && tdElements.get(0).getAttribute("class").equalsIgnoreCase("accessByNone") && chkBoxElements.get(0).getAttribute("class").equalsIgnoreCase("accessEdited")){
			userManager.setGeneralAccessWithoutSave(Driver.getDriver(), sp, "Admin Access");
			tdElements = Driver.getDriver().findElements(By.cssSelector("td[aria-describedby='generalAccesses_access']"));
			chkBoxElements = Driver.getDriver().findElements(By.cssSelector("td[aria-describedby='generalAccesses_granted']"));
			
			/*if(chkBoxElements.get(0).isSelected() && tdElements.get(0).getAttribute("class").equalsIgnoreCase("accessByAll") && chkBoxElements.get(0).getAttribute("class").equalsIgnoreCase("")){
				Utils.handleResult("3(a) Verify that in the 'General Access' table, for an access colored GREEN, clicking on the checkbox: (i.) Unchecks the checkbox (ii.) Clears the color GREEN, (iii.) gives a RED background to the checkbox. A 2nd click on the checkbox reverses (i.) through (iii.).", result, true);
			}else{
				Utils.handleResult("3(a) Verify that in the 'General Access' table, for an access colored GREEN, clicking on the checkbox: (i.) Unchecks the checkbox (ii.) Clears the color GREEN, (iii.) gives a RED background to the checkbox. A 2nd click on the checkbox reverses (i.) through (iii.).", result, false);
			}*/
			
		}else{
			//Utils.handleResult("3(a) Verify that in the 'General Access' table, for an access colored GREEN, clicking on the checkbox: (i.) Unchecks the checkbox (ii.) Clears the color GREEN, (iii.) gives a RED background to the checkbox. A 2nd click on the checkbox reverses (i.) through (iii.).", result, false);
		}
		
		userManager.setGeneralAccessWithoutSave(Driver.getDriver(), sp, "Copy Previous");
		tdElements = Driver.getDriver().findElements(By.cssSelector("td[aria-describedby='generalAccesses_access']"));
		chkBoxElements = Driver.getDriver().findElements(By.cssSelector("td[aria-describedby='generalAccesses_granted']"));
		
		if(!chkBoxElements.get(5).isSelected() && tdElements.get(5).getAttribute("class").equalsIgnoreCase("accessByNone") && chkBoxElements.get(5).getAttribute("class").equalsIgnoreCase("accessEdited")){
			userManager.setGeneralAccessWithoutSave(Driver.getDriver(), sp, "Copy Previous");
			
			tdElements = Driver.getDriver().findElements(By.cssSelector("td[aria-describedby='generalAccesses_access']"));
			chkBoxElements = Driver.getDriver().findElements(By.cssSelector("td[aria-describedby='generalAccesses_granted']"));
			
			if(chkBoxElements.get(5).isSelected() && tdElements.get(5).getAttribute("class").equalsIgnoreCase("accessByAll") && chkBoxElements.get(5).getAttribute("class").equalsIgnoreCase("accessEdited")){
				userManager.setGeneralAccessWithoutSave(Driver.getDriver(), sp, "Copy Previous");
				
				tdElements = Driver.getDriver().findElements(By.cssSelector("td[aria-describedby='generalAccesses_access']"));
				chkBoxElements = Driver.getDriver().findElements(By.cssSelector("td[aria-describedby='generalAccesses_granted']"));
				
				/*if(chkBoxElements.get(5).isSelected() && tdElements.get(5).getAttribute("class").equalsIgnoreCase("accessBySome") && chkBoxElements.get(5).getAttribute("class").equalsIgnoreCase("")){
					Utils.handleResult("3(b) Verify that in the 'General Access' table, for an access colored YELLOW, clicking on the checkbox: (i.) Unchecks the checkbox (ii.) Clears the color YELLOW, (iii.) gives a RED background to the checkbox. A 2nd click on the checkbox (i.) checks the checkbox (ii.) color the access GREEN (iii.) leaves the checkbox background as RED. A 3rd click on the checkbox (i.) leaves the checkbox checked (ii.) changes the access color from GREEN back to YELLOW (iii.) clears the RED from the background of the checkbox.", result, true);
				}else{
					Utils.handleResult("3(b) Verify that in the 'General Access' table, for an access colored YELLOW, clicking on the checkbox: (i.) Unchecks the checkbox (ii.) Clears the color YELLOW, (iii.) gives a RED background to the checkbox. A 2nd click on the checkbox (i.) checks the checkbox (ii.) color the access GREEN (iii.) leaves the checkbox background as RED. A 3rd click on the checkbox (i.) leaves the checkbox checked (ii.) changes the access color from GREEN back to YELLOW (iii.) clears the RED from the background of the checkbox.", result, false);
				}*/
			}else{
				//Utils.handleResult("3(b) Verify that in the 'General Access' table, for an access colored YELLOW, clicking on the checkbox: (i.) Unchecks the checkbox (ii.) Clears the color YELLOW, (iii.) gives a RED background to the checkbox. A 2nd click on the checkbox (i.) checks the checkbox (ii.) color the access GREEN (iii.) leaves the checkbox background as RED. A 3rd click on the checkbox (i.) leaves the checkbox checked (ii.) changes the access color from GREEN back to YELLOW (iii.) clears the RED from the background of the checkbox.", result, false);
			}
			
		}else{
			//Utils.handleResult("3(b) Verify that in the 'General Access' table, for an access colored YELLOW, clicking on the checkbox: (i.) Unchecks the checkbox (ii.) Clears the color YELLOW, (iii.) gives a RED background to the checkbox. A 2nd click on the checkbox (i.) checks the checkbox (ii.) color the access GREEN (iii.) leaves the checkbox background as RED. A 3rd click on the checkbox (i.) leaves the checkbox checked (ii.) changes the access color from GREEN back to YELLOW (iii.) clears the RED from the background of the checkbox.", result, false);
		}
		
		List<WebElement> radioElements = Driver.getDriver().findElements(By.id("wfs_access_1"));
		WebElement element = userManager.getProperElement(radioElements, UserManager.NO_ACCESS);
		userManager.setState(element);
		
		tdElements = Driver.getDriver().findElements(By.cssSelector("td[aria-describedby='workflowStateAccesses_no_access']"));
		if(tdElements.get(0).getAttribute("class").equalsIgnoreCase("accessByAll")){
			tdElements = Driver.getDriver().findElements(By.cssSelector("td[aria-describedby='workflowStateAccesses_reset']"));
			if(tdElements.get(0).getAttribute("class").equalsIgnoreCase("accessEdited")){
				tdElements = Driver.getDriver().findElements(By.cssSelector("td[aria-describedby='workflowStateAccesses_use']"));
				if(tdElements.get(0).getAttribute("class").equalsIgnoreCase("accessByNone")){
					tdElements = Driver.getDriver().findElements(By.cssSelector("td[aria-describedby='workflowStateAccesses_assign']"));
					/*if(tdElements.get(0).getAttribute("class").equalsIgnoreCase("accessByNone")){
						Utils.handleResult("(c) Verify that in the 'Workflow State Access' table, clicking on any radio button under 'No Access', 'Use', 'Assign': (i.) checks the radio button, and only that radio button is checked in that row (ii.) colors that radio button cell GREEN (iii.) colors the corresponding cell under 'Keep Current' RED (iv.) all other cells on that row have color WHITE.", result, true);
					}else{
						Utils.handleResult("(c) Verify that in the 'Workflow State Access' table, clicking on any radio button under 'No Access', 'Use', 'Assign': (i.) checks the radio button, and only that radio button is checked in that row (ii.) colors that radio button cell GREEN (iii.) colors the corresponding cell under 'Keep Current' RED (iv.) all other cells on that row have color WHITE.", result, false);
					}*/
				}else{
					//Utils.handleResult("(c) Verify that in the 'Workflow State Access' table, clicking on any radio button under 'No Access', 'Use', 'Assign': (i.) checks the radio button, and only that radio button is checked in that row (ii.) colors that radio button cell GREEN (iii.) colors the corresponding cell under 'Keep Current' RED (iv.) all other cells on that row have color WHITE.", result, false);
				}
				
			}else{
				//Utils.handleResult("(c) Verify that in the 'Workflow State Access' table, clicking on any radio button under 'No Access', 'Use', 'Assign': (i.) checks the radio button, and only that radio button is checked in that row (ii.) colors that radio button cell GREEN (iii.) colors the corresponding cell under 'Keep Current' RED (iv.) all other cells on that row have color WHITE.", result, false);
			}
		}else{
			//Utils.handleResult("(c) Verify that in the 'Workflow State Access' table, clicking on any radio button under 'No Access', 'Use', 'Assign': (i.) checks the radio button, and only that radio button is checked in that row (ii.) colors that radio button cell GREEN (iii.) colors the corresponding cell under 'Keep Current' RED (iv.) all other cells on that row have color WHITE.", result, false);
		}
		
		radioElements = Driver.getDriver().findElements(By.id("wfs_access_reset_0"));
		element = userManager.getProperElement(radioElements, "reset");
		userManager.setState(element);
		
		tdElements = Driver.getDriver().findElements(By.cssSelector("td[aria-describedby='workflowStateAccesses_reset']"));
		/*if(tdElements.get(0).getAttribute("class").equalsIgnoreCase("")){
			Utils.handleResult("(d) Verify that in the 'Workflow State Access' table, whenever a cell under 'KEEP CURRENT' is colored RED, clicking on the radio button of that RED 'KEEP CURRENT' cell reverses everything that happened previously.", result, true);
		}else{
			Utils.handleResult("(d) Verify that in the 'Workflow State Access' table, whenever a cell under 'KEEP CURRENT' is colored RED, clicking on the radio button of that RED 'KEEP CURRENT' cell reverses everything that happened previously.", result, false);
		}*/
		
		userManager.selectTeam("DEM0 Team 1");
		userManager.clickResetAll();
		
		String teamName = sp.checkValueOfSelectBox(Driver.getDriver(), "teamSelect");
		/*if(Driver.getDriver().findElement(By.id("teamSelect")).getAttribute("class").equalsIgnoreCase("") && (teamName.equalsIgnoreCase("DEM0 Admins") || teamName.equalsIgnoreCase(""))){
			Utils.handleResult("(4) Change the Team selection. Red indicates you have made a change. Hit 'Reset Team' next to the Team selection and verify Recenseo return the original team selection value (or blank), and removes the red.", result, true);
		}else{
			Utils.handleResult("(4) Change the Team selection. Red indicates you have made a change. Hit 'Reset Team' next to the Team selection and verify Recenseo return the original team selection value (or blank), and removes the red.", result, false);
		}*/
		
		userManager.clickResetAll();
		tdElements = Driver.getDriver().findElements(By.cssSelector("td[aria-describedby='generalAccesses_access']"));
		/*if(tdElements.get(0).getAttribute("class").equalsIgnoreCase("accessByAll")){
			Utils.handleResult("(5) After having made (and left) access modifications, as described in stepsabove, click on 'Reset All'. Verify that the access settings have ALL been reversed to how they were when you first loaded the users access settings.", result, true);
		}else{
			Utils.handleResult("(5) After having made (and left) access modifications, as described in stepsabove, click on 'Reset All'. Verify that the access settings have ALL been reversed to how they were when you first loaded the users access settings.", result, false);
		}*/
	}
		

}
