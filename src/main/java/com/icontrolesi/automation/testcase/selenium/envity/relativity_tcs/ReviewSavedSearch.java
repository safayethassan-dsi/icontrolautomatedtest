package com.icontrolesi.automation.testcase.selenium.envity.relativity_tcs;

import java.awt.GridBagLayout;

import javax.swing.BoxLayout;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.testng.SkipException;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.envity.common.ProjectPage;
import com.icontrolesi.automation.testcase.selenium.envity.common.RelativityWorkspaceHome;

public class ReviewSavedSearch extends TestHelper{
	
	@Test
	public void test_0004_ReviewSavedSearch(){
		final String [] REVIEW_FIELDS = {
				"dsi_review_field_01", "dsi_review_field_02", "dsi_review_field_03", 
				"dsi_review_field_04", "dsi_review_field_05", "dsi_review_field_06", 
				"dsi_review_field_07", "dsi_review_field_08", "dsi_review_field_09", 
			}; 

		JPanel inputPanelForReview = new JPanel(new GridBagLayout());
		inputPanelForReview.setLayout(new BoxLayout(inputPanelForReview, BoxLayout.Y_AXIS));
		
		
		JLabel savedSearchLabel = new JLabel("SavedSearch Name: ");
		JLabel reviewFieldLabel = new JLabel("Review Field Name: ");
		JLabel reviewFieldValueLabel = new JLabel("Review Field Value: ");
		
		JTextField savedSearchNameField = new JTextField(15);
		JComboBox<String> reviewFieldNameCombo = new JComboBox<>(REVIEW_FIELDS);
		reviewFieldNameCombo.setPrototypeDisplayValue("This is a Prototype.");
		JComboBox<String> reviewFieldValueCombo = new JComboBox<String>(new String[]{"Very Good", "Very Bad"});
		reviewFieldValueCombo.setPrototypeDisplayValue("This is a Prototype.");
		
		inputPanelForReview.add(new JPanel());
		
		JPanel savedSearchPanel = new JPanel();
		savedSearchPanel.add(savedSearchLabel);
		savedSearchPanel.add(savedSearchNameField);
		
		inputPanelForReview.add(savedSearchPanel);
		
		JPanel reviewFieldPanel = new JPanel();
		reviewFieldPanel.add(reviewFieldLabel);
		reviewFieldPanel.add(reviewFieldNameCombo);
		
		inputPanelForReview.add(reviewFieldPanel);
		
		JPanel reviewFiledValuePanel = new JPanel();
		reviewFiledValuePanel.add(reviewFieldValueLabel);
		reviewFiledValuePanel.add(reviewFieldValueCombo);
		
		inputPanelForReview.add(reviewFiledValuePanel);
		
		inputPanelForReview.add(new JPanel());
		
		int choice = JOptionPane.showConfirmDialog(null, inputPanelForReview, "Please Enter values for REVIEW DOCUMENTS: ", JOptionPane.OK_CANCEL_OPTION);
		
		String savedSearchName = "";
		String reviewField = null;
		String reviewFieldValue = null;
		
		if(choice == JOptionPane.OK_OPTION){
			savedSearchName = savedSearchNameField.getText();
			reviewField = reviewFieldNameCombo.getSelectedItem().toString();
			reviewFieldValue = reviewFieldValueCombo.getSelectedItem().toString();
		}else if(choice == JOptionPane.CANCEL_OPTION || savedSearchName.equals("")){
			throw new SkipException("You either provided a blank SavedSearch or CANCELLEd the operation!!!");
		}
		
		System.out.printf("%s\n%s\n%s\n", savedSearchName, reviewField, reviewFieldValue);
		
		ProjectPage.performLogout();
		
		RelativityWorkspaceHome.loginToRelativity();
		RelativityWorkspaceHome.selectWorkspace("iControl SvP");
	
		
		RelativityWorkspaceHome.openSavedSearch(savedSearchName);
		
		RelativityWorkspaceHome.setNumberofDocumentsPerPage(1000);
		
		tryClick(RelativityWorkspaceHome.CB_FILL_ITEMLIST_LOCATOR, 2);
		tryClick(RelativityWorkspaceHome.EDIT_BTN_LOCATOR, 10);
		switchToWindow(1);
		
		
		RelativityWorkspaceHome.selectFieldLayout("Dsi-Layout");
		RelativityWorkspaceHome.selectReviewField(reviewField, reviewFieldValue);
		RelativityWorkspaceHome.saveSelectedField();
		
		JOptionPane.showMessageDialog(null, String.format("Your SavedSearch with name %s have been reviewed with field %s & filed value %s.", savedSearchName, reviewField, reviewFieldValue), "SavedSearch Reviewd Successfully...", JOptionPane.INFORMATION_MESSAGE);		
	}
	
	/*public static void main(String[] args) {
		
		final String [] REVIEW_FIELDS = {
				"dsi_review_field_01", "dsi_review_field_02", "dsi_review_field_03", 
				"dsi_review_field_04", "dsi_review_field_05", "dsi_review_field_06", 
				"dsi_review_field_07", "dsi_review_field_08", "dsi_review_field_09", 
			}; 

		JPanel inputPanelForReview = new JPanel(new GridBagLayout());
		inputPanelForReview.setLayout(new BoxLayout(inputPanelForReview, BoxLayout.Y_AXIS));
		
		
		JLabel savedSearchLabel = new JLabel("SavedSearch Name: ");
		JLabel reviewFieldLabel = new JLabel("Review Field Name: ");
		JLabel reviewFieldValueLabel = new JLabel("Review Field Value: ");
		
		JTextField savedSearchNameField = new JTextField(15);
		JComboBox<String> reviewFieldNameCombo = new JComboBox<>(REVIEW_FIELDS);
		reviewFieldNameCombo.setPrototypeDisplayValue("This is a Prototype.");
		JComboBox<String> reviewFieldValueCombo = new JComboBox<String>(new String[]{"Very Good", "Very Bad"});
		reviewFieldValueCombo.setPrototypeDisplayValue("This is a Prototype.");
		
		inputPanelForReview.add(new JPanel());
		
		JPanel savedSearchPanel = new JPanel();
		savedSearchPanel.add(savedSearchLabel);
		savedSearchPanel.add(savedSearchNameField);
		
		inputPanelForReview.add(savedSearchPanel);
		
		JPanel reviewFieldPanel = new JPanel();
		reviewFieldPanel.add(reviewFieldLabel);
		reviewFieldPanel.add(reviewFieldNameCombo);
		
		inputPanelForReview.add(reviewFieldPanel);
		
		JPanel reviewFiledValuePanel = new JPanel();
		reviewFiledValuePanel.add(reviewFieldValueLabel);
		reviewFiledValuePanel.add(reviewFieldValueCombo);
		
		inputPanelForReview.add(reviewFiledValuePanel);
		
		inputPanelForReview.add(new JPanel());
		
		int choice = JOptionPane.showConfirmDialog(null, inputPanelForReview, "Please Enter values for REVIEW DOCUMENTS: ", JOptionPane.OK_CANCEL_OPTION);
		
		String savedSearchName = "";
		String reviewField = null;
		String reviewFieldValue = null;
		
		if(choice == JOptionPane.OK_OPTION){
			savedSearchName = savedSearchNameField.getText();
			reviewField = reviewFieldNameCombo.getSelectedItem().toString();
			reviewFieldValue = reviewFieldValueCombo.getSelectedItem().toString();
		}else if(choice == JOptionPane.CANCEL_OPTION || savedSearchName.equals("")){
			throw new SkipException("You either provided a blank SavedSearch or CANCELLEd the operation!!!");
		}
		
		System.out.printf("%s\n%s\n%s\n", savedSearchName, reviewField, reviewFieldValue);
	}*/
}
