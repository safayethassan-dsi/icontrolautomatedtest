package com.icontrolesi.automation.testcase.selenium.recenseo.savedsearch;

import java.util.Date;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Frame;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchesAccordion;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;

import com.icontrolesi.automation.platform.util.Logger;

public class SavingASearchInGroupAndAccessibleOnlyByTeam extends TestHelper{
	String savedSearchName = "DisplayOfASingleSavedSearchInGroup_" + new Date().getTime();
	String savedSearchGroupName = "SavedSearchDemoGroup";
	
	String finalStep = "Recenseo expands the Search Group folder in order to display the only single saved search (as mentioned in point 5) with check box on the left hand side of the search ( %s )";
	
	@Test
	public void test_c11_SavingASearchInGroupAndAccessibleOnlyByTeam(){
		handleSavingASearchInGroupAndAccessibleOnlyByTeam();
	}
	
	private void handleSavingASearchInGroupAndAccessibleOnlyByTeam(){
		new SelectRepo(AppConstant.DEM0);
    	SearchPage.setWorkflow("Search");
    	
    	SearchesAccordion.open();
    	SearchesAccordion.createTopLevelGroup(savedSearchGroupName, "All");
    	
    	switchToDefaultFrame();
        waitForFrameToLoad(Frame.MAIN_FRAME);
   
    	
    	SearchPage.addSearchCriteria("Author");
    	
    	hoverOverElement(SearchPage.searchBoxLocator);
    	tryClick(SearchPage.saveSearchBtnLocator, SearchPage.saveSearchNameLocator);
    	
    	String savedSearchDialogHeader = getText(SearchPage.saveSearchDialogHeaderLocator);
    	
    	softAssert.assertEquals(savedSearchDialogHeader, "Save Search As", "6. Recenseo opens up a \"Save Search As\" dialog box:: ");
    	
    	editText(SearchPage.saveSearchNameLocator, savedSearchName);
    	
    	SearchPage.selectPermissionForSavedSearchInPopup("Private");
    	
    	SearchPage.selectCheckGroupForSavedSearch();
    	
    	boolean isSaveToGroupUnChecked = isElementChecked(SearchPage.savedSearchCheckGroupLocator);
    	
    	boolean isSelectGroupDisabled = isElementEnabled(SearchPage.savedSearchSelectGroupLocator);
    	
    	softAssert.assertTrue(isSaveToGroupUnChecked && isSelectGroupDisabled, "10. Recenseo keeps the Select Group combo box disabled:: ");
    	
    	selectFromDrowdownByText(SearchPage.savedSearchSelectGroupLocator, savedSearchGroupName);
    	
    	tryClick(SearchPage.savedSearchContinueBtnLocator, 30);
    	
    	SearchesAccordion.open();
    	
    	boolean isSearchSaved = SearchesAccordion.isSavedSearchExists(savedSearchName);
    	
    	softAssert.assertTrue(isSearchSaved, String.format(finalStep, "Existence of saved search"));
    	
    	WebElement savedSearchElement = getElement(SearchesAccordion.savedSearchUnderAGroupLocator);
                
        String checkBoxDisplayProperty = savedSearchElement.findElement(By.cssSelector("i")).getCssValue("display").trim();
        
        softAssert.assertEquals(checkBoxDisplayProperty, "inline-block", String.format(finalStep, "Appearance of checkbox"));
        softAssert.assertTrue(getAttribute(savedSearchElement, "class").contains("childNode"), String.format(finalStep, "Save search is saved under the group name"));
        
        WebElement parentOfSavedSearchItem = savedSearchElement.findElement(By.xpath("..")).findElement(By.xpath(".."));
        
        System.out.println( Logger.getLineNumber() + "Tag: " + parentOfSavedSearchItem.getTagName());
        System.out.println( Logger.getLineNumber() + parentOfSavedSearchItem.getAttribute("class") + "*****");
        String savedSearchesParentName = getText(parentOfSavedSearchItem.findElement(By.tagName("a")));
        
        boolean isParentContainsFolderIcon = parentOfSavedSearchItem.findElement(By.cssSelector("a > i:nth-child(2)")).getAttribute("class").contains("icon-folder-plus"); 
        
        softAssert.assertEquals(savedSearchesParentName, savedSearchGroupName, String.format(finalStep, "SavedSearch's parent name matches"));
        softAssert.assertTrue(parentOfSavedSearchItem.getAttribute("class").endsWith("parentNode"), String.format(finalStep, "Parent of saved search exists?"));
        softAssert.assertTrue(isParentContainsFolderIcon, String.format(finalStep, "Verify the conatiner contains appropriate icon"));
        
        SearchesAccordion.openSavedSearchInfoDetail(savedSearchName);
    	
    	String permissionLevel = getText(SearchesAccordion.savedPermissionInfoLocator);
    	System.err.println(permissionLevel+"****");
    	
    	softAssert.assertEquals(permissionLevel, "Mine", "(*) SavedSearch's permission level is ok (Private/Mine):: ");
    	
    	//Test whether group member can access saved search by another user in the group
    	
    	SearchPage.logout();
    	performLoginWithCredential(AppConstant.ICEAUTOTEST_3, AppConstant.PASS3);
    	
    	new SelectRepo(AppConstant.DEM0);
    	SearchPage.setWorkflow("Search");
    	
    	SearchesAccordion.open();
    	SearchesAccordion.createTopLevelGroup(savedSearchGroupName, "All");
    	
    	boolean isSavedSearchHiddenFromGroupMembers = (SearchesAccordion.isSavedSearchExists(savedSearchName) == false);
    	
    	softAssert.assertTrue(isSavedSearchHiddenFromGroupMembers, "*** Verify - Saved Search is only accessible by the owner (Not accessible by Group members):: ");
    	
    	//Test whether non-group member can access saved search by another user
    	
    	SearchPage.logout();
    	performLoginWithCredential(AppConstant.USER2, AppConstant.PASS2);
    	
    	new SelectRepo(AppConstant.DEM0);
    	SearchPage.setWorkflow("Search");
    	
    	SearchesAccordion.open();
    	SearchesAccordion.createTopLevelGroup(savedSearchGroupName, "All");
    	
    	boolean isSavedSearchHiddenFromNonGroupUsers = (SearchesAccordion.isSavedSearchExists(savedSearchName) == false);
    	
    	softAssert.assertTrue(isSavedSearchHiddenFromNonGroupUsers, "*** Verify - Saved Search is only accessible by the owner (Not accessible by Non-group members):: ");
    	
    	softAssert.assertAll();
	}
}
