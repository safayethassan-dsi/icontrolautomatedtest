package com.icontrolesi.automation.testcase.selenium.recenseo.tags.manage_tags_grid;

import java.util.Calendar;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.TagManagerInGridView;

public class SearchForTagFromManageTagInGrid extends TestHelper{
	long timeFrame = Calendar.getInstance().getTimeInMillis();
	String tagName = "SearchForTagFromManageTagInGrid_c1813_" + timeFrame;
	
	@Test
	public void test_c1813_SearchForTagFromManageTagInGrid(){
		new SelectRepo(AppConstant.DEM0);
		SearchPage.setWorkflow("Search");
		
		SearchPage.addSearchCriteria("Addressee");
		
		performSearch();
		
		TagManagerInGridView.open();
		TagManagerInGridView.selectTagFilter("All");
		TagManagerInGridView.createTopLevelTag(tagName, "");
		
		enterText(TagManagerInGridView.tagSearchInputFieldLocator, tagName);
		
		waitFor(2);
		
		List<WebElement> suggetionList = getElements(By.cssSelector(".ui-autocomplete li"));
		
		softAssert.assertEquals(suggetionList.size(), 1, "4. As you're typing, confirm that the predictive text drop-down shows reasonable results(Total Count match):: ");
		softAssert.assertEquals(getText(suggetionList.get(0)), tagName, "4. As you're typing, confirm that the predictive text drop-down shows reasonable results(Search term match):: ");
		
		tryClick(TagManagerInGridView.tagSearchBtnLocator, 10);
		
		boolean isSearchOk = TagManagerInGridView.isTagFound(tagName);
		
		softAssert.assertTrue(isSearchOk, "***) Correct tag appeared in the search results:: ");
		
		TagManagerInGridView.deleteTag(tagName);
		
		softAssert.assertAll();
	}
}
