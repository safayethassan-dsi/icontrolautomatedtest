package com.icontrolesi.automation.testcase.selenium.envity.projects_creation;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.testng.annotations.Test;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.envity.common.CALSimpleGeneralConfigurationPage;
import com.icontrolesi.automation.testcase.selenium.envity.common.CALSmplInfo;
import com.icontrolesi.automation.testcase.selenium.envity.common.CALSmplInfo.CALSmplInfoBuilder;
import com.icontrolesi.automation.testcase.selenium.envity.common.LeftPanelForProjectPage;
import com.icontrolesi.automation.testcase.selenium.envity.common.ProjectInfoLoader;
import com.icontrolesi.automation.testcase.selenium.envity.common.SALInfo;
import com.icontrolesi.automation.testcase.selenium.envity.common.TaskHistoryPage;

public class CheckCALSimplifiedProjectCreation extends TestHelper{

	@Test(description = "Check whether the full execution path for CAL Simple project creation is ok.")
	public void test_0000002_CheckCALSimplifiedProjectCreation() {
		SimpleDateFormat sdf = new SimpleDateFormat("ddMMYYY_hhmm");
		
		 
		CALSmplInfo calInfo = new ProjectInfoLoader("cal_smpl").loadCALSmplInfo();
		
		//CALSimpleGeneralConfigurationPage.createProject(projectName);
		CALSimpleGeneralConfigurationPage.createProject(calInfo);
		
		LeftPanelForProjectPage.gotoTaskQueuePage();
		
		boolean isProjectCreationOk = TaskHistoryPage.isCurrentTaskFailed("Create Simple Envity CAL") == false;
		
		softAssert.assertTrue(isProjectCreationOk, "Simple CAL project creation FAILED:: ");
		
		softAssert.assertAll();
	} 

}
