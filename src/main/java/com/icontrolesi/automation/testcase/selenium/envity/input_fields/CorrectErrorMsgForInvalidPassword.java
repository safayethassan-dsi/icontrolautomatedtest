package com.icontrolesi.automation.testcase.selenium.envity.input_fields;

import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.envity.common.EnvitySupport;
import com.icontrolesi.automation.testcase.selenium.envity.common.ProjectPage;

public class CorrectErrorMsgForInvalidPassword extends TestHelper{
	private final String EXPECTED_ALERT_MSG = "Invalid username or password"; 
	@Test
	public void test_c259_CorrectErrorMsgForInvalidPassword(){
		ProjectPage.performLogout();
		
		inputLoginCredentials(AppConstant.USER + "_unexpected", "AnInvalidPassword.");
		
		tryClick(EnvitySupport.loginBtnLocator);
		
		String invalidPasswordMsg = getText(EnvitySupport.loginErrorMsgLocator);
		
		softAssert.assertEquals(invalidPasswordMsg, EXPECTED_ALERT_MSG);
		
		softAssert.assertAll();
	}
}
