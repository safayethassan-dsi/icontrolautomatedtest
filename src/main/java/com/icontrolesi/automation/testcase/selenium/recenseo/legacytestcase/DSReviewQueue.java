package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Frame;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.ManageDocumentSecurity;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.ReviewAssignmentAccordion;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SprocketMenu;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.UserManager;
/**
 *
 * @author Shaheed Sagar
 *
 *  "*Will Require Two Users (or two User IDs, an ""Admin"" ID and a ""Test User"" ID) 
	(1) Log into DEM0 and Select Administrative Settings under Settings Menu and Choose Manage Users 
	(2) Click on the Teams tab 
	(3) Confirm the ""Test User"" ID  is still in  ""Document Security"" Team  
	(4) Return to Search Page, Open ""DocumentSecurityTest"" Review Queue assignment  
	(5)  Select ""Manage Document Security"" in the Actions Menu and block 100 Documents from ""Document Security"" Team. Make sure that these docs are a mix of Remaining/Pending/Completed docs.  
	(6) Confirm the ""Test User"" ID cannot access those 100 documents in the Assignment  
	(7) Confirm that the Assignment Summary information Recenseo displays for the ""Test User"" ID does NOT include any of the secured docs in totals. Make sure that the total, pending, and completed numbers counts accurately reflect the documents that ""Test User"" ID DOES have access too."
 */

public class DSReviewQueue extends TestHelper{
	String assignMentToOpen = "DocumentSecurityTest";
	
	@Test(enabled = false)
	public void test_c1635_DSReviewQueue(){
		handleDSReviewQueue(driver);
	}

    private void handleDSReviewQueue(WebDriver driver){
    	new SelectRepo(AppConstant.DEM0);
    	
    	SearchPage.setWorkflow("Search");
        
        SprocketMenu.openUserManager();
        UserManager.switchTab("Teams");
        UserManager.selectTeam("Document Security");
        
        boolean isUserExist = UserManager.isUserExists(AppConstant.USER2);
        
        softAssert.assertTrue(isUserExist, "(3) Confirm the \"Test User\" ID  is still in  \"Document Security\" Team:: ");
        
        SearchPage.goToDashboard();
        
        waitForFrameToLoad(Frame.MAIN_FRAME);
        
        ReviewAssignmentAccordion.open();
        ReviewAssignmentAccordion.openAssignment(assignMentToOpen);
        
        int totalDocumentCountInTheAssignment = DocView.getDocmentCount();
        
        new DocView().clickActionMenuItem("Manage Document Security");
        switchToFrame(1);
        ManageDocumentSecurity.unBlockTeam("Document Security");	//unblock all documents first
        ManageDocumentSecurity.closeWindow();  
        
        DocView.setNumberOfDocumentPerPage(100);
        DocView.selectAllDocuments();
        
        new DocView().clickActionMenuItem("Manage Document Security");
        switchToFrame(getElements(By.cssSelector("iframe[src*='javascript']")).get(1));
        ManageDocumentSecurity.blockTeam("Document Security");	//unblock all documents first
        ManageDocumentSecurity.closeWindow();  
        
        SearchPage.logout();
		
        performLoginWithCredential(AppConstant.USER2, AppConstant.PASS2);	
        
        new SelectRepo(AppConstant.DEM0);
       
        SearchPage.setWorkflow("Search");
        
        ReviewAssignmentAccordion.open();
        ReviewAssignmentAccordion.openAssignment(assignMentToOpen);
        
        int totdalDocumentForTestUser = DocView.getDocmentCount();
        
        softAssert.assertEquals(totalDocumentCountInTheAssignment, totdalDocumentForTestUser + 100, "6) Confirm the \"Test User\" ID cannot access those 100 documents in the Assignment:: ");
     
        softAssert.assertAll();
    }
}