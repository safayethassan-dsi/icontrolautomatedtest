
package com.icontrolesi.automation.testcase.selenium.recenseo.savedsearch;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchesAccordion;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;

import com.icontrolesi.automation.platform.util.Logger;

public class SearchForPreviouslySaveSearchUsingSearchBox extends TestHelper{
	String savedSearchName = "SavedSearch";
	
	@Test
	public void test_c35_SearchForPreviouslySaveSearchUsingSearchBox(){
		handleSearchForPreviouslySaveSearchUsingSearchBox();
	}
	
	private void handleSearchForPreviouslySaveSearchUsingSearchBox(){
		new SelectRepo(AppConstant.DEM0);
		
    	SearchPage.setWorkflow("Search");
    	
    	SearchesAccordion.open();
    	SearchesAccordion.selectFilter("All");
    	editText(SearchesAccordion.searchFieldLocator, savedSearchName);
    	waitFor(driver, 10);
    	
    	softAssert.assertTrue(SearchesAccordion.isAutoSuggestionAppeared(), "*** The Search is narrowed down accordingly:: ");
    	
    	List<WebElement> suggestionList = getElements(SearchesAccordion.autoCompleteItemsLocator);
    	
    	for(WebElement suggestion : suggestionList){
    		String suggestionText = suggestion.getText().trim();
    		//System.err.println(suggestionText+"*********************************************");
    		System.out.println("[ Line# " + Logger.getLineNumber() + " ]:: " );
    		softAssert.assertTrue(suggestionText.equals("") || suggestionText.contains(savedSearchName), "8. Recenseo displays all the 'Saved' searches and Search Groups similar to the name typed in the search box:: ");
    	}
    	
    	softAssert.assertAll();
	}
}
