package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import java.util.Set;
import java.util.stream.Collectors;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.ReviewAssignmentAccordion;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;


/**
 * mantis# 15218
 * @author Ishtiyaq Kamal
 * 
 * DB: DEM0
(1) Go to search page. Open the Review Assignments Section.
(2) Sort by Priority and confirm Priority 1 is highest priority so it should show those first.
(3) Sort by Assignee and confirm Unassigned assignments should be at the BOTTOM.
(4) Verify the unassigned assignment has a Checkout button visible.
(5) Click the Refresh Icon and make sure it keeps the sort you selected.
 *
 */

public class RevAssignSortRefTest extends TestHelper{
   @Test
   public void test_c1655_RevAssignSortRefTest(){
	   //loginToRecenseo();
	   handleRevassignSortRefTest(driver);
   }


	protected void handleRevassignSortRefTest(WebDriver driver) {
		new SelectRepo(AppConstant.DEM0);
		
		SearchPage.setWorkflow("Search");
		
		ReviewAssignmentAccordion.open();
		ReviewAssignmentAccordion.sortAssignmentBy("Priority");
		
		Set<String> priorityList = getListOfItemsFrom(By.cssSelector("li[style$='block;'] span.priority"), i -> i.getText().trim()).stream()
				.collect(Collectors.toSet());
		Set<String> sortedPriorityList = priorityList.stream().sorted().collect(Collectors.toSet());
		
		softAssert.assertEquals(priorityList, sortedPriorityList, "2) Sort by Priority works (Priority 1 is highest priority):: ");
		
		ReviewAssignmentAccordion.sortAssignmentBy("Priority + Assignee");
		
		ReviewAssignmentAccordion.refreshAssignment();
		
		String sortType = getSelectedItemFroDropdown(ReviewAssignmentAccordion.sortAssignmentDropdownLocator);
		
		softAssert.assertEquals(sortType, "Priority + Assignee", "5) Click the Refresh Icon and make sure it keeps the sort you selected:: ");
		
		softAssert.assertAll();
	}
}
