package com.icontrolesi.automation.testcase.selenium.recenseo.report.general.selection_summary;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.config.Configuration;
import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;
import com.icontrolesi.automation.testcase.selenium.recenseo.report.common.GeneralReport;

public class ExpandingSelectionSummaryDrawerToDisplayCriterion extends TestHelper{
	@Test
	public void test_c88_ExpandingSelectionSummaryDrawerToDisplayCriterion(){
		 
		new SelectRepo(AppConstant.DEM0);
		 
		GeneralReport.openFileTypesReport();
		
		String selectionSummaryBefore = getText(GeneralReport.reportSummaryLocator);
		
		softAssert.assertEquals("No selections made", selectionSummaryBefore, "5) .Recenseo displays a contracted (closed) Selection Summary Drawer(Summary Text Before Selection):: ");
		
		List<WebElement> bars = getElements(By.cssSelector(".highcharts-series-group > g rect"));
		List<String> labelList = getListOfItemsFrom(By.className("js-ellipse"), i -> getAttribute(i, "title"));
		int itemPositionToBeSelected;
		
		if(Configuration.getConfig("selenium.browser").equals("ie11")){
			itemPositionToBeSelected = 0;
		}else{
			itemPositionToBeSelected = getRandomNumberInRange(1, bars.size()-1);
		}
		
		bars.get(itemPositionToBeSelected).click();
		
		boolean isDrawerClosed = getAttribute(By.cssSelector(".report-btn-summary-drawer span"), "class").endsWith("up2");
		
		softAssert.assertTrue(isDrawerClosed, "5) .Recenseo displays a contracted (closed) Selection Summary Drawer (Drawer Open/Close icon):: ");
		
		tryClick(By.cssSelector(".report-btn-summary-drawer span"));
		
		String criterionFieldName = SearchPage.getCriterionField(0);
		String criterionOperator = SearchPage.getOperatorValue(0);
		String criterionValue = SearchPage.getCriterionValue(0);
		
		softAssert.assertEquals(criterionFieldName, "Document Type", "*) Recenseo expands the drawer to display criterion in the same way they are displayed in the 'Search' builder page:: ");
		softAssert.assertEquals(criterionOperator, "contains", "**) Recenseo expands the drawer to display criterion in the same way they are displayed in the 'Search' builder page:: ");
		softAssert.assertEquals(criterionValue, labelList.get(itemPositionToBeSelected), "***) Recenseo expands the drawer to display criterion in the same way they are displayed in the 'Search' builder page:: ");
		
		softAssert.assertAll();	
	}
}
