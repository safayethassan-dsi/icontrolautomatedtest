package com.icontrolesi.automation.testcase.selenium.recenseo.nested_picklist;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.ReviewTemplatePane;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SprocketMenu;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.WorkflowFieldManager;

import com.icontrolesi.automation.platform.util.Logger;

public class DisplayingNestedValueInGridView extends TestHelper{
	String picklistName = "DisplayingNestedValueInGridView_c1693";
	String picklistName02 = picklistName + "_2";
	
	@Test
	public void test_c1693_DisplayingNestedValueInGridView(){
		new SelectRepo(AppConstant.DEM0);
		
		DocView.openPreferenceWindow();
		DocView.checkSelectedColumns("Priv Reason");
		DocView.closePreferenceWindow();
		
		SearchPage.setWorkflow("Search");
		
		SprocketMenu.openWorkflowAndFieldManager();
		
		WorkflowFieldManager.selectFieldFromFieldSetup("Priv Reason");
		
		WorkflowFieldManager.createParentPicklistItem(picklistName);
		
		hoverOverElement(WorkflowFieldManager.picklistItemLocator);
		
		WorkflowFieldManager.createChildPicklistItem(picklistName, picklistName02);
		
		SearchPage.gotoViewDocuments();
		
		int numberOfDocumentsPerPage = getTotalElementCount(DocView.gridRowLocator);
	    
	    int documentSelectionNumber = getRandomNumberInRange(1, numberOfDocumentsPerPage);
	    
	    DocView.selectDocumentBySpecificNumber(documentSelectionNumber);
	    
	    ReviewTemplatePane.expandPicklistFor("Priv Reason");
	    
	    WebElement privReasonElement = getParentOf(getElement(By.xpath("//label[text()='Priv Reason:']")));
	    
	    System.out.println( Logger.getLineNumber() + privReasonElement.findElement(By.cssSelector(" li a")).getText() + "**^^^^*");
	    
	    WebElement childPicklistItem = privReasonElement.findElement(By.xpath("//a[text()='" + picklistName02 + "']"));
	    
	    childPicklistItem.findElement(By.tagName("i")).click();
	    
	    ReviewTemplatePane.saveDocument();
	    
	    String privReasonValueForSelectedDocument = getListOfItemsFrom(DocView.documentPrivReasonColumnLocator, item -> getText(item)).get(documentSelectionNumber - 1);
	    
	    System.err.println(privReasonValueForSelectedDocument+ "***");
	    
	    softAssert.assertTrue(privReasonValueForSelectedDocument.contains(picklistName + " ➞ " + picklistName02));	
	    
	    SprocketMenu.openWorkflowAndFieldManager();
	    WorkflowFieldManager.selectFieldFromFieldSetup("Priv Reason");
	    WorkflowFieldManager.deletePicklistItem(picklistName);
	    WorkflowFieldManager.deletePicklistItem(picklistName02);
		
		softAssert.assertAll();
	}
}
