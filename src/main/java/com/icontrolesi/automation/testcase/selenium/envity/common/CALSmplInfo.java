package com.icontrolesi.automation.testcase.selenium.envity.common;

public class CALSmplInfo implements ProjectInfo{
    //  Optional Fields
    private String projectName = "";
    private String envizeInstance = "";
    private String relativityInstance = "";
    private String workspace = "";

    private String savedSearchName = "";
    private String predictionField = "";
    private String truePositive = "";
    private String trueNegative = "";
    private String stoppingPoint = "";
    private String randomSample = "";
    private String syntheticDocText = "";

    public String getProjectName() {
        return this.projectName;
    }

    public String getEnvizeInstance() {
        return this.envizeInstance;
    }

    public String getRelativityInstance() {
        return this.relativityInstance;
    }

    public String getWorkspace() {
        return this.workspace;
    }

    public String getSavedSearchName(){
        return this.savedSearchName;
    }

    public String getPredictionField() {
        return this.predictionField;
    }

    public String getTruePositive() {
        return this.truePositive;
    }

    public String getTrueNegative() {
        return this.trueNegative;
    }


    public String getStoppingPoint() {
        return this.stoppingPoint;
    }

    public String getRandomSample() {
        return this.randomSample;
    }

    public String getSyntheticDocText() {
        return this.syntheticDocText;
    }

    CALSmplInfo(CALSmplInfoBuilder calSmplInfoBuilder) {
        this.projectName =  calSmplInfoBuilder.projectName;
        this.envizeInstance = calSmplInfoBuilder.envizeInstance;
        this.relativityInstance = calSmplInfoBuilder.relativityInstance;
        this.workspace = calSmplInfoBuilder.workspace;

        this.predictionField =  calSmplInfoBuilder.predictionField;
        this.truePositive = calSmplInfoBuilder.truePositive;
        this.trueNegative = calSmplInfoBuilder.trueNegative;
        this.savedSearchName = calSmplInfoBuilder.savedSearchName;

        this.stoppingPoint = calSmplInfoBuilder.stoppingPoint;
        this.randomSample = calSmplInfoBuilder.randomSample;
        this.syntheticDocText = calSmplInfoBuilder.syntheticDocText;
    }	

    public static class CALSmplInfoBuilder{
        private String projectName = "";
        private String envizeInstance = "";
        private String relativityInstance = "";
        private String workspace = "";

        private String savedSearchName = "";
        private String predictionField = "";
        private String truePositive = "";
        private String trueNegative = "";
        private String stoppingPoint = "";
        private String randomSample = "";
        private String syntheticDocText = "";
        


        public CALSmplInfoBuilder setProjectName(String projectName) {
            this.projectName = projectName;
            return this;
        }


        public CALSmplInfoBuilder setEnvizeInstance(String envizeInstance) {
            this.envizeInstance = envizeInstance;
            return this;
        }

        public CALSmplInfoBuilder setRelativityInstance(String relativityInstance) {
            this.relativityInstance = relativityInstance;
            return this;
        }


        public CALSmplInfoBuilder setWorkspace(String workspace) {
            this.workspace = workspace;
            return this;
        }


        public CALSmplInfoBuilder setPredictionField(String predictionField) {
            this.predictionField = predictionField;
            return this;
        }


        public CALSmplInfoBuilder setTruePositive(String truePositive) {
            this.truePositive = truePositive;
            return this;
        }


        public CALSmplInfoBuilder setTrueNegative(String trueNegative) {
            this.trueNegative = trueNegative;
            return this;
        }
        
        public CALSmplInfoBuilder setSavedSearchName(String savedSearchName) {
            this.savedSearchName = savedSearchName;
            return this;
        }

        public CALSmplInfoBuilder setRandomSample(String randomSample) {
            this.randomSample = randomSample;
            return this;
        }


        public CALSmplInfoBuilder setStoppingPoint(String stoppingPoint) {
            this.stoppingPoint = stoppingPoint;
            return this;
        }


        public CALSmplInfoBuilder setSyntheticDocText(String syntheticDocText) {
            this.syntheticDocText = syntheticDocText;
            return this;
        }

        public CALSmplInfo build(){
            return new CALSmplInfo(this);
        }
    }
}