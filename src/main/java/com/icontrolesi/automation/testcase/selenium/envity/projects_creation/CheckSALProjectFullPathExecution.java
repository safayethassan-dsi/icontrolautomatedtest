package com.icontrolesi.automation.testcase.selenium.envity.projects_creation;

import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.envity.common.LeftPanelForProjectPage;
import com.icontrolesi.automation.testcase.selenium.envity.common.ProjectInfoLoader;
import com.icontrolesi.automation.testcase.selenium.envity.common.ProjectPage;
import com.icontrolesi.automation.testcase.selenium.envity.common.RelativityWorkspaceHome;
import com.icontrolesi.automation.testcase.selenium.envity.common.SALGeneralConfigurationPage;
import com.icontrolesi.automation.testcase.selenium.envity.common.SALInfo;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Driver;

import org.testng.annotations.Test;

public class CheckSALProjectFullPathExecution extends TestHelper {
	
	@Test(description = "Check whether the full execution path for SAL project creation is ok.")
	public void test_0001000_CheckSALProjectFullPathExecution(){
		String mainWindowHandler = Driver.getDriver().getWindowHandle();
		
		SALInfo salInfo = new ProjectInfoLoader("sal").loadSALInfo();
		
		SALGeneralConfigurationPage.createSALProject(salInfo);
		// ProjectPage.openProject("SL_TST_03102018_0204");
		//LeftPanelForProjectPage.gotoTaskQueuePage();
		
		
		SALGeneralConfigurationPage.addPopulation(salInfo.getSavedSearchNameForPopulatioin());
		
		// LeftPanelForProjectPage.Indexing.perform();
		
		boolean isPopulationAddingOk = SALGeneralConfigurationPage.isPopulationAdded();
		
		softAssert.assertTrue(isPopulationAddingOk, "**** Adding Population FAILED!!");
		
		LeftPanelForProjectPage.addJudgementalSample(salInfo.getJudgementalSampleName());
		
		boolean isJudgementalSampleAdded = SALGeneralConfigurationPage.isJudgementalSampleAdded();
		
		softAssert.assertTrue(isJudgementalSampleAdded, "**** Adding Judgemental Sample FAILED:: ");

			
		LeftPanelForProjectPage.addActiveSample(100);
		boolean isActiveSampleAdded = SALGeneralConfigurationPage.isActiveSampleAdded();
		softAssert.assertTrue(isActiveSampleAdded, "**** Adding Active Samples FAILED:: ");
		
		
		LeftPanelForProjectPage.addControlSample("Fixed Size", 400);
		boolean isControlSampleAdded = SALGeneralConfigurationPage.isControlSampleAdded();
		softAssert.assertTrue(isControlSampleAdded, "**** Adding Control Samples FAILED:: ");
	
		String projectName = salInfo.getProjectName();
		
		String cSampleName = String.format("%s %s", projectName, "CSmpl");
		String jSampleName = String.format("%s %s", projectName, "JSmpl");
		String aSampleName = String.format("%s %s", projectName, "ASmpl");

		// ProjectPage.openProject("SL_TST_28092018_0227");

		
		System.out.println(jSampleName + "******************");
		
		ProjectPage.performLogout();
		
		RelativityWorkspaceHome.loginToRelativity();
		RelativityWorkspaceHome.selectWorkspace("iControl SvP");
		
		RelativityWorkspaceHome.openSavedSearch(jSampleName);
		
		RelativityWorkspaceHome.setNumberofDocumentsPerPage(1000);
		
//		Frame.switchToExternalFrame();
		tryClick(RelativityWorkspaceHome.CB_FILL_ITEMLIST_LOCATOR, 2);
		tryClick(RelativityWorkspaceHome.EDIT_BTN_LOCATOR, 10);
		switchToWindow(1);
		
		
		RelativityWorkspaceHome.selectFieldLayout("Dsi-Layout");
		RelativityWorkspaceHome.selectReviewField("dsi_review_field_09", "Very Good");
		RelativityWorkspaceHome.saveSelectedField();
		
		switchToWindow(mainWindowHandler);
		
		
		RelativityWorkspaceHome.openSavedSearch(cSampleName);
		
		//RelativityWorkspaceHome.waitForLoadingComplete();
		
		landOnLoginPage();
		performLoginToEnvity();
		
		ProjectPage.openProject(projectName);
		
		LeftPanelForProjectPage.Sync.perform();
		
		LeftPanelForProjectPage.gotoPredictPage();
		LeftPanelForProjectPage.Prediction.perform();

		System.err.println(Driver.getDriver().getWindowHandles().size()+"****"); 
		
		softAssert.assertAll();
	}
}
