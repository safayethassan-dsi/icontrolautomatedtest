package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Frame;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;

import com.icontrolesi.automation.platform.util.Logger;


public class ImagesNavigation extends TestHelper{
	@Test
	public void test_c372_ImagesNavigation(){
		handleImagesNavigation(driver);
	}
	
	private void handleImagesNavigation(WebDriver driver){
		new SelectRepo(AppConstant.DEM0);

		SearchPage.waitForFrameLoad(Frame.MAIN_FRAME);
		
		SearchPage sp= new SearchPage();
		
		//Add search criteria
		SearchPage.addSearchCriteria("filename");
		
		//Set Operator value to "equals"
        SearchPage.setOperatorValue(0, "equals");
        
        
        //Assign value for filename as "paper
        SearchPage.setCriterionValue(0, "paper");
        
        //Click Run Search
        performSearch();

        //Click on the 26th document
         
         SearchPage.waitForElement("td[aria-describedby='grid_cb']", "css-selector");//Wait for docs to load
         DocView dc= new DocView();
         dc.clickOnDocument( 25);
        
         //Check whether image is being displayed correctly
         SearchPage.waitForFrameLoad(Frame.VIEWER_FRAME);
         
         try{
        	 getElement(By.cssSelector("div[class='ImageWrapper ui-selectable']"));
        	 
         }catch(NoSuchElementException e){
        	 System.out.println( Logger.getLineNumber() + "(3) Verify Image Displays as Intended:fail");
      		 //result.nl().record("(3) Verify Image Displays as Intended:fail");
      		 //result.fail();
         }
         
        
         //Check if navigation buttons are working ok
         dc.ImageNextPage();
         /*if(dc.getPageBoxValue(sp).equalsIgnoreCase("2")){
        	 result.nl().record("[Next page button] working:pass");
         }else{
        	 result.nl().record("[Next page button] working:fail");
        	 result.fail();
         }*/
         
         
         dc.ImagePreviousPage();
         /*if(dc.getPageBoxValue(sp).equalsIgnoreCase("1")){
        	 result.nl().record("[Previous page button] working:pass");
         }else{
        	 result.nl().record("[Previous page button] working:fail");
        	 result.fail();
         }*/
         
         
         dc.ImageLastPage();
         String totalPageNum= SearchPage.waitForElement("pageCount", "id").getText();
         /*if(dc.getPageBoxValue(sp).equalsIgnoreCase(totalPageNum)){
        	 
        	 result.nl().record("[Last page button] working:pass");
        	 
         }
         else{
        	 
        	 result.nl().record("[Last page button] working:fail");
        	 result.fail();
         }*/
         
         
         
         dc.ImageFirstPage();
         
        /* if(dc.getPageBoxValue(sp).equalsIgnoreCase("1")){
        	 
        	 result.nl().record("[First page button] working:pass");
        	 
         }
         else{
        	 
        	 result.nl().record("[First page button] working:fail");
        	 result.fail();
         }*/
         
         
         //Check Image magnification zoom
         dc.zoomImageSlideBar();                 
	}

}//end class
