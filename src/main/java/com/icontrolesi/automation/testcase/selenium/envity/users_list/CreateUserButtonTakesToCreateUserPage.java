package com.icontrolesi.automation.testcase.selenium.envity.users_list;

import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.envity.common.EnvitySupport;
import com.icontrolesi.automation.testcase.selenium.envity.common.ProjectPage;
import com.icontrolesi.automation.testcase.selenium.envity.common.UsersListPage;

public class CreateUserButtonTakesToCreateUserPage extends TestHelper{
	
	@Test(description = "Checks whether clicking the 'Create User' button opens up the 'Create User' window.")
	public void test_c265_CreateUserButtonTakesToCreateUserPage(){
		ProjectPage.gotoUserAdministrationPage();
		
		UsersListPage.openCreateUserWindow();
		
		String windowHeader = getText(UsersListPage.CreateUser.HEADER);
		
		softAssert.assertEquals(windowHeader, "Create User", "Create User window opened:: ");
		
		softAssert.assertAll();
	}
}
