package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Driver;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.ReviewTemplatePane;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;

import com.icontrolesi.automation.platform.util.Logger;
/**
 *
 * @author Shaheed Sagar
 *
 *  "(1) Open a new Recenseo session that you can leave unattended for at least 65 minutes.  
	(2) Log into a database, run a search, and review a doc or two, but don't do anything else in that specific Recenseo session for at least 65 minutes (Session time-out should be 60 minutes).  
	(3) Attempt to start working again. Recenseo should present you with the login page, and notify you of session timeout.  
	(4) Verify that you can easily log in without any odd messages/behavior."

*/

public class SessionTimeout extends TestHelper{
   @Test(enabled = false)
   public void test_c401_SessionTimeout(){
	   //loginToRecenseo();
	   handleSessionTimeout(driver);
   }

    private void handleSessionTimeout(WebDriver driver){
    	new SelectRepo(Driver.getDriver(), "Recenseo Demo DB");
        SearchPage.setWorkflow("Search");
        //Starting of test
       
        performSearch();

        ReviewTemplatePane.postDocument();
        
        waitFor(driver, 4000);	//wait at least 65 minutes
        
        try {
			ReviewTemplatePane.postDocument();
		} catch (Exception e) {
			System.out.println( Logger.getLineNumber() + "Some unusual things have happened: " + e.getMessage());
		}
        
        if(SearchPage.isSearchPage()){
        	System.out.println( Logger.getLineNumber() + "User is redirected to Loging page after waiting more than 65 minutes...");
        	
        	//loginToRecenseo();
        }
        	
    }
}
