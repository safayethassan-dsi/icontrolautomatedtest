package com.icontrolesi.automation.testcase.selenium.recenseo.tags.manage_tags_grid;

import java.util.Calendar;

import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.TagManagerInGridView;

public class RemovingDocsFromManageTagInGrid extends TestHelper{
	long timeFrame = Calendar.getInstance().getTimeInMillis();
	String tagName = "RemovingDocsFromManageTagInGrid_c1810_" + timeFrame;
	
	@Test
	public void test_c1810_RemovingDocsFromManageTagInGrid(){
		new SelectRepo(AppConstant.DEM0);
		SearchPage.setWorkflow("Search");
		
		SearchPage.addSearchCriteria("Addressee");
		
		performSearch();
		
		TagManagerInGridView.open();
		TagManagerInGridView.createTopLevelTag(tagName, "");
		
		TagManagerInGridView.addDocumentsForTag(tagName);
		
		TagManagerInGridView.removeDocumentsForTag(tagName);
		
		boolean isDocsRemovedFromSelectedTag = getTotalElementCount(TagManagerInGridView.tagWiseDocCountLocator) == 0;
		
		softAssert.assertTrue(isDocsRemovedFromSelectedTag, "***) Documents removed from selected tag:: ");
		
		TagManagerInGridView.deleteTag(tagName);
		
		softAssert.assertAll();
	}
}
