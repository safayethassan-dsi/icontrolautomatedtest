package com.icontrolesi.automation.testcase.selenium.envity.project_management;

import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.envity.common.CALStandardGeneralConfigurationPage;
import com.icontrolesi.automation.testcase.selenium.envity.common.ManageProjectsPage;
import com.icontrolesi.automation.testcase.selenium.envity.common.OverviewPage;
import com.icontrolesi.automation.testcase.selenium.envity.common.ProjectInfo;
import com.icontrolesi.automation.testcase.selenium.envity.common.ProjectInfoLoader;
import com.icontrolesi.automation.testcase.selenium.envity.common.ProjectPage;

import org.testng.annotations.Test;

public class ConfirmingDeleteActionRemovesTheProjectFromList extends TestHelper {
    
    @Test(description = "Confirming delete action remove the project from both the Project page and the Manage Project page")
    public void test_c1884_ConfirmingDeleteActionRemovesTheProjectFromList() {
        ProjectInfo calStdInfo = new ProjectInfoLoader("cal_std").loadCALStdInfo();
        CALStandardGeneralConfigurationPage.createProject(calStdInfo);

        OverviewPage.waitForBatchJobStart(900);

        ProjectPage.gotoProjectAdministrationPage();

        String pageTitle = getText(ManageProjectsPage.PAGE_TITLE_LOCATOR);

        softAssert.assertEquals(pageTitle, "Manage Projects", "*** Job status is ok::  ");

        ManageProjectsPage.deleteProject(calStdInfo.getProjectName());

        boolean isProjectDeleted = ManageProjectsPage.isProjectPresent(calStdInfo.getProjectName()) == false;

        softAssert.assertTrue(isProjectDeleted, "1. Project is deleted from Manage Project page:: ");

        ProjectPage.gotoHomePage();

        isProjectDeleted = ProjectPage.isProjectFound(calStdInfo.getProjectName()) == false;

        softAssert.assertTrue(isProjectDeleted, "2. Project is deleted from Project Page:: ");

        softAssert.assertAll();
    }
}