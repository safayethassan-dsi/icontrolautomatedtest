package com.icontrolesi.automation.testcase.selenium.recenseo.savedsearch;

import java.util.Date;

import org.openqa.selenium.By;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchesAccordion;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;

public class HoverOverSavedSearchGroupEdit extends TestHelper{
	String savedSearchGroupName = "HoverOverSavedSearchGroupEdit_" + new Date().getTime();
	String editedSavedSearchGroupName = savedSearchGroupName + "_Edited";
	
	By savedGroupEditBtnLocator = By.cssSelector("li[role='treeitem'] > div.hoverDiv");
	
	@Test
	public void test_c28_HoverOverSavedSearchGroupEdit(){
		handleHoverOverSavedSearchGroupEdit();
	}
	
	private void handleHoverOverSavedSearchGroupEdit(){
		new SelectRepo(AppConstant.DEM0);
    	
    	SearchPage.setWorkflow("Search");
    	
    	SearchesAccordion.open();
    	SearchesAccordion.createTopLevelGroup(savedSearchGroupName, "All");
    	SearchesAccordion.searchForSavedSearch(savedSearchGroupName);
    	
    	String editBtnDiplayProperty = getCssValue(savedGroupEditBtnLocator, "display");
    	
    	softAssert.assertEquals(editBtnDiplayProperty, "none", "8. Hovering over a search group displays an Edit icon (Before):: ");
    	
    	hoverOverElement(SearchesAccordion.historyItemsLocation);
    	
    	editBtnDiplayProperty = getCssValue(savedGroupEditBtnLocator, "display");
    	
    	softAssert.assertEquals(editBtnDiplayProperty, "block", "8. Hovering over a search group displays an Edit icon (After):: ");
    	
    	SearchesAccordion.openEditSearchGroupWindow(savedSearchGroupName);
    	
    	String editWindowTitle = getText(SearchesAccordion.savedSearchOrGroupWindowHeaderLocator);
    	
    	softAssert.assertEquals(editWindowTitle, "Edit group", "10. Recenseo provides the user with an editing dialog box:: ");
    	
    	editText(SearchesAccordion.savedGroupNameLocator, editedSavedSearchGroupName); 
    	
    	tryClick(SearchesAccordion.createGroupBtnLocator);
    	acceptAlert(driver);
    	
    	SearchesAccordion.searchForSavedSearch(editedSavedSearchGroupName);
    	
    	boolean isSavedSearchGroupEdited = SearchesAccordion.isTopLevelGroupExists(editedSavedSearchGroupName);
    	
    	softAssert.assertTrue(isSavedSearchGroupEdited, "*** The Search Group Name is renamed provided user entered unique name during renaming:: ");
    	
    	
    	softAssert.assertAll();
	}
}
