package com.icontrolesi.automation.testcase.selenium.recenseo.hotkeys.personalized_hotkeys;

import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;
import com.icontrolesi.automation.testcase.selenium.recenseo.hotkeys.Hotkeys;

public class PostDocumentUsingCustomHotkeys extends TestHelper{
	@Test
	public void test_c1719_PostDocumentUsingCustomHotkeys(){
		new SelectRepo(AppConstant.DEM0);
		
		Hotkeys.open();
		Hotkeys.enableHotkeys();
		Hotkeys.setDefaultHotkeys();
		Hotkeys.setPersonalizedHotkey(Hotkeys.postDocKey, "P");
		Hotkeys.close();
		
		SearchPage.setWorkflow("Search");
		
		SearchPage.addSearchCriteria("Author");
		SearchPage.setCriterionValue(0, "jeff");
		
		performSearch();
		
		int documentIdBeforePosting = DocView.getDocmentId();
		int documentPositionBeforePosting = DocView.getHighlightedDocumentNumber();
		
		Hotkeys.pressHotkey(true, "p");
		
		waitFor(10);
		
		int documentIdAfterPosting = DocView.getDocmentId();
		int documentPositionAfterPosting = DocView.getHighlightedDocumentNumber();
		
		softAssert.assertNotEquals(documentIdAfterPosting, documentIdBeforePosting, "*** The selected document has been posted (ID not same):: ");
		softAssert.assertEquals(documentPositionAfterPosting, documentPositionBeforePosting + 1, "*** The selected document has been posted (Position moved to next):: ");
		
		softAssert.assertAll();
	}
}
