package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.zip.ZipFile;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.FileMethods;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Frame;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;

import com.icontrolesi.automation.platform.util.Logger;


/**
 *
 * @author Ishtiyaq Kamal
 *
 
*/

public class DownloadOrigImage extends TestHelper{
	@Test
	public void test_c224_DownloadOrigImage(){
		handleDownloadOrigImage(driver);
	}
	
    private void handleDownloadOrigImage(WebDriver driver){
    	new SelectRepo(AppConstant.DEM0);
    	
    	DocView.openPreferenceWindow();
    	DocView.checkSelectedColumns("Annotation");
    	DocView.clickCrossBtn();
    	
        waitForFrameToLoad(Frame.MAIN_FRAME);
        SearchPage.addSearchCriteria("filename");
        SearchPage.setCriterionValue(0, "paper");  

        performSearch();
        
        DocView.sortByColumn("Annotation");
        DocView.sortByColumn("Annotation");
        
        DocView.clickOnDocument(1);
        
        int documentId = DocView.getDocmentId();
        
        Calendar cal = Calendar.getInstance();
		long year = cal.get(Calendar.YEAR);
		long month = cal.get(Calendar.MONTH) + 1;
		long day = cal.get(Calendar.DAY_OF_MONTH);
        
		String folderPath = AppConstant.DOWNLOAD_DIRECTORY;
		String fileMatcher = year+"\\.0*"+month+"\\.0*"+day + "\\s\\d{2}(-|_)\\d{2}(-|_)\\d{2}\\.zip";
        
        File [] files = FileMethods.getListOfFiles(folderPath, fileMatcher);
			
		FileMethods.deleteFiles(files);
        
        tryClick(By.id("btnDownload"));
        waitFor(10);
        
        waitForFrameToLoad(Frame.DOWNLOAD_PARENT_FRAME);
        
        tryClick(By.name("redc"));
        tryClick(By.name("doc_set"));
        tryClick(By.name("translation"));
        tryClick(By.name("include_ppdat"));
        
        tryClick(By.id("submitJob"));
        waitFor(40);
        
        ZipFile zipEntry = null;
		try {
			zipEntry = new ZipFile(FileMethods.getListOfFiles(folderPath, fileMatcher)[0].getAbsolutePath());
		} catch (IOException e) {
			e.printStackTrace();
		}
        Enumeration<?> zipEntries = zipEntry.entries();
		boolean isDownloadOk = false;
		while (zipEntries.hasMoreElements()) {
			String zipEntryExtracted = zipEntries.nextElement().toString();
		    System.out.println( Logger.getLineNumber() + zipEntryExtracted);
		    if(zipEntryExtracted.matches("1\\."+documentId+".\\w+-\\d{6}-\\w+-\\d{6}\\.pdf")){
		    	isDownloadOk = true;
		    	break;
		    }
		}
        
        softAssert.assertTrue(isDownloadOk, "(7) Open the zip file and make sure that the file downloaded correctly and naming is correct:: ");
        
        softAssert.assertAll();
  }
}