package com.icontrolesi.automation.testcase.selenium.envity.common;

import org.openqa.selenium.By;

import com.icontrolesi.automation.common.CommonActions;

public class BatchSetDetailsPage implements CommonActions{
	public static final By ITEMLIST_CONTROL_INPUT_LOCATOR = By.cssSelector(".itemListTopPaginationControls input");
	public static final By ITEMLIST_TOTAL_ITEM_COUNT_LOCATOR = By.cssSelector(".textualPaginationControls span:nth-child(6)");
	
	public static final BatchSetDetailsPage bsdp = new BatchSetDetailsPage();
	
	public static int getTotalBatchCount() {
		String itemCountString = bsdp.getText(ITEMLIST_TOTAL_ITEM_COUNT_LOCATOR);
		return itemCountString.length() == 0 ? 0 : Integer.valueOf(itemCountString);
	}
}
