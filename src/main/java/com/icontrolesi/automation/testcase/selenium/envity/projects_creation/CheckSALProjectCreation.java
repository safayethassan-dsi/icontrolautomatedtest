package com.icontrolesi.automation.testcase.selenium.envity.projects_creation;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.testng.annotations.Test;

import com.icontrolesi.automation.common.Frame;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.envity.common.LeftPanelForProjectPage;
import com.icontrolesi.automation.testcase.selenium.envity.common.ProjectInfoLoader;
import com.icontrolesi.automation.testcase.selenium.envity.common.ProjectPage;
import com.icontrolesi.automation.testcase.selenium.envity.common.RelativityWorkspaceHome;
import com.icontrolesi.automation.testcase.selenium.envity.common.SALGeneralConfigurationPage;
import com.icontrolesi.automation.testcase.selenium.envity.common.SALInfo;
import com.icontrolesi.automation.testcase.selenium.envity.common.TaskHistoryPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Driver;

public class CheckSALProjectCreation extends TestHelper{
	
	@Test(description = "Check whether the full execution path for SAL project creation is ok.")
	public void test_0000001_CheckSALProjectCreation(){
		String mainWindowHandler = Driver.getDriver().getWindowHandle();
		
		SALInfo salInfo = new ProjectInfoLoader("sal").loadSALInfo();
		
		SALGeneralConfigurationPage.createSALProject(salInfo);
		//ProjectPage.openProject(projectName);
		//LeftPanelForProjectPage.gotoTaskQueuePage();
		
		
		SALGeneralConfigurationPage.addPopulation(salInfo.getSavedSearchNameForPopulatioin());
		
		// LeftPanelForProjectPage.Indexing.perform();
		
		boolean isPopulationAddingOk = TaskHistoryPage.isCurrentTaskFailed("Add Population") == false;
		
		softAssert.assertTrue(isPopulationAddingOk, "**** Adding Population FAILED!!");
		
		LeftPanelForProjectPage.addJudgementalSample(salInfo.getJudgementalSampleName());
		
		boolean isJudgementalSampleAdded = TaskHistoryPage.isCurrentTaskFailed("Create judgmental Samples") == false;
		
		softAssert.assertTrue(isJudgementalSampleAdded, "**** Adding Judgemental Sample FAILED:: ");

			
		LeftPanelForProjectPage.addActiveSample(100);
		boolean isActiveSampleAdded = TaskHistoryPage.isCurrentTaskFailed("Create Active Samples") == false;
		softAssert.assertTrue(isActiveSampleAdded, "**** Adding Active Samples FAILED:: ");
		
		
		LeftPanelForProjectPage.addControlSample("Fixed Size", 400);
		boolean isControlSampleAdded = TaskHistoryPage.isCurrentTaskFailed("Create Control Samples") == false;
		softAssert.assertTrue(isControlSampleAdded, "**** Adding Control Samples FAILED:: ");
	
		String projectName = salInfo.getProjectName();
		
		String cSampleName = String.format("%s %s", projectName, "CSmpl");
		String jSampleName = String.format("%s %s", projectName, "JSmpl");
		String aSampleName = String.format("%s %s", projectName, "ASmpl");
		
	/* 	System.out.println(jSampleName + "******************");
		
		ProjectPage.performLogout();
		
		RelativityWorkspaceHome.loginToRelativity();
		RelativityWorkspaceHome.selectWorkspace("iControl SvP");
		
		RelativityWorkspaceHome.openSavedSearch(jSampleName);
		
		RelativityWorkspaceHome.setNumberofDocumentsPerPage(1000);
		
//		Frame.switchToExternalFrame();
		tryClick(RelativityWorkspaceHome.CB_FILL_ITEMLIST_LOCATOR, 2);
		tryClick(RelativityWorkspaceHome.EDIT_BTN_LOCATOR, 10);
		switchToWindow(1);
		
		
		RelativityWorkspaceHome.selectFieldLayout("Dsi-Layout");
		RelativityWorkspaceHome.selectReviewField("dsi_review_field_09", "Very Good");
		RelativityWorkspaceHome.saveSelectedField();
		
		switchToWindow(mainWindowHandler);
		
		
		RelativityWorkspaceHome.openSavedSearch(cSampleName);
		
		//RelativityWorkspaceHome.waitForLoadingComplete();
		
		landOnLoginPage();
		performLoginToEnvity();
		
		ProjectPage.openProject(projectName);
		
		LeftPanelForProjectPage.Sync.perform();
		
		System.err.println(Driver.getDriver().getWindowHandles().size()+"****"); */
		
		softAssert.assertAll();
	}
}
