package com.icontrolesi.automation.testcase.selenium.recenseo.tags.tags_accordion;

import java.util.Calendar;

import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Frame;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.TagAccordion;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.TagManagerInGridView;

public class AddingSubTag extends TestHelper{
	long timeFrame = Calendar.getInstance().getTimeInMillis();
	String parentTag = "AddingTag_c1794_" + timeFrame;
	String subTagToBeCreated = "AddingTag_c1794_Sub_" + timeFrame;
	
	@Test
	public void test_c1795_AddingSubTag(){
		new SelectRepo(AppConstant.DEM0);
		SearchPage.setWorkflow("Search");
		
		TagAccordion.open();
		TagAccordion.createTag(parentTag, "");	
		TagAccordion.createSubTagFor(parentTag, subTagToBeCreated, "");
		
		boolean isSubtagCreated = TagAccordion.isTagFound(subTagToBeCreated);
		
		switchToDefaultFrame();
		waitForFrameToLoad(Frame.MAIN_FRAME);
		
		performSearch();
		
		TagManagerInGridView.open();
		TagManagerInGridView.deleteTag(subTagToBeCreated);
		
		softAssert.assertTrue(isSubtagCreated, "***) New tag appeared on the Accordion:: ");
		
		softAssert.assertAll();
	}
}
