package com.icontrolesi.automation.testcase.selenium.recenseo.tags.tags_accordion;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.ManageTags;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.TagAccordion;

public class SearchForTags extends TestHelper{
	String tagNameToSearch = "SecurityTagTestSubTag";
	
	@Test
	public void test_c1792_SearchForTags(){
		new SelectRepo(AppConstant.DEM0);
		SearchPage.setWorkflow("Search");
		
		TagAccordion.open();
		TagAccordion.createTag(tagNameToSearch, "");	
		TagAccordion.selectFilter("All");
		enterText(TagAccordion.tagSearchFieldLocator, tagNameToSearch);
		
		waitFor(2);
		
		List<WebElement> suggetionList = getElements(By.cssSelector(".ui-autocomplete li"));
		
		softAssert.assertEquals(suggetionList.size(), 1, "4. As you're typing, confirm that the predictive text drop-down shows reasonable results(Total Count match):: ");
		softAssert.assertEquals(getText(suggetionList.get(0)), tagNameToSearch, "4. As you're typing, confirm that the predictive text drop-down shows reasonable results(Search term match):: ");
		
		tryClick(TagAccordion.tagFindBtnLocator, 10);
		
		boolean isSearchOk = TagAccordion.isTagFound(tagNameToSearch);
		
		softAssert.assertTrue(isSearchOk, "***) Correct tag appeared in the search results:: ");
		
		ManageTags.open();
		ManageTags.deleteTag(tagNameToSearch);
		
		softAssert.assertAll();
	}
}
