package com.icontrolesi.automation.testcase.selenium.envity.create_user;

import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.envity.common.ProjectPage;
import com.icontrolesi.automation.testcase.selenium.envity.common.UsersListPage;
import org.testng.annotations.Test;

public class CancelBtnWorksProperly extends TestHelper{
	private String password = "Password length is-22";
	
	@Test(description="When clicked on 'Cancel' button, it returns to User List")
	public void test_c276_CancelBtnWorksProperly() {
		ProjectPage.gotoUserAdministrationPage();
		
		UsersListPage.openCreateUserWindow();
		


		softAssert.assertEquals(getText(UsersListPage.CreateUser.HEADER), "Create User", "***) Create User window opened:: ");

		UsersListPage.CreateUser.closeWindow();

		softAssert.assertEquals(getTotalElementCount(UsersListPage.CreateUser.HEADER), 0, "*** Create User window closed:: ");
		
		softAssert.assertAll();
	}
}
