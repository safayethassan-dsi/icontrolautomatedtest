package com.icontrolesi.automation.testcase.selenium.envity.basic_utility_tests;

import javax.swing.JOptionPane;

import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.envity.common.ManageProjectsPage;
import com.icontrolesi.automation.testcase.selenium.envity.common.ProjectPage;

import org.testng.annotations.Test;

public class CleanUpProjects extends TestHelper{
    final String SEARCH_MSG = "No data available in table";

    @Test
    public void test_00000_CleanUpProjects(){
        if(ProjectPage.getTotalProjectCount() == 0){
            JOptionPane.showMessageDialog(null, "No Projects to Delete!!");
        }else{
            ProjectPage.gotoProjectAdministrationPage();

            deleteProjectBy("SL_");  
            deleteProjectBy("CL_");       
            
            JOptionPane.showMessageDialog(null, "Project Clean up process completed!!!");
        }
    }

    void deleteProjectBy(String name){
        while(true){
            editText(ManageProjectsPage.searchBoxLocator, name);
            if(getText(ManageProjectsPage.PROJECT_COL).equals(SEARCH_MSG)){
                break;
            }else{
                ManageProjectsPage.deleteProject();
            }
        }
    }
}