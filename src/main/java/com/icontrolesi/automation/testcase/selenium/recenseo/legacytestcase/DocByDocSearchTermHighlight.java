package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Frame;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchesAccordion;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;

/**
 *
 * @author Milon
 *
 *			DB: DEM0 
 *			(1) Run each saved search below, and confirm that each term
 *			 searched results in correct highlighting on the result document:  
  
 *			(a) SAVED SEARCH DBD-0074-001 (Doc ID 30 should result)  
 *			Multiple terms: CANTARELLA DEJOUANY FRIEDMANN  
 *			Exact Quote: Rule 28.2.1, the undersigned counsel of record certifies that the following 
 *			Leading Wildcards: *PPINOT  
 * 			Trailing Wildcards:  monospace* 
 *			Proximity Short:  Abernathy Joplin~6  
 *			Proximity Long: Frederic Appellees~100 (Search Returns w/o highlight on 10/28)  
  
 *			(b) SAVED SEARCH DBD-0074-002 (Doc ID 1225659 should result along with 1225694, 1225694)  
 *			Email Address Variations (user only, domain only, complete): Jad@Ubuntu.com dr.khaled.hosny* *eglug.org  
 *			Like these words: adfantage localiser  
 *			Non-English: ????? (apparently mantis doesn't let us display Arabic yet, 
 * 			but the saved search includes an Arabic term that Recenseo must highlight to pass this test).
 
 **/

public class DocByDocSearchTermHighlight extends TestHelper{
	@Test
	public void test_c249_DocByDocSearchTermHighlight(){
		handleDocByDocSearchTermHighlight(driver);
	}
	
	private void handleDocByDocSearchTermHighlight(WebDriver driver){
		new SelectRepo(AppConstant.DEM0);
		
		DocView.openPreferenceWindow();
    	DocView.checkSelectedColumns("Document ID");
    	DocView.clickCrossBtn();
		
		SearchPage.setWorkflow("Search");
		
		SearchesAccordion.open();
		SearchesAccordion.selectFilter("All");
		SearchesAccordion.openSavedSearchForView("DBD-0074-001");
				
		softAssert.assertEquals(DocView.getDocmentId(driver), 30, "1a.) Doc ID 30 should result:: ");
		
		DocView.selectView("Text");
		
		DocView.openHighlightWindow();
		
		softAssert.assertEquals(getColorFromElement(By.id("0_anchor"), "background-color"), "#C2D985", "1a.) Proximity Short:  Abernathy Joplin~6:: ");
		softAssert.assertEquals(getColorFromElement(By.id("1_anchor"), "background-color"), "#FFB7B7", "1a.) Exact Quote: Rule 28.2.1, the undersigned counsel of record certifies that the following :: ");
		softAssert.assertEquals(getColorFromElement(By.id("2_anchor"), "background-color"), "#FFDE89", "1a.) Multiple terms: CANTARELLA DEJOUANY FRIEDMANN:: ");
		softAssert.assertEquals(getColorFromElement(By.id("3_anchor"), "background-color"), "#B1DDF3", "1a.) Proximity Long: Frederic Appellees~100:: ");
		
		SearchPage.goToDashboard();
		
		waitForFrameToLoad(Frame.MAIN_FRAME);
		SearchesAccordion.open();
		SearchesAccordion.openSavedSearchForView("DBD-0074-002");
		
		waitForNumberOfElementsToBeGreater(By.cssSelector("td[aria-describedby='grid_inv_id']"), 1);
		softAssert.assertEquals(DocView.getDocmentId(driver), 1225659, "1b.) Doc ID should be 1225659:: ");
		softAssert.assertEquals(getElements(By.cssSelector("td[aria-describedby='grid_inv_id']")).size(), 3, "1b.) Doc ID 1225659 should result along with 2 other IDs:: ");
		
		DocView.selectView("Text");
		
		tryClick(By.id("toggleHighlightTerms"));
		
		softAssert.assertEquals(getColorFromElement(By.id("0_anchor"), "background-color"), "#C2D985", "1b.) Email Address Variations (user only, domain only, complete)::: ");
		softAssert.assertEquals(getColorFromElement(By.id("1_anchor"), "background-color"), "#FFB7B7", "1b.) Non-English:: ");
		softAssert.assertEquals(getColorFromElement(By.id("2_anchor"), "background-color"), "#FFDE89", "1b.) Like these words: adfantage localiser:: ");
		
		softAssert.assertAll();
	}
}
