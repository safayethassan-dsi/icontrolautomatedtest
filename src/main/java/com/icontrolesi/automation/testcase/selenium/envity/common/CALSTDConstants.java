package com.icontrolesi.automation.testcase.selenium.envity.common;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.icontrolesi.automation.platform.config.Configuration;

public class CALSTDConstants {
	public static final String PROJECT_NAME_PREFIX = Configuration.getConfig("envityFieldsValues.project_name_prefix");
	public static final String PROJECT_NAME = String.format("%s%s", PROJECT_NAME_PREFIX, new SimpleDateFormat("ddMMYYY_hhmm").format(new Date()));
	public static final String ENVIZE_INSTANCE = Configuration.getConfig("envityFieldsValues.envize_instance");
	public static final String RELATIVITY_INSTANCE = Configuration.getConfig("envityFieldsValues.relativity_instance");
	public static final String WORKSPACE = Configuration.getConfig("envityFieldsValues.workspace");
	public static final String DOCUMENT_SET = Configuration.getConfig("envityFieldsValues.document_set");
	public static final String PREDICTION_FIELD = Configuration.getConfig("envityFieldsValues.prediction_field");
	public static final String TRUE_POSITIVE_VALUE = Configuration.getConfig("envityFieldsValues.true_positive_value");
	public static final String TRUE_NEGATIVE_VALUE = Configuration.getConfig("envityFieldsValues.true_negative_value");
	public static final String SAVED_SEARCH_NAME_SUFFIX = Configuration.getConfig("envityFieldsValues.saved_search_name_suffix");
	public static final String BATCHSET_NAME_SUFFIX = Configuration.getConfig("envityFieldsValues.batchset_name_suffix");
	public static final String JUDGEMENTAL_SAMPLE = Configuration.getConfig("envityFieldsValues.judgemental_sample");
	public static final String SYNTHETIC_DOCUMENT = Configuration.getConfig("envityFieldsValues.synthetic_document");
	
	
	public static final String CHECKPOINT_TYPE = Configuration.getConfig("envityFieldsValues.checkpoint_type");
	public static final String NUMBER_OF_BATCHES = Configuration.getConfig("envityFieldsValues.number_of_batches");
	public static final String PERCENTAGE = Configuration.getConfig("envityFieldsValues.percentage");
	public static final String STOP_BATCHING = Configuration.getConfig("envityFieldsValues.stop_batching");
	public static final String CREATE_QC_ELUSION_BATCH = Configuration.getConfig("envityFieldsValues.create_qc_elusion_batch");
	public static final String QC_ELUSION_BATCH_SIZE = Configuration.getConfig("envityFieldsValues.qc_elusion_batch_size");
	public static final String NUMBER_OF_LATEST_FOUND_DOCS_FOR_REVIEW_BATCH = Configuration.getConfig("envityFieldsValues.number_of_latest_found_docs_for_review_batch");	
}
