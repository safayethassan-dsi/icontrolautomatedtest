package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.config.Configuration;
import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Download;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.FileMethods;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Frame;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;


import com.icontrolesi.automation.platform.util.Logger;

public class ActionsDownloadNative extends TestHelper{
	@Test
	public void test_c179_ActionsDownloadNative(){
		new SelectRepo(AppConstant.DEM0);
		
		DocView.openPreferenceWindow();
    	DocView.checkSelectedColumns("File Name");
    	DocView.closePreferenceWindow();
		
		SearchPage.setWorkflow("Search");
		
		SearchPage.addSearchCriteria("Document Name");
		SearchPage.setOperatorValue(0, "is not empty");
		
		performSearch();
		
		DocView.setNumberOfDocumentPerPage(500);
		
		Set<String> itemSetInGridView = getListOfItemsFrom(DocView.gridRowLocator, file -> file.getText().trim()).stream().collect(Collectors.toSet());
		
		List<String> fileListInTheExtractedFolder = new ArrayList<>();
		
		DocView.selectAllDocuments();
		
		new DocView().clickActionMenuItem("Prepare Download");
		
		driver.switchTo().frame(getElement(By.cssSelector("iframe[src^='javascript:']")));
		
		Download.openFileTypePrefs();
        Download.setDownloadPreferenceAllTo("Native");
		Download.closeFileTypePrefs();
		
		new DocView().clickSubmitButton();
		
		String downloadConfirmationMsg = getElement(By.tagName("body")).getText().trim();
 		
	    softAssert.assertTrue(downloadConfirmationMsg.contains("Recenseo is preparing your download in the background."), "4) Submit the job:: ");
	    
	    tryClick(By.xpath("//a[text()='Click HERE to go directly to \"Downloads\" now']"), By.className("BlueAnchor"));
	    waitFor(driver, 15);
	    
	    List<WebElement> downloadLinks =  getElements(By.className("BlueAnchor"));
	    
	    WebElement downloadableLink = downloadLinks.get(downloadLinks.size() - 1);
	    
	    String downloadableLinkName;
	    
	    System.out.println(downloadableLink.getText() + "&&&&&&&&&&&");
	    if(Configuration.getConfig("selenium.browser").equals("chrome")){
	    	downloadableLinkName = getText(downloadableLink).replaceAll(":|-", "_");
	    }else{
	    	downloadableLinkName = getText(downloadableLink).replaceAll(":|-", "_");
	    }
	    
	    System.out.println( Logger.getLineNumber() + "FileName: " + downloadableLinkName);
	    
	    String downloadableFile = AppConstant.DOWNLOAD_DIRECTORY + downloadableLinkName;
	    String extractedFolderLocation = downloadableFile.replaceAll("\\.zip", "");
	    
	    FileMethods.deleteFileWithPattern(AppConstant.DOWNLOAD_DIRECTORY, "^"+downloadableLinkName.replaceAll(".zip", "")+".*\\.zip");
	    
	    
	    
	    Set<String> contentInDialog = getListOfItemsFrom(By.cssSelector("#contentDialog > ul > li"), file -> getText(file)).stream().collect(Collectors.toSet());
	    
	    downloadableLink.click();
	    Download.waitForFileDownloadToComplete(downloadableFile, 1800);
	     
	    List<WebElement> contentLinks = getElements(By.cssSelector("a[title='Contents']"));
	    contentLinks.get(contentLinks.size() - 1).click();
	    waitFor(5);
	    
	    unzip(downloadableFile, extractedFolderLocation);
	    
	    File [] listOfFiles = new File(extractedFolderLocation).listFiles();
	    
	    Set<String> fileList = Stream.of(listOfFiles).map(file -> file.getName()).collect(Collectors.toSet());
	    
	    softAssert.assertEquals(itemSetInGridView, fileListInTheExtractedFolder, "6) Download contains native versions with names that include sort number, inv_id and filename (from CFIL):: ");
	    softAssert.assertEquals(fileListInTheExtractedFolder, contentInDialog, "6) Download contains native versions with names that include sort number, inv_id and filename (from CFIL):: ");
	    
	    FileMethods.deleteFileOrFolder(downloadableFile);
	    FileMethods.deleteFileOrFolder(extractedFolderLocation);
	    
	    getElements(By.name("chkDownload")).stream().forEach(link -> link.click());
	    Download.deleteSelectDownloads();
	    
	    switchToDefaultFrame();
        waitForFrameToLoad(Frame.MAIN_FRAME);
        tryClick(getElements(By.xpath("//span[text()='Close']")).get(1), 5);		
	}
}
