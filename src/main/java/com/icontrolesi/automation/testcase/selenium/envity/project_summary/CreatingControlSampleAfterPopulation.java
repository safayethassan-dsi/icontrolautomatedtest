package com.icontrolesi.automation.testcase.selenium.envity.project_summary;

import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.envity.common.LeftPanelForProjectPage;
import com.icontrolesi.automation.testcase.selenium.envity.common.ProjectInfo;
import com.icontrolesi.automation.testcase.selenium.envity.common.ProjectInfoLoader;
import com.icontrolesi.automation.testcase.selenium.envity.common.SALGeneralConfigurationPage;

public class CreatingControlSampleAfterPopulation extends TestHelper {

	@Test(description = "Add Control Sample after adding Population to a new project")
	public void test_388_CreatingControlSampleAfterPopulation() {
		ProjectInfo salInfo = new ProjectInfoLoader("sal").loadSALInfo();

		SALGeneralConfigurationPage.createSALProject(salInfo);;
		LeftPanelForProjectPage.gotoTaskQueuePage();

		checkPopulationAddingWorksForSAL();

		LeftPanelForProjectPage.syncProject();

		checkControlSampleCreationForSAL();
		
		softAssert.assertAll();
	} 

	void checkPopulationAddingWorksForSAL(){
		
		boolean isProjectCreationOk = SALGeneralConfigurationPage.isPorjectCreated();
		
		softAssert.assertTrue(isProjectCreationOk, "Simple CAL project creation FAILED:: ");

		SALGeneralConfigurationPage.addPopulation("1 of 11k");

		boolean isPopulationAdded = SALGeneralConfigurationPage.isPopulationAdded();

		softAssert.assertTrue(isPopulationAdded, "SAL Population Adding FAILED:: ");
	}

	void checkControlSampleCreationForSAL(){
		LeftPanelForProjectPage.addControlSample("");
		boolean isControlSampleAdded = SALGeneralConfigurationPage.isControlSampleAdded();

		softAssert.assertTrue(isControlSampleAdded, "SAL Control Sample Adding FAILED:: ");
	}
}
