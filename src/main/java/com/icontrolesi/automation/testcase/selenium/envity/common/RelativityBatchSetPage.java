package com.icontrolesi.automation.testcase.selenium.envity.common;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.icontrolesi.automation.common.CommonActions;

public class RelativityBatchSetPage implements CommonActions{
	public static final By SHOW_FILTER_BTN_LOCATOR = By.cssSelector("button[title='Show Filters']");
	public static final By NAME_FILTER_INPUT_LOCATOR = By.cssSelector("input[title='Hit enter to set filter']");
	
	public static final By BATCHSET_NAME_COLUMN_LOCATOR = By.cssSelector("#fil_itemListFUI tr[class^='ui-widget-content'] td:nth-child(4)");
	
	
	public static final RelativityBatchSetPage rbsp = new RelativityBatchSetPage();
	
	public static void filterByName(String batchsetName) {
		int countBefore = rbsp.getTotalElementCount(BATCHSET_NAME_COLUMN_LOCATOR);
		
		rbsp.tryClick(SHOW_FILTER_BTN_LOCATOR);
		rbsp.enterText(NAME_FILTER_INPUT_LOCATOR, String.format("%s%s", batchsetName, "\n"));
		
		//RelativityWorkspaceHome.waitForLoadingComplete();
		rbsp.waitForNumberOfElementsToBeLessThan(NAME_FILTER_INPUT_LOCATOR, countBefore);
		
		System.err.printf("\nFiltering completed for %s ...\n", batchsetName);
	}
	
	public static void openBatchset(String batchsetName) {
		filterByName(batchsetName);
		
		List<WebElement> batchsetList = rbsp.getElements(BATCHSET_NAME_COLUMN_LOCATOR);
		
		for(WebElement batchset : batchsetList) {
			System.err.printf("Batchset: %s ", batchset.getText().trim());
			if(batchset.getText().equals(batchsetName)) {
				batchset.click();
				break;
			}
		}
		
		/*rbsp.getElements(BATCHSET_NAME_COLUMN_LOCATOR).stream()
			.filter(name -> name.getText().trim().equals(batchsetName))
			.findFirst().get().click();*/
		
		//RelativityWorkspaceHome.waitForLoadingComplete();
		
		rbsp.waitForInVisibilityOf(SHOW_FILTER_BTN_LOCATOR);
		rbsp.waitForInVisibilityOf(By.className("itemTable"));
		
		System.err.printf("Batchset '%s' opened....");
	}
	
}
