package com.icontrolesi.automation.testcase.selenium.recenseo.common;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.icontrolesi.automation.common.CommonActions;

import com.icontrolesi.automation.platform.util.Logger;

public class ManageDocumentSecurity implements CommonActions{
	static WebDriver driver;
	public static By teamListLocator = By.cssSelector("#teamsec-teamList > div > label");
	public static By blockUnblockBtnLocator = By.cssSelector("span.ui-button-text");
	public static By blockUnblockMsgLocator = By.id("teamsec-messageContainer");
	
	
	static ManageDocumentSecurity manageDocumentSecurity = new ManageDocumentSecurity();
	
	
	public static void clickBlockButton(){
    	clickBlockUnblockClearButton("Block");
    }
    
    public static void clickUnblockButton(){
    	clickBlockUnblockClearButton("Unblock");
    }
    
    public static void clickClearButton(){
    	clickBlockUnblockClearButton("Clear");
    }
    
    public static void clickBlockUnblockClearButton(String buttonText){
    	List<WebElement> buttonList = manageDocumentSecurity.getElements(blockUnblockBtnLocator);
    	for(WebElement button : buttonList){
    		if(button.getText().trim().equalsIgnoreCase(buttonText)){
    			button.click();
    			manageDocumentSecurity.waitForVisibilityOf(blockUnblockMsgLocator);
    			manageDocumentSecurity.waitForInVisibilityOf(blockUnblockMsgLocator);
    			System.out.println( Logger.getLineNumber() + buttonText + " button clicked...");
    			break;
    		}
    	}
    } 
	
	public static void selectTeam(String teamName){
		List<WebElement> teamLocators = manageDocumentSecurity.getElements(teamListLocator);
		for(WebElement team : teamLocators){
			String teamNameText = team.getText().trim();
			System.out.println( Logger.getLineNumber() + "TeamName: "+ teamNameText+"***");
			if(teamNameText.startsWith(teamName + ":")){
				//manageDocumentSecurity.tryClick(element);
				WebElement teamSelectionBox = team.findElement(By.tagName("input"));
				if(teamSelectionBox.isSelected()){
					System.out.println( Logger.getLineNumber() + "Team " + teamName + " already selected...");
				}else{
					teamSelectionBox.click();
					System.out.println( Logger.getLineNumber() + "Team " + teamName + " selected...");
				}
				System.out.println( Logger.getLineNumber() + "Team '" + teamName + "' selected...");
				break;
			}
		}
	}
	
	public static void blockTeam(String teamName){
		selectTeam(teamName);
		clickBlockButton(); 
		//System.out.println( Logger.getLineNumber() + DocView.getNumberOfSelectedDocuments() + " documents blocked for '" + teamName + "...");
		System.out.println( Logger.getLineNumber() + "Some documents blocked for '" + teamName + "...");
	}
	
	public static void unBlockTeam(String teamName){
		selectTeam(teamName);
		clickUnblockButton();
		System.out.println( Logger.getLineNumber() + "Some documents unblocked for '" + teamName + "...");
	}
	
	public static void closeWindow(){
		  manageDocumentSecurity.switchToDefaultFrame();
		  manageDocumentSecurity.waitForFrameToLoad(Frame.MAIN_FRAME);
		  List<WebElement> allButtons = manageDocumentSecurity.getElements(By.tagName("button"));
		  for(WebElement e : allButtons){
			 if(e.isDisplayed()){ 
			  if(e.getAttribute("title").equalsIgnoreCase("Close")){
				  e.click();
				  System.out.println( Logger.getLineNumber() + "'Manage Document Security' Window closed...");
				  manageDocumentSecurity.waitFor(10);
				  break;
			  }
			 }
		  }
	}
}
