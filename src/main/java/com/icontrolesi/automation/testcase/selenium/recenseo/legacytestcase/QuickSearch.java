package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Frame;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;


/**
 * @author atiq,ishtiyaq
 * 
 * DB: DEM4 
(1) Go to search page, enter "Orange" into the "Quick Search" text box and click enter instead of run search.***  
(2) Go to text view and confirm that the term you searched for is highlighted.  
(3) Return to the search page and verify that it did not create two redundant sets of criteria. 
 
***If the search term is in email/web domains like orange.com or orange.com.cn will not highlight, as of the 2014.1 release, unless there is a wildcard after the term. Search term orange* will highlight orange.com, jeff@orange.com, etc 
 * 
 */

public class QuickSearch extends TestHelper{
	String searchTerm ="Orange";
	
   @Test
   public void test_c103_QuickSearch(){
	   //loginToRecenseo();
	   handleQuickSearch(driver);
   }


    protected void handleQuickSearch(WebDriver driver){
		new SelectRepo(AppConstant.DEM4);
		
		SearchPage.setWorkflow("Search");
		
		performQuickSearch(searchTerm);
		 
		DocView.selectView("Text");
		
		DocView.findNextHighlight();
		
		boolean isSearchTermHighlighted = getTotalElementCount(By.cssSelector("span[id^='term']")) > 0;
		
		softAssert.assertTrue(isSearchTermHighlighted, "2) Go to text view and confirm that the term you searched for is highlighted:: ");
		
		SearchPage.goToDashboard();
		
		waitForFrameToLoad(Frame.MAIN_FRAME);
		
		String operatorUsed = SearchPage.getOperatorValue(0);
		String searchTextUsed = SearchPage.getCriterionValue(0);
		
		softAssert.assertEquals(operatorUsed, "all of these words", "3) Criteri added:: ");
		softAssert.assertEquals(searchTextUsed, searchTerm, "3) Criteri added:: ");
		
		softAssert.assertAll();
	}
}
