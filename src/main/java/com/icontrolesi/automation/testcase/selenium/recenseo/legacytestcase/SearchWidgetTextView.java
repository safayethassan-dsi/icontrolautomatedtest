package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;

import com.icontrolesi.automation.platform.util.Logger;


public class SearchWidgetTextView extends TestHelper{
public static final int SORTING_TIME = 50;
public static final String SORTING_MESSAGE = "Confirm each time that the re-sort works properly(sorting done in an expected amount of time):";
public static int alertPresent=0;

@Test
public void test_c236_SearchWidgetTextView(){
	//loginToRecenseo();
	handleSearchWidgetTextView(driver);
}

private void handleSearchWidgetTextView(WebDriver driver){
			new SelectRepo(AppConstant.DEM0);

			SearchPage.setWorkflow("Search");
				
			SearchPage.addSearchCriteria("Document ID");
			SearchPage.setCriterionValue(0, "26");
			
			performSearch();
			
			DocView.selectView("Text");
			
			DocView.searchTextInDocument("March");
			
			int matchNum = DocView.getNumberOfMatches();
			
			String matchString = "'Hit Find"+0+" Selected'";
			String cssSelectorMatchString = "span[class="+matchString+"]";
		
			int testPass=1;
			
			for(int i=0;i<matchNum;i++){
				matchString="'Hit Find"+i+" Selected'";
			    cssSelectorMatchString="span[class="+matchString+"]";
				WebElement spanNode= SearchPage.waitForElement(cssSelectorMatchString, "css-selector");
				String colour= spanNode.getCssValue("background-color");
				
				if(colour.equalsIgnoreCase("rgba(10, 36, 106, 1)")){
					testPass=1;
				}else{
					testPass=0;
				}
				System.out.println( Logger.getLineNumber() + colour);
				
				DocView.findNextTextMatch();
			}
			
			
			Assert.assertEquals(testPass, 1, "3)Search for words witin the text make sure it highlights them and moves to the next match if there is one:");
		}
}
