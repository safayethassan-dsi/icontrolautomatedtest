package com.icontrolesi.automation.testcase.selenium.envity.users_list;

import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.envity.common.ManageUserProjectPage;
import com.icontrolesi.automation.testcase.selenium.envity.common.ProjectPage;
import com.icontrolesi.automation.testcase.selenium.envity.common.UsersListPage;

public class ManageProjectsButtonTakesToManageProjectsPage extends TestHelper{
	
	@Test(description = "Checks whether clicking the 'Manage User' icons opens up the 'Manage User Project' page.")
	public void test_c267_ManageProjectsButtonTakesToManageProjectsPage(){
		ProjectPage.gotoUserAdministrationPage();
		
		UsersListPage.gotoManageUserProjectPageFor(AppConstant.USER);
		
		String windowHeader = getText(ManageUserProjectPage.PAGE_TITLE);
		
		softAssert.assertEquals(windowHeader, "Manage User Project", "Create User window opened:: ");
		
		softAssert.assertAll();
	}
}