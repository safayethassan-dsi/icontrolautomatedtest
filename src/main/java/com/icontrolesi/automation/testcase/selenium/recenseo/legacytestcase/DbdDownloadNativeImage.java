package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;


import java.io.File;
import java.util.Calendar;
import java.util.zip.ZipFile;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.FileMethods;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.FolderAccordion;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.ReviewTemplatePane;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;

public class DbdDownloadNativeImage extends TestHelper{
	@Test
	public void test_c235_DbdDownloadNativeImage(){
		handleDbdDownloadNativeImage(driver);
	}

	private void handleDbdDownloadNativeImage(WebDriver driver){
		new SelectRepo(AppConstant.DEM0);
		
		DocView.openPreferenceWindow();
		DocView.deselectAllDocument();
		DocView.checkSelectedColumns("File Name");
		DocView.clickCrossBtn();

		SearchPage.setWorkflow("Search");
		
		FolderAccordion.open();
		FolderAccordion.expandFolder("Email");
		FolderAccordion.openFolderForView("Foster, Ryan e-mail");
		
		DocView.sortByColumn("File Name");
		DocView.sortByColumn("File Name");
		
		DocView.clickOnDocument(1);
		
		int documentId = DocView.getDocmentId();
		String documentName = DocView.getDocumentName(1);
		
		ReviewTemplatePane.openDownloadWindow();
		ReviewTemplatePane.openFileTypePrefs();
		ReviewTemplatePane.setDownloadPreferenceAllTo("Imaged");
		ReviewTemplatePane.closeFileTypePrefs();
		ReviewTemplatePane.clickIncludeCompleteDocFamily();
		
		Calendar cal = Calendar.getInstance();
		long year = cal.get(Calendar.YEAR);
		long month = cal.get(Calendar.MONTH) + 1;
		long day = cal.get(Calendar.DAY_OF_MONTH);
		
		//String folderPath = Configuration.getConfig("selenium.download_dir");
		String folderPath = AppConstant.DOWNLOAD_DIRECTORY;
		String fileMatcher = year+"\\.0*"+month+"\\.0*"+day + "\\s\\d{2}(-|_)\\d{2}(-|_)\\d{2}\\.zip";
	        
	    File [] files = FileMethods.getListOfFiles(folderPath, fileMatcher);
				
		FileMethods.deleteFiles(files);
		
		tryClick(By.name("mergePdfs"), 1);
	        
        tryClick(By.name("anno"), 1);
        tryClick(By.name("redc"), 1);
       
        tryClick(By.name("uidd"), 1);
        tryClick(By.name("bpro"), 1);
        tryClick(By.name("useSeparator"), 1);
        tryClick(By.name("doc_set"), 1);
        tryClick(By.name("translation"), 1);
		
		tryClick(By.id("submitJob"));
		waitFor(15);
		
		ZipFile zipEntry = getZipFile(FileMethods.getListOfFiles(folderPath, fileMatcher)[0].getAbsolutePath());
		
        boolean isDownloadOk = false;
		
	    isDownloadOk = isZipContentsOk(zipEntry, "merged1.pdf");
		
	    closeZipFile(zipEntry);
        
        softAssert.assertTrue(isDownloadOk, "(8) Open the zip file and make sure that the file(s) downloaded correctly and naming is correct and Confirm Redactions and Annotations match location on image in Doc by Doc:: ");
	     
	    ReviewTemplatePane.closePopupWindow();
       
	    ReviewTemplatePane.openDownloadWindow();
       
       
        files = FileMethods.getListOfFiles(folderPath, fileMatcher);
		
	    FileMethods.deleteFiles(files);
	    
	    tryClick(By.name("include_ppdat"), 1);
       
        tryClick(By.id("submitJob"), 15);
       
        zipEntry = getZipFile(FileMethods.getListOfFiles(folderPath, fileMatcher)[0].getAbsolutePath());
       
        isDownloadOk = isZipContentsOk(zipEntry, "1\\."+documentId+"\\.\\w+-\\d{7}-\\w+-\\d{7}\\." + "pdf||marged\\.pdf||1\\." + documentId + "\\." + documentName);
       
        softAssert.assertTrue(isDownloadOk, "(11) Open the zip file and make sure that the file(s) downloaded correctly and confirm bates numbers are present (If produced should be Doc ID.Bates Number -Bates Number.pdf if not produced then just Doc ID.pdf unless:: ");
       
        softAssert.assertAll();	
	}
}
