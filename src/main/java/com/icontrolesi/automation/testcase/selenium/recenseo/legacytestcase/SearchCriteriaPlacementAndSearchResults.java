package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Frame;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchesAccordion;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Utils;

/**
 *
 * @author Shaheed Sagar
 *
 *  "*Will Require Three Users (or three User IDs, an ""Admin"" ID and two ""Test User"" ID) 
	(1) Log into DEM0 and Select Administrative Settings under Settings Menu and Choose Manage Users 
	(2) Click on the Teams tab 
	(3) Confirm that one ""Test User"" ID is on the same Team as the ""Admin"" ID and that the other ""Test User"" ID is on a different team. 
	(4) Return to the Search Page and Create a new Search that is Saved for Team Members Only 
	(5) Confirm that the ""Test User"" ID on the same team can view the Saved Search and Run it to see the documents 
	(6) Confirm that the ""Test User"" ID on the different team cannot view the Saved Search or Run it to see the documents 
	(7) Return to the Search Page and create a new Saved Search with the same name that is Public 
	(8) Confirm that you are prompted to overwrite existing search 
	(9) Confirm that both ""Test User"" ID can now view the Saved Search and Run it to see the documents"

*/

public class SearchCriteriaPlacementAndSearchResults extends TestHelper{
    @Test
    public void test_c1650_SearchCriteriaPlacementAndSearchResults(){
    	//loginToRecenseo();
    	handleSearchCriteriaPlacementAndSearchResults(driver);
    }

    private void handleSearchCriteriaPlacementAndSearchResults(WebDriver driver){
        new SelectRepo(AppConstant.DEM0);
        
        SearchPage.setWorkflow("Search");
        
        SearchesAccordion.open();
        SearchesAccordion.selectFilter("All");
        SearchesAccordion.addSelectedToSearch("iControl_032 - Complicated Saved Search");
         
        switchToDefaultFrame();
        waitForFrameToLoad(Frame.MAIN_FRAME);
        
        WebElement parentDivElement = Utils.waitForElement("criterion6", "id");

        new Actions(driver).moveToElement(parentDivElement).build().perform();
        new Actions(driver).click(parentDivElement).build().perform();

        List<WebElement> allDivsUnderParent = parentDivElement.findElements(By.tagName("div"));
        int previousSize = allDivsUnderParent.size();
        
        SearchPage.addSearchCriteria("Document ID");
        Utils.waitForElement("VALUE-12-1", "id").sendKeys("139");

        parentDivElement = SearchPage.getParent(SearchPage.getParent(parentDivElement));
        
        allDivsUnderParent = parentDivElement.findElements(By.tagName("div"));
        int afterSize = allDivsUnderParent.size();
        
        softAssert.assertTrue(afterSize > previousSize, "3) Make sure appears in the middle group and comes before the third group visually:: ");
        
        performSearch();
        
        SearchPage.goToDashboard();
        waitForFrameToLoad(Frame.MAIN_FRAME);

        WebElement criterion7Element = Utils.waitForElement("criterion7", "id");
        new Actions(driver).moveToElement(criterion7Element).perform();
        waitFor(5);
        WebElement divEleemnt = criterion7Element.findElement(By.tagName("div"));
        click(divEleemnt.findElements(By.tagName("button")).get(2));
        
        performSearch();
        
        boolean isSearchPerformed = SearchPage.isSearchPage() == false;
        
        softAssert.assertTrue(isSearchPerformed, "6) Re-run the search and ensure that is returns results (does not fail or run forever):: ");

        softAssert.assertAll();
      }
}

