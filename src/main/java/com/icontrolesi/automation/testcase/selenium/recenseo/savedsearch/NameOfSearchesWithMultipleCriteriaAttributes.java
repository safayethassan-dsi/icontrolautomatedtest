package com.icontrolesi.automation.testcase.selenium.recenseo.savedsearch;

import org.openqa.selenium.By;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Frame;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchesAccordion;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;

import com.icontrolesi.automation.platform.util.Logger;

public class NameOfSearchesWithMultipleCriteriaAttributes extends TestHelper{
By todayItemLocator = By.cssSelector("#today > ul > li");
	
	String searchCriteria = "Author";
	String searchCriteria2 = "Addressee";
	
	@Test
	public void test_c39_NameOfSearchesWithMultipleCriteriaAttributes(){
		handleNameOfSearchesWithMultipleCriteriaAttributes();
	}
	
	private void handleNameOfSearchesWithMultipleCriteriaAttributes(){
		new SelectRepo(AppConstant.DEM0);
		
		SearchPage.setWorkflow("Search");
		
		SearchPage.addSearchCriteria(searchCriteria);
		SearchPage.addSearchCriteria(searchCriteria2);
		performSearch();
		
		SearchPage.goToDashboard();
		waitForFrameToLoad(driver, Frame.MAIN_FRAME);
		
		SearchesAccordion.open();
		SearchesAccordion.openSearchHistoryTab();
		
		int totalCountBeforeRunningSearch = getTotalElementCount(todayItemLocator);
		System.out.println( Logger.getLineNumber() + "Total count: " + totalCountBeforeRunningSearch);
		
		boolean isSavedSearchNameFormatOk = false;
		
		if(totalCountBeforeRunningSearch > 0){
			String searchName = getText(todayItemLocator);
			isSavedSearchNameFormatOk = searchName.matches("\\d{2}:\\d{2}:\\d{2}\\s+(A|P)M," + searchCriteria + "," + searchCriteria2);
		}else{
			System.out.println( Logger.getLineNumber() + "No search history stored...");
		}
		
		softAssert.assertTrue(isSavedSearchNameFormatOk, "5. The 'Search History' instance being created is named in the following format : <time>, <search_criteria_attribute_1>, <search_criteria_attribute_2>,... <search_criteria_attribute_n>:: ");
    	
       	softAssert.assertAll();
	}
}
