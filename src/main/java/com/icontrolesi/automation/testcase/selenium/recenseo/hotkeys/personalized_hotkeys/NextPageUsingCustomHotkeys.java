package com.icontrolesi.automation.testcase.selenium.recenseo.hotkeys.personalized_hotkeys;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;
import com.icontrolesi.automation.testcase.selenium.recenseo.hotkeys.Hotkeys;

public class NextPageUsingCustomHotkeys extends TestHelper{
	
	@Test(enabled = true)
	public void test_c1725_NextPageUsingCustomHotkeys(){
		new SelectRepo(AppConstant.DEM0);
		
		Hotkeys.open();
		Hotkeys.enableHotkeys();
		Hotkeys.setDefaultHotkeys();
		Hotkeys.setPersonalizedHotkey(Hotkeys.nextDocKey, "W");
		Hotkeys.close();
		
		SearchPage.setWorkflow("Search");
		
		SearchPage.addSearchCriteria("Author");
		SearchPage.setCriterionValue(0, "jeff");
		
		performSearch();
		
		int totalDocInView = getTotalElementCount(DocView.gridRowLocator);
		
		int docToSelect = getRandomNumberInRange(1, totalDocInView - 1);
		
		DocView.clickOnDocument(docToSelect);
		
		Hotkeys.pressHotkey(true, "w");
		waitFor(15);
		
		String documentNumber = getElements(DocView.gridRowLocator).stream()
			.filter(r -> getAttribute(r, "class").endsWith("Highlight"))
			.findFirst().get().findElement(By.tagName("td")).getText().trim();
		
		Assert.assertEquals(documentNumber, docToSelect + 1 + "", "*** The next page in the currently selected document displayed:: ");
	}
}
