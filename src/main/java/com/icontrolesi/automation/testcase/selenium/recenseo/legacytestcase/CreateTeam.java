package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import java.util.Date;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SprocketMenu;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.UserManager;


public class CreateTeam extends TestHelper{
	String teamName = "test_c1679_UpdateTeam_" + new Date().getTime();
	
	@Test
	public void test_c1678_CreateTeam(){
	    new SelectRepo(AppConstant.DEM0);
		
		SprocketMenu.openUserManager();
		UserManager.createTeam(teamName, "Team of Ishtiyaq","USER");
		
		boolean teamPresent = UserManager.teamExists(teamName);
		
		UserManager.deleteTeam(teamName);	
		
		Assert.assertTrue(teamPresent, "(5) Verify that the [Available Teams] drop down now contains the New Team:: ");
	}
}
