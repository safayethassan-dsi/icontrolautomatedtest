package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Frame;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;

import com.icontrolesi.automation.platform.util.Logger;

/**
 * 
 * @author Shaheed Sagar
 *
 *	"DB: DEM0 
	(1) Run a search where Document ID is in list:  
	1351  
	2970  
	572859  
	576515  
	582338  
	904517  
	1002390  
	1022900  
	1027881  
	1225709  
	1636979  
	(2) Verify that Recenseo's persistent highlighting highlights each of the terms (in the included color) in the appropriate doc id below.  
	(a) 1351: Control(Red)  
	 
	(b) 2970:  
	controldocs.com (green)  
	PO (Light Violet)  
	Prim* (Light Violet)  
	*apital (Light Violet)   
	""fix loans"" proximity within 2 (Light Violet)  
	""fixed rate loan"" exact phrase (Light Violet)  
	info (Light Violet)  
	gazillion-now.net (Light Violet)  
	Todd@ToddDwyer.com (Light Violet)  
	http://gazillion-now.net* (Light Violet)  
	""Home payment"" proximity within 15 (Light Violet)  
	 
	(c) 572859: ""Home payment"" proximity within 15 (Light Violet)  
	 
	(d) 576515: ""Bill Gates"" exact phrase (Light Magenta)  
	 
	(e) 582338: ""Home payment"" proximity within 15 (Light Violet) (word order reversed)  
	 
	(f) 904517: SP-15* (Light Violet)  
	 
	(g) 1022900: opt-in* (Light Violet)  
	 
	(h) 1027881: opt-in* (Light Violet)  
 
	(i) 1225709: AN ARABIC WORD THAT MANTIS CANNOT DISPLAY.  
	Recenseo should highlight it in the center of the first line of the body AFTER the subject line. (Green)  
	 
	(j) 1636979: geinstalle* (Red)  
	  
	(k) Verify that the following limitation still exists: Recenseo cannot highlight proximity terms with wildcards:  
	Example: 1002390: "debt pay* proximity within 10 (Light Violet)
 */

public class DbdPersistentHighlight extends TestHelper{
	final static String RED = "#FF3333";
	final static String LIGHT_MAGENTA = "#FF66FF";
	final static String LIGHT_VIOLET = "#BF5FFF";
	final static String LIGHT_GREEN = "#74BBFB";
	final static String GREEN = "#33FF33";
	@Test
	public void test_c243_DbdPersistentHighlight(){
		handleDbdPesistentHighlight(driver);
	}

	private void handleDbdPesistentHighlight(WebDriver driver){
		new SelectRepo(AppConstant.DEM0);
		
		DocView.openPreferenceWindow();
		DocView.checkSelectedColumns("Document ID");
		DocView.clickCrossBtn();
		
		SearchPage.setWorkflow("Search");
		
		SearchPage.addSearchCriteria("Document ID");
		Select criterionSelector = new Select(getElement(By.className("CriterionOperator")));
		criterionSelector.selectByValue("IN");
		SearchPage.setCriterionValueTextArea(0, "1351\n2970\n572859\n576515\n582338\n904517\n1002390\n1022900\n1027881\n1225709\n1636979");
		waitFor(2);
		
		SearchPage.setSortCriterion();
		SearchPage.setSortCriterionValue(1, "Document ID");
		
		performSearch();
		
		DocView.clickOnDocument(1);
		DocView.selectViewByLink("Text");
		DocView.openHighlightWindow();
		
		softAssert.assertEquals(getColorFromElement(By.id("1_anchor"), "background-color"), RED, "(2a) 1351: Control(Red):: ");
		
		switchToDefaultFrame();
		waitForFrameToLoad(Frame.MAIN_FRAME);
		DocView.clickOnDocument(2);
		DocView.selectViewByLink("Text");
		DocView.openHighlightWindow();
		
		//softAssert.assertEquals(getColorFromElement(By.id("36_anchor"), "background-color"), GREEN, "(2b) 2970: controldocs.com (green):: ");
		
		List<WebElement> anchors = getElements(By.cssSelector("li:not([style='display:none'])[class$='leaf'] > a"));
		//softAssert.assertEquals(getColorFromElement(anchors.get(0), "background-color"), LIGHT_GREEN, "(2b) 2970: " + anchors.get(0).getText() + " (Green):: ");
		//anchors.remove(0); //remove last element, it's out of this group (i.e.: remove green only)
		//int i = 0;
		for(WebElement anchor : anchors){
			System.out.println( Logger.getLineNumber() + anchor.getText() + " : " + anchor.getCssValue("background-color") + "***");
			if(anchor.getText().contains("controldocs.com"))
				softAssert.assertEquals(getColorFromElement(anchor, "background-color"), GREEN,  "(2b) 2970: " + anchor.getText() + "(Green):: ");
			else
				softAssert.assertEquals(getColorFromElement(anchor, "background-color"), LIGHT_VIOLET, "(2b) 2970: " + anchor.getText() + " (Light Violet):: ");
		}
		
		switchToDefaultFrame();
		waitForFrameToLoad(Frame.MAIN_FRAME);
		
		DocView.clickOnDocument(3);
		DocView.selectViewByLink("Text");
		DocView.openHighlightWindow();
		
		softAssert.assertEquals(getColorFromElement(By.id("24_anchor"), "background-color"), LIGHT_VIOLET, "(2c) 572859: \"Home payment\" proximity within 15 (Light Violet):: ");

		switchToDefaultFrame();
		waitForFrameToLoad(Frame.MAIN_FRAME);
		
		DocView.clickOnDocument(4);
		DocView.selectViewByLink("Text");
		DocView.openHighlightWindow();
		
		anchors = getElements(By.cssSelector("li:not([style='display:none'])[class$='leaf'] > a"));
		
		softAssert.assertEquals(getColorFromElement(anchors.get(0), "background-color"), LIGHT_MAGENTA, "(2d) 576515: \"Bill Gates\" exact phrase (Light Magenta):: ");
		
		
		switchToDefaultFrame();
		waitForFrameToLoad(Frame.MAIN_FRAME);
		
		DocView.clickOnDocument(5);
		DocView.selectViewByLink("Text");
		DocView.openHighlightWindow();
		
		anchors = getElements(By.cssSelector("li:not([style='display:none'])[class$='leaf'] > a"));
		
		softAssert.assertEquals(getColorFromElement(anchors.get(anchors.size() - 2), "background-color"), LIGHT_VIOLET, "(2e) 572859: \"Home payment\" proximity within 15 (Light Violet):: ");

		
		switchToDefaultFrame();
		waitForFrameToLoad(Frame.MAIN_FRAME);
		
		DocView.clickOnDocument(6);
		DocView.selectViewByLink("Text");
		DocView.openHighlightWindow();
		
		anchors = getElements(By.cssSelector("li:not([style='display:none'])[class$='leaf'] > a"));
		
		softAssert.assertEquals(getColorFromElement(anchors.get(0), "background-color"), LIGHT_VIOLET, "(2f)  904517: SP-15* (Light Violet):: ");


		switchToDefaultFrame();
		waitForFrameToLoad(Frame.MAIN_FRAME);
		
		DocView.clickOnDocument(8);
		DocView.selectViewByLink("Text");
		DocView.openHighlightWindow();
		
		anchors = getElements(By.cssSelector("li:not([style='display:none'])[class$='leaf'] > a"));
		
		softAssert.assertEquals(getColorFromElement(anchors.get(0), "background-color"), LIGHT_VIOLET, "(2g)  1022900: opt-in* (Light Violet):: ");

		
		switchToDefaultFrame();
		waitForFrameToLoad(Frame.MAIN_FRAME);
		
		DocView.clickOnDocument(9);
		DocView.selectViewByLink("Text");
		DocView.openHighlightWindow();
		
		anchors = getElements(By.cssSelector("li:not([style='display:none'])[class$='leaf'] > a"));
		
		softAssert.assertEquals(getColorFromElement(anchors.get(0), "background-color"), LIGHT_VIOLET, "(2h)1027881: opt-in* (Light Violet):: ");
		
		
		switchToDefaultFrame();
		waitForFrameToLoad(Frame.MAIN_FRAME);
		
		DocView.clickOnDocument(10);
		DocView.selectViewByLink("Text");
		DocView.openHighlightWindow();
		
		anchors = getElements(By.cssSelector("li:not([style='display:none'])[class$='leaf'] > a"));
		
		softAssert.assertEquals(getColorFromElement(anchors.get(0), "background-color"), GREEN, "(2i) 1225709: AN ARABIC WORD THAT MANTIS CANNOT DISPLAY. Recenseo should highlight it in the center of the first line of the body AFTER the subject line. (Green)::");

		
		switchToDefaultFrame();
		waitForFrameToLoad(Frame.MAIN_FRAME);
		
		DocView.clickOnDocument(11);
		DocView.selectViewByLink("Text");
		DocView.openHighlightWindow();
		
		anchors = getElements(By.cssSelector("li:not([style='display:none'])[class$='leaf'] > a"));
		
		softAssert.assertEquals(getColorFromElement(anchors.get(0), "background-color"), RED, "(2j) 1636979: geinstalle* (Red)::");
		
		switchToDefaultFrame();
		waitForFrameToLoad(Frame.MAIN_FRAME);
		
		DocView.clickOnDocument(7);
		DocView.selectViewByLink("Text");
		DocView.openHighlightWindow();
		
		anchors = getElements(By.cssSelector("li:not([style='display:none'])[class$='leaf'] > a"));
		
		softAssert.assertEquals(anchors.size(), 0, "(2k) Verify that the following limitation still exists: Recenseo cannot highlight proximity terms with wildcards: Example: 1002390: \"debt pay* proximity within 10 (Light Violet)::");
	
		softAssert.assertAll();
	}
}
