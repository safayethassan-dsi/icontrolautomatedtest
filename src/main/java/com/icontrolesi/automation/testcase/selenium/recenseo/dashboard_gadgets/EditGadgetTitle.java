package com.icontrolesi.automation.testcase.selenium.recenseo.dashboard_gadgets;

import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Frame;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;

public class EditGadgetTitle extends TestHelper{
	private final String gadgetName = "Top 5 Custodians";
	private final String gadgetEditedName = gadgetName + "_Edited";
	@Test
	public void test_c154_EditGadgetTitle(){
		new SelectRepo(AppConstant.DEM0);
		
		waitForFrameToLoad(Frame.MAIN_FRAME);
		
		if(!SearchPage.Gadgets.isGadgetExist(gadgetName)){
			SearchPage.Gadgets.addGadgetFromDashboard();
		}
		
		int totalGadgetCount = SearchPage.Gadgets.getGadgetCountFor(gadgetName);
		int totalGadgetCountForNewName = SearchPage.Gadgets.getGadgetCountFor(gadgetEditedName);
		
		SearchPage.Gadgets.editGadgetName(gadgetName, gadgetEditedName);
		
		int totalGadgetCountAfterEdit = SearchPage.Gadgets.getGadgetCountFor(gadgetName);
		
		int totalGadgetCountForNewNameAfter = SearchPage.Gadgets.getGadgetCountFor(gadgetEditedName);
		
		softAssert.assertEquals(totalGadgetCount, totalGadgetCountAfterEdit + 1, "Specified Gadget count decreased after editing.");
		softAssert.assertEquals(totalGadgetCountForNewName + 1, totalGadgetCountForNewNameAfter, "*** Reanamed Gadget Count Matches as expected.");
		
		SearchPage.Gadgets.removeGadgetWithName(gadgetEditedName);
		
		softAssert.assertAll();
	}
}
