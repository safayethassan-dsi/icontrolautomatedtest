package com.icontrolesi.automation.testcase.selenium.envity.forgot_password;

import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.envity.common.EnvitySupport;
import com.icontrolesi.automation.testcase.selenium.envity.common.ProjectPage;

public class ResettingInactiveUser extends TestHelper{
	private final String EXPECTED_ERROR_MSG = "User is inactive."; 
	
	@Test
	public void test_c1835_ResettingInactiveUser(){
		ProjectPage.performLogout();
		
		gotoForgotPasswordPage();
		
		enterText(ResetPassword.USERNAME_FIELD, AppConstant.DISABLED_USERNAME);
		tryClick(ResetPassword.RESET_BTN, 1);
		
		String errorMsg = getText(EnvitySupport.ResetPassword.ERR_MSG_LOCATOR).replaceAll("[^\\p{ASCII}]", "").replaceAll("\n", "");
		
		softAssert.assertEquals(errorMsg, EXPECTED_ERROR_MSG);
		
		softAssert.assertAll();
	}
}
