package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.config.Configuration;
import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Download;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.FileMethods;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.FolderAccordion;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Frame;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;

import com.icontrolesi.automation.platform.util.Logger;
/**
 *
 * @author Shaheed Sagar
 *
 *  "DB: DEM0 
 
	1) Search any folder and include Bates = All Produced  
	2) Select all the documents and go to Actions\Prepare Download.  
	3) Click the 'Set File Type Settings' button and set the Download Preferences to Natives for all files 
	4) For the download options, do NOT select As Produced, confirming each will download in native format 
	5) Submit the job.  
	6) Follow the link to go to the Downloads tab. Once the Download has completed, open the .zip file and verify that the download contains native versions with names that include sort number, inv_id and filename (from CFIL). 
	7) Repeat the download, selecting ""As produced"" with any Production Set (CDOCDEMO should work). Verify the file names (still native files) in the download include sort number, inv_id, and bates number range. 
	8) Repeat the download, but change the Download Preferences to download the Imaged versions.  
	   8a) Perform one download with ""As produced"" selected and one without.  
	   8b) Verify both downloads contain the imaged PDF versions of the files, and that the naming standard follows that described in steps 6 and 7."

*/

public class ActionsPrepareDownloadNaming extends TestHelper{
	@Test
	public void test_c160_ActionsPrepareDownloadNaming(){
		String cmd = "REG ADD \"HKEY_CURRENT_USER\\Software\\Microsoft\\Internet Explorer\\New Windows\" /F /V \"PopupMgr\" /T REG_SZ /D \"yes\"";
		try {
		    Runtime.getRuntime().exec(cmd);
		    System.out.println( Logger.getLineNumber() + "Popup blocker set...");
		} catch (Exception e) {
		    System.out.println( Logger.getLineNumber() + "Error ocured!");
		}
		handleActionsPrepareDownloadNaming(driver);
	}
	
    private void handleActionsPrepareDownloadNaming(WebDriver driver){
    	new SelectRepo(AppConstant.DEM0);
    	
    	DocView.openPreferenceWindow();
    	DocView.checkSelectedColumns("Document ID", "File Name", "Bates Number");
    	DocView.closePreferenceWindow();
    	
    	SearchPage.setWorkflow("Search");
     
    	FolderAccordion.open();
        FolderAccordion.selectFolder("Email");
        FolderAccordion.addSelectedToSearch();
        
        switchToDefaultFrame();
        waitForFrameToLoad(Frame.MAIN_FRAME);
        SearchPage.addSearchCriteria("Bates Number");
        SearchPage.setOperatorValue(1, "All Produced");
    
        performSearch();
        
        DocView.setNumberOfDocumentPerPage(25);
       
        List<WebElement> rowList = getElements(DocView.gridRowLocator); 
        
        List<String> fileListInTheExtractedFolder = new ArrayList<>();
       
        for(int i = 0; i < rowList.size(); i++){
        	String serialNumber = String.format("%02d", (i+1));
        	String documentID = getText(rowList.get(i).findElement(DocView.documentIDColumnLocator));
        	String documentName = getText(rowList.get(i).findElement(DocView.documentfileNameColumnLocator));
        	
        	if(documentName.equals("")){
        		//documentName = ".pdf";
        	}else{
        		documentName = "_" + documentName.replaceAll("\\.\\w+", "");
        	}
        	
        	String fileName = serialNumber +"." + documentID + documentName;
        	
        	System.err.println(fileName + "^^^^^^^^^^^^^^^^^^^^^^^");
        	
        	fileListInTheExtractedFolder.add(fileName);
        }
        
        DocView.selectAllDocuments();
        new DocView().clickActionMenuItem("Prepare Download");
        driver.switchTo().frame(getElement(By.cssSelector("iframe[src^='javascript:']")));
        //switchToFrame(1);
        
        Download.openFileTypePrefs();
        Download.setDownloadPreferenceAllTo("Native");
		Download.closeFileTypePrefs();
		
		new DocView().clickSubmitButton();
			
		String downloadConfirmationMsg = getElement(By.tagName("body")).getText().trim();
 		
	    softAssert.assertTrue(downloadConfirmationMsg.contains("Recenseo is preparing your download in the background."), "4) Submit the job:: ");
	    
	    tryClick(By.xpath("//a[text()='Click HERE to go directly to \"Downloads\" now']"), By.className("BlueAnchor"));
	    waitFor(driver, 15);
	    
	    List<WebElement> downloadLinks =  getElements(By.className("BlueAnchor"));
	    
	    WebElement downloadableLink = downloadLinks.get(downloadLinks.size() - 1);
	    
	    String downloadableLinkName;
	    
	    
	    if(Configuration.getConfig("selenium.browser").equals("chrome")){
	    	downloadableLinkName = getText(downloadableLink).replaceAll(":|-", "_");
	    }else{
	    	downloadableLinkName = getText(downloadableLink).replaceAll(":|-", "_");
	    }
	    
	    System.out.println( Logger.getLineNumber() + "FileName: " + downloadableLinkName);
	    
	    String downloadableFile = AppConstant.DOWNLOAD_DIRECTORY + downloadableLinkName;
	    String extractedFolderLocation = downloadableFile.replaceAll("\\.zip", "");
	    
	    deleteFileWithPattern(AppConstant.DOWNLOAD_DIRECTORY, "^"+downloadableLinkName.replaceAll(".zip", "")+".*\\.zip");
	    
	    downloadableLink.click();
	     
	    Download.waitForFileDownloadToComplete(downloadableFile, 1800);
	    
	    unzip(downloadableFile, extractedFolderLocation);
	    
	    File [] listOfFiles = new File(extractedFolderLocation).listFiles();
	    
	    
	    for(File file : listOfFiles){
	    	String fileName = file.getName().replaceAll("\\.\\D+$", "");
	    	System.err.println(fileName+"&&&&&&&&&&&");
	    	fileListInTheExtractedFolder.remove(fileName);
	    }
	    
	    softAssert.assertEquals(fileListInTheExtractedFolder.size(), 0, "6) Download contains native versions with names that include sort number, inv_id and filename (from CFIL):: ");
	    
	    FileMethods.deleteFileOrFolder(downloadableFile);
	    FileMethods.deleteFileOrFolder(extractedFolderLocation);
	    
	    getElements(By.name("chkDownload")).stream().forEach(link -> link.click());
	    Download.deleteSelectDownloads();
	    
	    switchToDefaultFrame();
        waitForFrameToLoad(Frame.MAIN_FRAME);
        tryClick(getElements(By.xpath("//span[text()='Close']")).get(1), 5);
        
        new DocView().clickActionMenuItem("Prepare Download");
        List<WebElement> frameList = getElements(By.cssSelector("iframe[src*='javascript']"));
        driver.switchTo().frame(frameList.get(1));
       
        tryClick(By.name("include_ppdat"));
	    selectFromDrowdownByText(By.name("ppset_selected"), "CDOCDEMO");
	    
	    new DocView().clickSubmitButton();
		
		downloadConfirmationMsg = getElement(By.tagName("body")).getText().trim();
 		
	    softAssert.assertTrue(downloadConfirmationMsg.contains("Recenseo is preparing your download in the background."), "*) Submit the job:: ");
	    
	    tryClick(By.xpath("//a[text()='Click HERE to go directly to \"Downloads\" now']"), By.className("BlueAnchor"));
	    waitFor(driver, 15);
	  
	    downloadLinks =  getElements(By.className("BlueAnchor"));
	    
	    downloadableLink = downloadLinks.get(downloadLinks.size() - 1);
	    
	    
	    if(Configuration.getConfig("selenium.browser").equals("chrome")){
	    	downloadableLinkName = getText(downloadableLink).replaceAll(":", "_");
	    }else{
	    	downloadableLinkName = getText(downloadableLink).replaceAll(":", "_");
	    }
	    
	    System.out.println( Logger.getLineNumber() + "FileName: " + downloadableLinkName);
	    
	    downloadableFile = AppConstant.DOWNLOAD_DIRECTORY + downloadableLinkName;
	    extractedFolderLocation = downloadableFile.replaceAll("\\.zip", "");
	    
	    downloadableLink.click();
	    
	    if(Configuration.getConfig("selenium.browser").equals("chrome")){
	    	Actions actions = new Actions(driver);
	    	actions.sendKeys(Keys.ENTER).perform();
	    }
	     
	    Download.waitForFileDownloadToComplete(downloadableFile, 1800);
	    
	    tryClick(By.cssSelector("a[title='Contents']"));
	    
	    fileListInTheExtractedFolder.clear();
	    
	    List<String> downloadFileList = getListOfItemsFrom(By.cssSelector("#contentDialog > ul li"), f -> f.getText().trim());
	    for(String file : downloadFileList){
        	fileListInTheExtractedFolder.add(file);
        }
	    
	    unzip(downloadableFile, extractedFolderLocation);
	    
	    listOfFiles = new File(extractedFolderLocation).listFiles();
	    
	    for(File file : listOfFiles){
	    	fileListInTheExtractedFolder.remove(file);
	    }	    
	    
	    softAssert.assertEquals(fileListInTheExtractedFolder.size(), 0, "7) Verify the file names (still native files) in the download include sort number, inv_id, and bates number range (For As Produced):: ");
	    
	    FileMethods.deleteFileOrFolder(downloadableFile);
	    FileMethods.deleteFileOrFolder(extractedFolderLocation);
	 
	    softAssert.assertAll();
    }
    
    public static void deleteFileWithPattern(String directory, String pattern){
    	File [] fileList = new File(directory).listFiles();
    	Pattern p = Pattern.compile(pattern);
    	
    	for(File f : fileList){
    		if(p.matcher(f.getName()).find()){
    			f.delete();
    			System.err.println(f.getName() + " deleted...");
    		}
    	}
	}
    
    public static void main(String[] args) {
		deleteFileWithPattern(AppConstant.DOWNLOAD_DIRECTORY, "^2017.08.22.*\\.zip$");
	}
}
