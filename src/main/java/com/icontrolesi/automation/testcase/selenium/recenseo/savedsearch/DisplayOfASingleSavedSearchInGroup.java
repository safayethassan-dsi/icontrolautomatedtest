package com.icontrolesi.automation.testcase.selenium.recenseo.savedsearch;

import java.util.Date;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Frame;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchesAccordion;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;

import com.icontrolesi.automation.platform.util.Logger;

public class DisplayOfASingleSavedSearchInGroup extends TestHelper{
	String savedSearchName = "DisplayOfASingleSavedSearchInGroup_" + new Date().getTime();
	String savedSearchGroupName = "SavedSearchDemoGroup";
	
	String step8 = "Recenseo expands the Search Group folder in order to display the only single saved search (as mentioned in point 5) with check box on the left hand side of the search ( %s )";
	
	@Test
	public void test_c5_DisplayOfASingleSavedSearchInGroup(){
		handleDisplayOfASingleSavedSearchInGroup();
	}
	
	private void handleDisplayOfASingleSavedSearchInGroup(){
		new SelectRepo(AppConstant.DEM0);
    	
    	SearchPage.setWorkflow("Search");
    	
    	SearchesAccordion.open();
    	SearchesAccordion.createTopLevelGroup(savedSearchGroupName, "All");
    	
    	switchToDefaultFrame();
        waitForFrameToLoad(driver, Frame.MAIN_FRAME);
        
    	SearchPage.addSearchCriteria("Author");
    	
    	SearchPage.createSavedSearch(savedSearchName, "", savedSearchGroupName);
        
        SearchesAccordion.open();
        SearchesAccordion.selectFilter("All");
        SearchesAccordion.searchForSavedSearch(savedSearchName);
        
        int totalSearchResultFound = getTotalElementCount(SearchesAccordion.savedSearchUnderAGroupLocator);
        
        System.out.println( Logger.getLineNumber() + totalSearchResultFound+"*******");
        
        softAssert.assertEquals(totalSearchResultFound, 1, String.format(step8, "Verify the only one saved search exists"));
        
        WebElement savedSearchElement = getElement(SearchesAccordion.savedSearchUnderAGroupLocator);
        
        boolean isSavedSearchAChildeNode = getAttribute(savedSearchElement, "class").endsWith("childNode");
        String checkBoxDisplayProperty = savedSearchElement.findElement(By.cssSelector("i")).getCssValue("display").trim();
        
        softAssert.assertEquals(checkBoxDisplayProperty, "inline-block", String.format(step8, "Verify checkbox appears before saved search"));
        softAssert.assertTrue(isSavedSearchAChildeNode, String.format(step8, "Verify the saved search is a child node"));
        
        WebElement parentOfSavedSearchItem = savedSearchElement.findElement(By.xpath("..")).findElement(By.xpath(".."));
        System.out.println( Logger.getLineNumber() + "Tag: " + parentOfSavedSearchItem.getTagName());
        System.out.println( Logger.getLineNumber() + parentOfSavedSearchItem.getAttribute("class") + "*****");
        String savedSearchesParentName = getText(parentOfSavedSearchItem.findElement(By.tagName("a")));
        
        boolean isParentContainsFolderIcon = parentOfSavedSearchItem.findElement(By.cssSelector("a > i:nth-child(2)")).getAttribute("class").contains("icon-folder-plus"); 
        
        softAssert.assertEquals(savedSearchesParentName, savedSearchGroupName, String.format(step8, "Verify the Container name match with Group name"));
        softAssert.assertTrue(parentOfSavedSearchItem.getAttribute("class").endsWith("jstree-last parentNode"), String.format(step8, "Verify group is Parent Node"));
        softAssert.assertTrue(isParentContainsFolderIcon, String.format(step8, "Verify the conatiner contains appropriate icon"));
         
        softAssert.assertAll();
	}
}
