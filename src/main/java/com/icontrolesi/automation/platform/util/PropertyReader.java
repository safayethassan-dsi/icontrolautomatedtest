package com.icontrolesi.automation.platform.util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class PropertyReader {
	static Properties props = new Properties();
	
	
	public PropertyReader() {
		
	}
	
	public static Properties getPropertiesFrom(String path){
		try {
			props.load(new FileInputStream(path));
			System.err.println(path+"**********");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return props;
	}
}
