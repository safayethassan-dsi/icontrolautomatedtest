package com.icontrolesi.automation.platform.util;

/**
 * @author atiq
 */
public class StringUtils {

    public static boolean isEmpty(String str) {
        return (str == null || "".equals(str.trim()));
    }
    public static boolean isNotEmpty(String str) {
        return !isEmpty(str);
    }
}
