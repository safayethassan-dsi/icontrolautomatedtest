package com.icontrolesi.automation.platform.util;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import com.icontrolesi.automation.testcase.selenium.recenseo.common.Driver;

public class ScreenshotHandler {
	public static void captureScreenAndSave(String filePath){
		File capturedScreenFile = ((TakesScreenshot)Driver.getDriver()).getScreenshotAs(OutputType.FILE);
		
		try{
			FileUtils.copyFile(capturedScreenFile, new File(filePath + ".png"));
		}catch (Exception e) {
			System.out.println( Logger.getLineNumber() + "Error:: " + e.getMessage());
		}
	}
}
