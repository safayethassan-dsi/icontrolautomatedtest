package com.icontrolesi.automation.platform.config;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.TreeSet;

import javax.swing.JOptionPane;

import com.icontrolesi.automation.framework.root.Main;

/**
 * @author atiq
 */
public class Configuration {
	private static String PROJECT_SELECTED = "recenseo";
    private static final String SYSTEM = "system";
    private static final String SELENIUM = "selenium";
    public static final String PROJECT_NAME = "system.project.name";
    private static final String RESULT_OUTPUT = "system.result.output";
    private static final String ENVITY_FIELDS_VALUES = "envityFieldsValues";
    
    private static Map<String, String> configs = new HashMap<>();

    public static void loadConfigurations() throws IOException {
        getProperties(SYSTEM, SYSTEM);
        
        String [] projects = {"Recenseo", "Envity"};	// Get from config file
        
        //String projectSelected = "";
        
        if(Main.IS_RUNNING_FROM_WEB == false){
        	PROJECT_SELECTED = (String)JOptionPane.showInputDialog(null, "Select your project...", "Automation test for project", JOptionPane.QUESTION_MESSAGE, null, projects, projects[0]);
    		
        	//PROJECT_SELECTED = (PROJECT_SELECTED == null ? "recenseo" : PROJECT_SELECTED.toLowerCase());
        	
        	if(PROJECT_SELECTED == null) {
        		System.err.println("You selected no project!");
        		JOptionPane.showMessageDialog(null, "You selected no project! Exiting system...", "Exit Testing...", JOptionPane.ERROR_MESSAGE);
        		System.exit(0);
        	}else {
        		PROJECT_SELECTED = PROJECT_SELECTED.toLowerCase();
        	}
    		
    		setConfig("system.project.name", PROJECT_SELECTED);
        }else{
        	setConfig("system.project.name", PROJECT_SELECTED);
        }
        
        if(PROJECT_SELECTED.equals("envity")){
        	//getProperties(configs.get(ENVITY_FIELDS_VALUES), configs.get(ENVITY_FIELDS_VALUES));
        	getProperties(ENVITY_FIELDS_VALUES, ENVITY_FIELDS_VALUES);
        }
        
        //getProperties(ENVITY_FIELDS_VALUES, ENVITY_FIELDS_VALUES);
        
        getProperties(SELENIUM, SELENIUM);
        
        getProperties(configs.get(PROJECT_NAME), configs.get(PROJECT_NAME));
        getProperties(configs.get(RESULT_OUTPUT), configs.get(RESULT_OUTPUT));
    }

    private static void getProperties(String prefix, String path) {
        ResourceBundle resourceBundle = ResourceBundle.getBundle(path);
        for (String key : resourceBundle.keySet()) {
            String configKey = prefix + "." + key;
            configs.put(configKey, resourceBundle.getString(key).trim());
        }
    }

    public static String getConfig(String key) {
        return configs.get(key);
    }
    
    public static void setConfig(String key, String value) {
        configs.put(key, value);
    }

    public static Set<String> getAllConfigs(String configPrefix) {
    	Set<String> keys = new TreeSet<>();
        for (String key : configs.keySet()) {
            if (key.startsWith(configPrefix)) {
                keys.add(key);
            }
        }
        
        Set<String> classpathSet = new LinkedHashSet<>();
        for (String key : keys) {
        	classpathSet.add(configs.get(key));
        }
        return classpathSet;
    }
    
    public static void setSelectedProject(String prjectSelected){
    	Configuration.PROJECT_SELECTED = prjectSelected;
    }
    
    public static String getSelectedProject(){
    	return Configuration.PROJECT_SELECTED;
    }
}
